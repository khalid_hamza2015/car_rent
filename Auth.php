<?php
require_once("systemRent/admin_permissions.php");
$admin_permission 	= implode(',', $admin_permissions);
class auth {
	function isLogged() {
      if(isset($_SESSION['rent_id']))  
        return true;
      else
        return false;
    }
    function login($username,$password) {
		include('../DBF.php');	
		$username 	= trim($username," ");	
		$dbh  		= new PDO('mysql:dbname='.$db.';host='.$host.';port='.$port,$user,$pass);
		$stmt 		= $dbh->prepare("SELECT * FROM `users` WHERE `uname` = :uname AND `password` = :password AND `flag_status` = 1 ");
		$stmt->bindParam(':uname'   ,$username, PDO::PARAM_STR);
		$stmt->bindParam(':password',$password, PDO::PARAM_STR);
		$stmt->execute();		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch();
			header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
            session_start();
            session_regenerate_id(); 
			$_SESSION['OBSOLETE']  			= true;
		    $_SESSION['EXPIRES']   			= time() + 60000; 
            $_SESSION['rent_id'] 			= $row["id"];
            $_SESSION["user_name"] 			= $row["name"];
            $_SESSION["flag_type"] 			= $row["flag_type"];//1:admin,2:provider
            $_SESSION["permission"] 		= 0; // admin
            $_SESSION["country"] 			= 0; // admin
            $_SESSION["permission_view"] 	= $GLOBALS['admin_permission'];
            $_SESSION["permission_add"] 	= $GLOBALS['admin_permission'];
            $_SESSION["permission_edit"] 	= $GLOBALS['admin_permission'];
            $_SESSION["permission_delete"] 	= $GLOBALS['admin_permission'];
            return true;
		} else {
			// PROVIDER
			$stmt = $dbh->prepare("SELECT * FROM `providers` 
									WHERE (
											`provider_username` = :username 
											OR 
											`provider_email`= :username
											) 
									AND `provider_password` = :user_password 
									AND `status` = 1 
								");
			$password 	= base64_encode($password);
			$stmt->bindParam(':username',$username, PDO::PARAM_STR);
			$stmt->bindParam(':user_password',$password, PDO::PARAM_STR);		
			$stmt->execute();

			if($stmt->rowCount() > 0) {				
				print_r($stmt);
				$row = $stmt->fetch();
				header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	            session_start();
	            session_regenerate_id(); 

				$_SESSION['OBSOLETE']  			= true;
			    $_SESSION['EXPIRES']   			= time() + 60000; 
	            $_SESSION["rent_id"]	 		= $row["provider_id"];
	            $_SESSION["user_name"] 			= $row["provider_username"];
	            $_SESSION["user_email"] 		= $row["provider_email"];
	            $_SESSION["flag_type"] 			= 2;
	            $_SESSION["country"] 			= $row["country_id"];
	            $_SESSION["permission"] 		= $row["provider_type"];// type
	            $_SESSION["permission_view"] 	= $row["permission_view"]; 
	            $_SESSION["permission_add"] 	= $row["permission_add"];
	            $_SESSION["permission_edit"] 	= $row["permission_edit"];
	            $_SESSION["permission_delete"] 	= $row["permission_delete"];
	            $_SESSION["parent_id"] 			= $row["parent_id"];
	            return true;

			}else{

	           unset($_SESSION["rent_id"]);
	           unset($_SESSION["user_name"]) ;
	           unset($_SESSION["flag_type"]) ;
	           return false;
			}
		}	
    }
	
    function logOut(){
		session_start();
		unset($_SESSION['rent_id']) ;
		unset($_SESSION["user_name"]) ;
		unset($_SESSION["flag_type"]) ;
		unset($_SESSION["user_email"]) ;
		unset($_SESSION["country"]);
		unset($_SESSION["permission"]) ;
		unset($_SESSION["permission_view"]);
		unset($_SESSION["permission_add"]);
		unset($_SESSION["permission_edit"]);
		unset($_SESSION["permission_delete"]);
		unset($_SESSION["parent_id"]);
				
		session_destroy();
		session_regenerate_id();  
		header( 'Location: index.php' ) ;
    }
	
    function getLoggedUserID()
	{
		return  $_SESSION['rent_id'];
    }
	
    function getLoggedUserName()
	{
		return  $_SESSION["user_name"]; 
    }
}?>