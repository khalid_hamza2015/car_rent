-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2017 at 03:31 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rent_car`
--

-- --------------------------------------------------------

--
-- Table structure for table `car_brands`
--

CREATE TABLE IF NOT EXISTS `car_brands` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `cat_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>active;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `car_brands`
--

INSERT INTO `car_brands` (`cat_id`, `parent_id`, `sub_id`, `cat_name_en`, `cat_name_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 0, 0, 'car brand', 'car brand', 1, 1, '2017-12-12 05:13:40', 1, '2017-12-12 05:17:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `status_flag` smallint(6) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order_no` int(11) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `location_en` text,
  `location_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `phone`, `mobile`, `fax`, `email`, `longitude`, `latitude`, `location_en`, `location_ar`) VALUES
(1, '', '+965 ', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `desc_en` text,
  `desc_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `name_en`, `name_ar`, `img`, `video`, `desc_en`, `desc_ar`) VALUES
(1, 'About Us', 'من نحن', '', '', '\r\n', ''),
(2, 'Terms and Conditions', 'الشروط والأحكام', '', '', '', ''),
(3, 'Supporters', 'الدعم', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `country_city`
--

CREATE TABLE IF NOT EXISTS `country_city` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(100) NOT NULL,
  `name_ar` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `sort_id` int(11) DEFAULT NULL,
  `status_flag` smallint(6) DEFAULT '1' COMMENT '1:active,2:disable,3:del',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `country_city`
--

INSERT INTO `country_city` (`id_country`, `name_en`, `name_ar`, `parent_id`, `sort_id`, `status_flag`) VALUES
(1, 'Kuwait', 'الكويت', 0, 0, 1),
(2, 'Kuwait City', 'مدينة الكويت', 1, NULL, 1),
(3, 'Farwaniyah', 'الفروانية', 1, NULL, 1),
(4, 'Hawalli', 'حولي', 1, NULL, 1),
(5, 'Saudi Arabia', 'السعودية', 0, 0, 1),
(6, 'Riyadh', 'الرياض', 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>active;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`feature_id`, `feature_name_en`, `feature_name_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Feature 1', 'Feature 1', 1, 1, '2017-12-12 11:23:40', 1, '2017-12-12 11:30:53'),
(2, 'Feature 2', 'Feature 2', 1, 1, '2017-12-12 11:31:03', 0, '0000-00-00 00:00:00'),
(3, 'Feature 3', 'Feature 3', 1, 1, '2017-12-12 11:31:11', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `txt_phone` varchar(255) NOT NULL,
  `txt_email` varchar(255) NOT NULL,
  `gender` smallint(6) DEFAULT NULL COMMENT '1:male,2:female',
  `password` varchar(255) NOT NULL,
  `status_flag` smallint(6) DEFAULT NULL COMMENT '1:active,2:not active',
  `date_create` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `txt_name`, `username`, `txt_phone`, `txt_email`, `gender`, `password`, `status_flag`, `date_create`, `img`, `weight`, `height`) VALUES
(1, 'Mohammed AlDini', 'maldini', '55060102', 'm.aldini@amkwt.com', 2, '123', 1, '2017-12-04 00:00:00', NULL, '110', '183');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE IF NOT EXISTS `providers` (
  `provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_type` tinyint(4) NOT NULL COMMENT '1=Car_Rent;2=Traveling_Office;3=Sales_Representative',
  `provider_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_description_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_brief_en` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_brief_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `provider_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_responsible_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_responsible_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_contract_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_important` tinyint(4) NOT NULL COMMENT '1=>High;2=>Average;3=>Low',
  `featured` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `can_reserve` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `status` smallint(6) NOT NULL COMMENT '1=>enable;2=>disable;3=>delete',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`provider_id`, `provider_type`, `provider_email`, `provider_username`, `provider_password`, `provider_name_en`, `provider_name_ar`, `provider_description_en`, `provider_description_ar`, `provider_brief_en`, `provider_brief_ar`, `provider_mobile`, `provider_telephone`, `country_id`, `area_id`, `provider_address`, `provider_lat`, `provider_long`, `provider_logo`, `provider_banner`, `provider_responsible_name`, `provider_responsible_mobile`, `provider_features`, `provider_contract_number`, `provider_important`, `featured`, `can_reserve`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'provider@sahalah.com', 'provider', 'MTIz', 'provider name', 'kkkkk', 'llll', 'llll', 'lll', 'llll', '12234587', '', 1, 0, 'llll', '', '', '', '', 'vvvv', '1223', '', '', 0, 1, 0, 1, 1, '2017-11-23 04:56:49', 1, '2017-11-28 02:49:29'),
(2, 1, 'provider2@system.com', 'provider2', 'MTQ3', 'provider 2 test', 'provider 2 test', 'c test', 'c test', 'c test', 'c test', '', '', 1, 2, 'provider 2 address test', '29.311660', '47.481766', '', '', 'provider 2 test', '87654321', '1,3', '120', 1, 0, 0, 1, 1, '2017-12-12 01:20:42', 1, '2017-12-12 01:31:05');

-- --------------------------------------------------------

--
-- Table structure for table `provider_mobiles`
--

CREATE TABLE IF NOT EXISTS `provider_mobiles` (
  `mobile_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `mobile_type` tinyint(4) NOT NULL COMMENT '1=>mobile;2=>telephone',
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`mobile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `provider_mobiles`
--

INSERT INTO `provider_mobiles` (`mobile_id`, `provider_id`, `mobile_type`, `mobile_number`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, '12345678', 1, 1, '2017-12-12 02:01:04', 1, '2017-12-12 03:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `provider_social`
--

CREATE TABLE IF NOT EXISTS `provider_social` (
  `social_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `social_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`social_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `provider_social`
--

INSERT INTO `provider_social` (`social_id`, `provider_id`, `social_name`, `social_link`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'instagram', 'http://127.0.0.1/car_rent/systemRent/providers/provider_socials.php?provider_id=1', 1, 1, '2017-12-12 02:25:55', 1, '2017-12-12 02:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE IF NOT EXISTS `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(100) NOT NULL,
  `name_ar` varchar(256) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` text,
  `description_ar` text,
  `image` varchar(100) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `tele` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `flag_type` varchar(10) DEFAULT NULL COMMENT '1:Get Active,2:Eat Better,3:Shop',
  `status_flag` smallint(6) NOT NULL DEFAULT '1' COMMENT '1:active,2:not active,3:delete',
  `date_create` datetime NOT NULL,
  `date_update` varchar(20) DEFAULT NULL,
  `user_create` int(11) DEFAULT '0',
  `user_update` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`id`, `name_en`, `name_ar`, `email`, `username`, `password`, `description`, `description_ar`, `image`, `banner`, `sort`, `tele`, `mobile`, `instagram`, `website`, `flag_type`, `status_flag`, `date_create`, `date_update`, `user_create`, `user_update`) VALUES
(1, 'Techbox', 'تيكبوكس', 'm.a@amkw.com', 'techbox', 'P@ssw0rd', 'test', 'test', '5536logo_xs.jpg', '3931Banner02.jpg', 0, '64641361', '123456', 'https://www.instagram.com/mo_aldini/', 'www.google.com', '1,2', 1, '2017-12-04 00:00:00', '2017-12-05 16:30:47', 0, 1),
(2, 'Aswar Almubarkiah', 'أسوار المباركية', 'info@amkwt.com', 'amkwt', '123456', 'test', 'test', '9534portfolio09.jpg', '', 0, '123654', '', '', '', '1,2,3', 1, '2017-12-06 10:10:24', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `uname` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `flag_type` smallint(6) DEFAULT NULL COMMENT '1:admin,2:receptionist',
  `flag_status` smallint(6) DEFAULT NULL COMMENT '1:active,2:delete',
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `uname`, `password`, `flag_type`, `flag_status`, `date_create`) VALUES
(1, 'administrator', 'admin', 'admin', 1, 1, '2017-12-04 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
