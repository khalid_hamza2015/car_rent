-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2017 at 01:07 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rent_car`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_title_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_desc_en` text COLLATE utf8_unicode_ci NOT NULL,
  `banner_desc_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `banner_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>enable;2=>disable;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_title_en`, `banner_title_ar`, `banner_desc_en`, `banner_desc_ar`, `banner_url`, `banner_image`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'cc test', 'ccc test', 'c test', 'c test', 'http://127.0.0.1/car_rent/systemRent/banners/banner_form.php?id=1', '', 1, 1, '2017-12-14 04:10:58', 1, '2017-12-14 04:13:15');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_owner` tinyint(4) NOT NULL COMMENT '1=>admin;2=>provider',
  `provider_id` int(11) NOT NULL,
  `car_brand` int(11) NOT NULL,
  `car_type` int(11) NOT NULL,
  `car_cat` int(11) NOT NULL,
  `car_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `car_color` int(11) NOT NULL,
  `car_inner_color` int(11) NOT NULL,
  `car_engine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `car_gear` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `car_features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stop_status` tinyint(4) NOT NULL COMMENT '1=>stopped;0=>working ',
  `stop_start_date` date NOT NULL,
  `stop_end_date` date NOT NULL,
  `delivery_service` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `delivery_to_airport` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `delivery_with_driver` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `commission_value` decimal(11,3) NOT NULL,
  `commission_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>KD;2=>percent',
  `approved` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `status` tinyint(4) NOT NULL COMMENT '1=>enabled;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`car_id`, `car_owner`, `provider_id`, `car_brand`, `car_type`, `car_cat`, `car_model`, `car_color`, `car_inner_color`, `car_engine`, `area_id`, `car_gear`, `car_features`, `stop_status`, `stop_start_date`, `stop_end_date`, `delivery_service`, `delivery_to_airport`, `delivery_with_driver`, `commission_value`, `commission_type`, `approved`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 2, 1, 5, 6, 7, '2017', 7, 8, 'engine test', 2, 'gear test', '4,5', 0, '0000-00-00', '0000-00-00', 0, 1, 0, '1.000', 2, 1, 1, 1, '2017-12-13 04:41:50', 1, '2017-12-14 05:02:25'),
(2, 1, 0, 1, 2, 4, '2017', 7, 7, 'Engine', 2, 'Gear', '4,5', 0, '0000-00-00', '0000-00-00', 1, 1, 1, '1.000', 1, 1, 1, 1, '2017-12-17 01:10:03', 0, '0000-00-00 00:00:00'),
(3, 2, 1, 1, 2, 3, '1988', 8, 8, 'engine test', 5, 'gear test', '5', 0, '0000-00-00', '0000-00-00', 1, 0, 0, '0.000', 1, 0, 1, 4, '2017-12-17 02:44:11', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_brands`
--

CREATE TABLE IF NOT EXISTS `car_brands` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `cat_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>active;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `car_brands`
--

INSERT INTO `car_brands` (`cat_id`, `parent_id`, `sub_id`, `cat_name_en`, `cat_name_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 0, 0, 'American', 'American', 1, 1, '2017-12-12 05:13:40', 1, '2017-12-13 08:53:18'),
(2, 1, 0, 'Ford', 'Ford', 1, 1, '2017-12-12 05:40:16', 1, '2017-12-13 08:53:38'),
(3, 1, 2, 'Expedition ', 'Expedition ', 1, 1, '2017-12-12 06:00:07', 1, '2017-12-13 08:54:49'),
(4, 1, 2, 'Expedition 2', 'Expedition 2', 1, 1, '2017-12-12 06:02:23', 1, '2017-12-13 08:55:09'),
(5, 0, 0, 'Japan ', 'Japan ', 1, 1, '2017-12-13 08:55:27', 0, '0000-00-00 00:00:00'),
(6, 5, 0, 'Toyota ', 'Toyota ', 1, 1, '2017-12-13 08:55:42', 0, '0000-00-00 00:00:00'),
(7, 5, 6, 'Camry ', 'Camry ', 1, 1, '2017-12-13 08:55:56', 0, '0000-00-00 00:00:00'),
(8, 5, 6, 'corolla  ', 'corolla  ', 1, 1, '2017-12-13 08:57:19', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_instructions`
--

CREATE TABLE IF NOT EXISTS `car_instructions` (
  `instruction_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `instruction_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instruction_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>enabled;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`instruction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `car_instructions`
--

INSERT INTO `car_instructions` (`instruction_id`, `car_id`, `instruction_image`, `instruction_order`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, '4956Penguins.jpg', '', 3, 1, '2017-12-14 02:03:51', 1, '2017-12-14 02:08:09'),
(2, 1, '7480Chrysanthemum.jpg', '', 1, 1, '2017-12-14 04:40:47', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_insurance`
--

CREATE TABLE IF NOT EXISTS `car_insurance` (
  `insurance_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `insurance_type` int(11) NOT NULL,
  `insurance_features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insurance_strat_date` date NOT NULL,
  `insurance_end_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`insurance_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `car_insurance`
--

INSERT INTO `car_insurance` (`insurance_id`, `car_id`, `company_id`, `insurance_type`, `insurance_features`, `insurance_strat_date`, `insurance_end_date`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, 1, '6,16', '2017-12-01', '2017-12-01', 1, '2017-12-14 12:46:18', 1, '2017-12-14 04:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `car_rent_cost`
--

CREATE TABLE IF NOT EXISTS `car_rent_cost` (
  `rent_cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `rent_type` int(11) NOT NULL,
  `rent_cost` decimal(11,3) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`rent_cost_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `car_rent_cost`
--

INSERT INTO `car_rent_cost` (`rent_cost_id`, `car_id`, `rent_type`, `rent_cost`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(4, 1, 10, '125.000', 1, '2017-12-14 11:19:39', 1, '2017-12-17 12:27:16'),
(6, 1, 9, '50.000', 1, '2017-12-17 12:27:16', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_traffic`
--

CREATE TABLE IF NOT EXISTS `car_traffic` (
  `traffic_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `car_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rule_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `car_book` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`traffic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `car_traffic`
--

INSERT INTO `car_traffic` (`traffic_id`, `car_id`, `car_plate`, `rule_number`, `car_book`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 2, '123', '123', '', 1, '2017-12-13 04:34:04', 0, '0000-00-00 00:00:00'),
(2, 1, '123456', '123456', '', 1, '2017-12-13 04:41:50', 1, '2017-12-14 05:02:25'),
(3, 2, '12301258', '1250', '', 1, '2017-12-17 01:10:03', 0, '0000-00-00 00:00:00'),
(4, 3, '145875', '125698', '', 4, '2017-12-17 02:44:11', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `company_type` tinyint(4) NOT NULL COMMENT '1=>insurance ',
  `company_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_desc_en` text COLLATE utf8_unicode_ci NOT NULL,
  `company_desc_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>enabled;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `country_id`, `company_type`, `company_name_en`, `company_name_ar`, `company_desc_en`, `company_desc_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, 'insurance company 1', 'insurance company 1 ar', 'insurance company 1', 'insurance company 1', 1, 1, '2017-12-13 10:48:41', 0, '2017-12-13 10:50:02'),
(2, 1, 1, 'test', 'test', 'test', 'test', 1, 1, '2017-12-14 11:58:05', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `location_en` text,
  `location_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `phone`, `mobile`, `fax`, `email`, `longitude`, `latitude`, `location_en`, `location_ar`) VALUES
(1, '', '+965 ', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `desc_en` text,
  `desc_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `name_en`, `name_ar`, `img`, `video`, `desc_en`, `desc_ar`) VALUES
(1, 'About Us', 'من نحن', '', '', '\r\n', ''),
(2, 'Terms and Conditions', 'الشروط والأحكام', '', '', '', ''),
(3, 'Supporters', 'الدعم', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `country_city`
--

CREATE TABLE IF NOT EXISTS `country_city` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(100) NOT NULL,
  `name_ar` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `sort_id` int(11) DEFAULT NULL,
  `status_flag` smallint(6) DEFAULT '1' COMMENT '1:active,2:disable,3:del',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `country_city`
--

INSERT INTO `country_city` (`id_country`, `name_en`, `name_ar`, `parent_id`, `sort_id`, `status_flag`) VALUES
(1, 'Kuwait', 'الكويت', 0, 0, 1),
(2, 'Kuwait City', 'مدينة الكويت', 1, NULL, 1),
(3, 'Farwaniyah', 'الفروانية', 1, NULL, 1),
(4, 'Hawalli', 'حولي', 1, NULL, 1),
(5, 'Saudi Arabia', 'السعودية', 0, 0, 1),
(6, 'Riyadh', 'الرياض', 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_for` tinyint(4) NOT NULL COMMENT '1=>provider;2=>car;3=>insurance_features;4:colors;5=>rent_period',
  `feature_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>active;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`feature_id`, `feature_for`, `feature_name_en`, `feature_name_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'Feature 1', 'Feature 1', 1, 1, '2017-12-12 11:23:40', 1, '2017-12-12 11:30:53'),
(2, 1, 'Feature 2', 'Feature 2', 1, 1, '2017-12-12 11:31:03', 0, '0000-00-00 00:00:00'),
(3, 1, 'Feature 3', 'Feature 3', 1, 1, '2017-12-12 11:31:11', 0, '0000-00-00 00:00:00'),
(4, 2, 'car feature', 'car feature', 1, 1, '2017-12-13 09:37:16', 0, '0000-00-00 00:00:00'),
(5, 2, 'car feature 2', 'car feature 2', 1, 1, '2017-12-13 09:38:04', 0, '0000-00-00 00:00:00'),
(6, 3, 'Insurance Feature test', 'Insurance Feature test', 1, 1, '2017-12-13 10:08:38', 1, '2017-12-13 10:08:50'),
(7, 4, 'red', 'احمر', 1, 1, '2017-12-14 09:40:44', 0, '0000-00-00 00:00:00'),
(8, 4, 'blue', 'ازرق', 1, 1, '2017-12-14 09:41:08', 0, '0000-00-00 00:00:00'),
(9, 5, 'day cost in dialy rent', 'سعر اليوم بالتأجير اليومي', 1, 1, '2017-12-14 09:43:47', 0, '0000-00-00 00:00:00'),
(10, 5, 'day cost in 3  days rent', 'سعر اليوم بالتأجير 3 ايام', 1, 1, '2017-12-14 09:44:36', 0, '0000-00-00 00:00:00'),
(11, 5, 'day cost in weekly rent', 'سعر اليوم بالتأجيرالاسبوعي', 1, 1, '2017-12-14 09:45:26', 0, '0000-00-00 00:00:00'),
(12, 5, 'day cost in monhly rent', 'سعر اليوم بالتأجير الشهري', 1, 1, '2017-12-14 09:46:00', 0, '0000-00-00 00:00:00'),
(13, 5, 'day cost in yearly rent', 'سعر اليوم بالتأجيرالسنوي', 1, 1, '2017-12-14 09:46:35', 0, '0000-00-00 00:00:00'),
(14, 5, 'day cost in weekend', 'سعر اليوم في الويكند', 1, 1, '2017-12-14 09:47:19', 0, '0000-00-00 00:00:00'),
(15, 5, '+ one kilometer ', 'سعر الكيلومتر الواحد في الزيادة', 1, 1, '2017-12-14 09:49:31', 0, '0000-00-00 00:00:00'),
(16, 3, 'insurance feature ', 'insurance feature ', 1, 1, '2017-12-14 11:49:36', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_types`
--

CREATE TABLE IF NOT EXISTS `insurance_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `type_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_desc_en` text COLLATE utf8_unicode_ci NOT NULL,
  `type_desc_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>enabled;2=>disabled;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `insurance_types`
--

INSERT INTO `insurance_types` (`type_id`, `company_id`, `type_name_en`, `type_name_ar`, `type_desc_en`, `type_desc_ar`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'insurance type-company 1 test', 'insurance type-company 1 AR', 'insurance type-company 1 test', 'insurance type-company 1 test', 1, 1, '2017-12-13 11:29:18', 1, '2017-12-13 11:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `txt_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `txt_phone` varchar(255) NOT NULL,
  `txt_email` varchar(255) NOT NULL,
  `gender` smallint(6) DEFAULT NULL COMMENT '1:male,2:female',
  `password` varchar(255) NOT NULL,
  `status_flag` smallint(6) DEFAULT NULL COMMENT '1:active,2:not active',
  `date_create` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `provider_id`, `txt_name`, `username`, `txt_phone`, `txt_email`, `gender`, `password`, `status_flag`, `date_create`, `img`, `weight`, `height`) VALUES
(1, 0, 'Mohammed AlDini', 'maldini', '55060102', 'm.aldini@amkwt.com', 2, '123', 1, '2017-12-04 00:00:00', NULL, '110', '183'),
(3, 1, 'customer', 'customer', '55060101', 'cutomer@sys.com', 1, 'MTIz', 1, '2017-12-17 04:41:32', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_title_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_brief_en` text COLLATE utf8_unicode_ci NOT NULL,
  `news_brief_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `news_desc_en` text COLLATE utf8_unicode_ci NOT NULL,
  `news_desc_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `news_date` date NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=>enable;2=>disable;3=>deleted',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title_en`, `news_title_ar`, `news_image`, `news_brief_en`, `news_brief_ar`, `news_desc_en`, `news_desc_ar`, `news_date`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'title', 'title', '', 'cc title', 'cc title', 'cc title', 'cc title', '2017-12-01', 1, 1, '2017-12-14 04:33:26', 1, '2017-12-14 04:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_by` tinyint(4) NOT NULL COMMENT '1=>admin;2=>provider',
  `provider_id` int(11) NOT NULL,
  `car_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_for` tinyint(4) NOT NULL COMMENT '1=>all;2=>provider_customers;3=>traveling|_office;4=>all_customers',
  `offer_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_valid_from` date NOT NULL,
  `offer_valid_to` date NOT NULL,
  `offer_price` decimal(11,3) NOT NULL,
  `price_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>KD;2=>percent',
  `approved` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `status` tinyint(4) NOT NULL COMMENT '1=>enabled;2=>disabled;3=>deleted ',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_by` tinyint(4) NOT NULL COMMENT '1=>admin;2=>provider',
  `provider_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `rent_cost_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_place_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_place_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_time_from` datetime NOT NULL,
  `order_time_to` datetime NOT NULL,
  `order_cost` decimal(11,3) NOT NULL,
  `total_cost` decimal(11,3) NOT NULL,
  `offer_id` int(11) NOT NULL COMMENT '0=>not_offer',
  `commission` decimal(11,3) NOT NULL,
  `commission_type` tinyint(4) NOT NULL COMMENT '1=>KD;2=>percent',
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_by`, `provider_id`, `car_id`, `rent_cost_id`, `customer_id`, `customer_mobile`, `customer_name`, `order_place_from`, `order_place_to`, `order_time_from`, `order_time_to`, `order_cost`, `total_cost`, `offer_id`, `commission`, `commission_type`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 2, 1, 1, 10, 3, '55060101', 'customer', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '123.125', '173.125', 0, '50.000', 1, 1, 1, '2017-12-18 02:31:24', 0, '0000-00-00 00:00:00'),
(2, 2, 1, 1, 10, 3, '55060101', 'customer', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '123.125', '173.125', 1, '50.000', 1, 1, 1, '2017-12-18 02:33:24', 0, '0000-00-00 00:00:00'),
(3, 2, 1, 1, 10, 3, '55060101', 'customer', 'assuit', 'cairo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '123.125', '173.125', 1, '50.000', 1, 1, 1, '2017-12-18 02:51:52', 0, '0000-00-00 00:00:00'),
(4, 2, 1, 1, 10, 3, '55060101', 'customer', 'c', 'c', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '123.125', '124.356', 1, '1.000', 2, 1, 1, '2017-12-18 03:04:19', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE IF NOT EXISTS `providers` (
  `provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `provider_type` tinyint(4) NOT NULL COMMENT '1=Car_Rent;2=Traveling_Office;3=Sales_Representative',
  `provider_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_name_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_description_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_brief_en` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_brief_ar` text COLLATE utf8_unicode_ci NOT NULL,
  `provider_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `provider_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_responsible_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_responsible_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_contract_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_important` tinyint(4) NOT NULL COMMENT '1=>High;2=>Average;3=>Low',
  `featured` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `can_reserve` tinyint(4) NOT NULL COMMENT '1=>yes;0=>no',
  `permission_view` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_add` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_edit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_delete` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL COMMENT '1=>enable;2=>disable;3=>delete',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`provider_id`, `parent_id`, `provider_type`, `provider_email`, `provider_username`, `provider_password`, `provider_name_en`, `provider_name_ar`, `provider_description_en`, `provider_description_ar`, `provider_brief_en`, `provider_brief_ar`, `provider_mobile`, `country_id`, `area_id`, `provider_address`, `provider_lat`, `provider_long`, `provider_logo`, `provider_banner`, `provider_responsible_name`, `provider_responsible_mobile`, `provider_features`, `provider_contract_number`, `provider_important`, `featured`, `can_reserve`, `permission_view`, `permission_add`, `permission_edit`, `permission_delete`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 0, 1, 'provider@sys.com', 'provider', 'MTIz', 'provider name', 'kkkkk', 'llll', 'llll', 'lll', 'llll', '12234587', 1, 2, 'llll', '29.31514119318733', '48.01300048828125', '', '', 'vvvv', '1223', '1', '1', 1, 1, 1, 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 1, 1, '2017-11-23 04:56:49', 1, '2017-12-18 09:41:13'),
(2, 0, 1, 'provider2@system.com', 'provider2', 'MTQ3', 'provider 2 test', 'provider 2 test', 'c test', 'c test', 'c test', 'c test', '', 1, 2, 'provider 2 address test', '29.311660', '47.481766', '', '', 'provider 2 test', '87654321', '1,3', '120', 1, 0, 0, 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 'cars,employees,bills,services,orders,news,offers,customers', 1, 1, '2017-12-12 01:20:42', 1, '2017-12-17 09:11:42'),
(4, 1, 1, 'user@provider.com', 'user', 'MTIz', 'user', 'user', '', '', '', '', '123456', 0, 0, 'user address', '', '', '', '', '', '', '', '', 0, 0, 0, 'cars,employees,bills', 'cars,employees', 'cars,employees', 'cars,employees', 1, 1, '2017-12-17 11:26:24', 1, '2017-12-17 02:41:56'),
(5, 1, 1, 'new@provider.com', 'newuser', 'MTIz', 'newuser', 'newuser', '', '', '', '', '123', 0, 0, 'newuser', '', '', '', '', '', '', '', '', 0, 0, 0, 'cars,employees,bills,services,orders,news,offers,customers', '', '', '', 1, 4, '2017-12-17 12:03:01', 4, '2017-12-17 12:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `provider_mobiles`
--

CREATE TABLE IF NOT EXISTS `provider_mobiles` (
  `mobile_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `mobile_type` tinyint(4) NOT NULL COMMENT '1=>mobile;2=>telephone',
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`mobile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `provider_mobiles`
--

INSERT INTO `provider_mobiles` (`mobile_id`, `provider_id`, `mobile_type`, `mobile_number`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, '12345678', 2, 1, '2017-12-12 02:01:04', 1, '2017-12-12 03:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `provider_social`
--

CREATE TABLE IF NOT EXISTS `provider_social` (
  `social_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `social_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`social_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `provider_social`
--

INSERT INTO `provider_social` (`social_id`, `provider_id`, `social_name`, `social_link`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'instagram', 'http://127.0.0.1/car_rent/systemRent/providers/provider_socials.php?provider_id=1', 1, 1, '2017-12-12 02:25:55', 1, '2017-12-12 02:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `uname` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `flag_type` smallint(6) DEFAULT NULL COMMENT '1:admin,2:receptionist',
  `flag_status` smallint(6) DEFAULT NULL COMMENT '1:active,2:delete',
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `uname`, `password`, `flag_type`, `flag_status`, `date_create`) VALUES
(1, 'administrator', 'admin', 'admin', 1, 1, '2017-12-04 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
