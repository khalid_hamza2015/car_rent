/*
SQLyog Community v8.6 GA
MySQL - 5.6.17 : Database - rent_car
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `categories` */

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `status_flag` smallint(6) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order_no` int(11) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

/*Table structure for table `contact` */

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `location_en` text,
  `location_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `contact` */

insert  into `contact`(`id`,`phone`,`mobile`,`fax`,`email`,`longitude`,`latitude`,`location_en`,`location_ar`) values (1,'','+965 ','','','','','','');

/*Table structure for table `content` */

CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `desc_en` text,
  `desc_ar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `content` */

insert  into `content`(`id`,`name_en`,`name_ar`,`img`,`video`,`desc_en`,`desc_ar`) values (1,'About Us','من نحن','','','\r\n','');
insert  into `content`(`id`,`name_en`,`name_ar`,`img`,`video`,`desc_en`,`desc_ar`) values (2,'Terms and Conditions','الشروط والأحكام','','','','');
insert  into `content`(`id`,`name_en`,`name_ar`,`img`,`video`,`desc_en`,`desc_ar`) values (3,'Supporters','الدعم','','','','');

/*Table structure for table `country_city` */

CREATE TABLE `country_city` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(100) NOT NULL,
  `name_ar` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `sort_id` int(11) DEFAULT NULL,
  `status_flag` smallint(6) DEFAULT '1' COMMENT '1:active,2:disable,3:del',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `country_city` */

insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (1,'Kuwait','الكويت',0,0,1);
insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (2,'Kuwait City','مدينة الكويت',1,NULL,1);
insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (3,'Farwaniyah','الفروانية',1,NULL,1);
insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (4,'Hawalli','حولي',1,NULL,1);
insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (5,'Saudi Arabia','السعودية',0,0,1);
insert  into `country_city`(`id_country`,`name_en`,`name_ar`,`parent_id`,`sort_id`,`status_flag`) values (6,'Riyadh','الرياض',5,NULL,1);

/*Table structure for table `members` */

CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `txt_phone` varchar(255) NOT NULL,
  `txt_email` varchar(255) NOT NULL,
  `gender` smallint(6) DEFAULT NULL COMMENT '1:male,2:female',
  `password` varchar(255) NOT NULL,
  `status_flag` smallint(6) DEFAULT NULL COMMENT '1:active,2:not active',
  `date_create` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `members` */

insert  into `members`(`id`,`txt_name`,`username`,`txt_phone`,`txt_email`,`gender`,`password`,`status_flag`,`date_create`,`img`,`weight`,`height`) values (1,'Mohammed AlDini','maldini','55060102','m.aldini@amkwt.com',2,'123',1,'2017-12-04 00:00:00',NULL,'110','183');

/*Table structure for table `shop` */

CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(100) NOT NULL,
  `name_ar` varchar(256) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` text,
  `description_ar` text,
  `image` varchar(100) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `tele` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `flag_type` varchar(10) DEFAULT NULL COMMENT '1:Get Active,2:Eat Better,3:Shop',
  `status_flag` smallint(6) NOT NULL DEFAULT '1' COMMENT '1:active,2:not active,3:delete',
  `date_create` datetime NOT NULL,
  `date_update` varchar(20) DEFAULT NULL,
  `user_create` int(11) DEFAULT '0',
  `user_update` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `shop` */

insert  into `shop`(`id`,`name_en`,`name_ar`,`email`,`username`,`password`,`description`,`description_ar`,`image`,`banner`,`sort`,`tele`,`mobile`,`instagram`,`website`,`flag_type`,`status_flag`,`date_create`,`date_update`,`user_create`,`user_update`) values (1,'Techbox','تيكبوكس','m.a@amkw.com','techbox','P@ssw0rd','test','test','5536logo_xs.jpg','3931Banner02.jpg',0,'64641361','123456','https://www.instagram.com/mo_aldini/','www.google.com','1,2',1,'2017-12-04 00:00:00','2017-12-05 16:30:47',0,1);
insert  into `shop`(`id`,`name_en`,`name_ar`,`email`,`username`,`password`,`description`,`description_ar`,`image`,`banner`,`sort`,`tele`,`mobile`,`instagram`,`website`,`flag_type`,`status_flag`,`date_create`,`date_update`,`user_create`,`user_update`) values (2,'Aswar Almubarkiah','أسوار المباركية','info@amkwt.com','amkwt','123456','test','test','9534portfolio09.jpg','',0,'123654','','','','1,2,3',1,'2017-12-06 10:10:24','',1,0);

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `uname` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `flag_type` smallint(6) DEFAULT NULL COMMENT '1:admin,2:receptionist',
  `flag_status` smallint(6) DEFAULT NULL COMMENT '1:active,2:delete',
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`uname`,`password`,`flag_type`,`flag_status`,`date_create`) values (1,'administrator','admin','admin',1,1,'2017-12-04 00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
