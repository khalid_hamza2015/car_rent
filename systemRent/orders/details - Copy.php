<?php 
    require_once("../module/include_mod.php");

    if(! isset($_GET['order_id'])){header('Location:index.php');}
    $order_id       = $_GET['order_id'];

    $order_query    = $dbh->query("SELECT * FROM `orders` WHERE `order_id`='{$order_id}' ");
    
    if($order_query->rowCount()>0){
        $order  = $order_query->fetch();
    
    
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
	<?php include("../module/header_mod.php");?><!--header end-->
    <?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="index.php">Orders</a></li>
                    <li class="active">Order details</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">            	
                        <h3>Order# <?php echo $_GET['order_id']?></h3>                        
                        <table class="table table-hover general-table">
                            <tbody>
                                <tr>
                                    <th style="width:20%">Order Date</th>
                                    <td><?php echo date('Y-m-d',strtotime($order['created_at']));?></td>
                                </tr>                                
                                <tr>
                                    <th>Status</th>
                                    <td><?php 
                                    switch($order['status']) { 
                                        case 0: echo "Pending"; break;
                                        case 1: echo "Confirmed"; break;
                                        case 2: echo "Delivered"; break;
                                        case 3: echo "Cancelled"; break;
                                    }?></td>
                                </tr>                                                                                    
                            </tbody>
                        </table>                            
                        <div class="gap gap-small"></div>
                        <h3>Order Information</h3>
                        <table class="table table-hover general-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Car details</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Qunatity</th>
                                    <th>Price</th>
                                    <th>Total</th>                                    
                                </tr>
                            </thead>
                            <tbody>                            
                            </tbody>                          
                        </table>
                        <!--  ADDRESS -->
                        <h3>Customer Information</h3>
                        <div style="clear:both"></div>
                        <div class="form-group col-md-12" style="padding:0 2px;">
                            <label class="col-md-2">Name</label>
                            <div class="col-md-4"><?php echo $order['customer_name']?></div>
                        </div>                        
                        <div style="clear:both"></div>
                        <div class="form-group col-md-12" style="padding:0 2px;">
                            <label class="col-md-2">Mobile</label>
                            <div class="col-md-4"><?php echo $order['customer_mobile'];?></div>
                        </div>                                             
                    </div>
                    </div>
        		</section>
            </div>
        </div>
    </section>
 </section>
</section>
<?php include("../module/footer_mod.php");?>
</body>
<?php } ?>
</html>