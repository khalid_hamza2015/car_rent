<?php 
    require_once("../module/include_mod.php");

    if(! isset($_GET['order_id'])){header('Location:index.php');}
    $order_id       = $_GET['order_id'];

    $order_query    = $dbh->query("SELECT * FROM `orders` WHERE `order_id`='{$order_id}' ");
    
    if($order_query->rowCount()>0){
        $order  = $order_query->fetch();
        $car_id = $order['car_id'];    

        $car_query  = $dbh->query("SELECT *,
                                    b.cat_name_en as car_brand,
                                    c.cat_name_en as car_type,
                                    e.cat_name_en as car_cat
                                    FROM `cars` a
                                    INNER JOIN `car_brands` b ON(b.cat_id=a.car_brand)
                                    INNER JOIN `car_brands` c ON(c.cat_id=a.car_type)
                                    INNER JOIN `car_brands` e ON(e.cat_id=a.car_cat)
                                    INNER JOIN `car_traffic` d ON(d.car_id=a.car_id)
                                    WHERE a.car_id ='{$car_id}'
                            ");
        if($car_query->rowCount() > 0){
            $car      = $car_query->fetch();       
        }
    
    
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                        <li><a href="index.php">Orders</a></li>
                        <li class="active">Order details</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <div class="panel-body invoice">
                            <div class="invoice-header"> 
                                <div class="invoice-title col-md-3 col-xs-2">
                                    <h1>invoice</h1>
                                    <img class="logo-print" src="images/bucket-logo.png" alt="">
                                </div>
                                <div class="invoice-info col-md-9 col-xs-10">

                                    <div class="pull-right">                                        
                                        <div class="col-md-12 col-sm-12 pull-right">
                                            <p>
                                                Phone: <?php echo $order['customer_mobile']?> <br>
                                                Email : <?php echo $order['customer_email']?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row invoice-to">
                                <div class="col-md-4 col-sm-4 pull-left">
                                    <h4>Invoice To:</h4>
                                    <h2>Envato</h2>
                                    <p>
                                        Phone: <?php echo $order['customer_mobile']?> <br>
                                        Email : <?php echo $order['customer_email']?>
                                    </p>
                                </div>
                                <div class="col-md-4 col-sm-5 pull-right">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 inv-label">Invoice #</div>
                                        <div class="col-md-8 col-sm-7"><?php echo $order_id;?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 inv-label">Date #</div>
                                        <div class="col-md-8 col-sm-7"><?php echo date('d M Y',strtotime($order['created_at']));?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 inv-label">Status #</div>
                                        <div class="col-md-8 col-sm-7">
                                        <?php 
                                            switch($order['status']) { 
                                                case 0: echo "Pending"; break;
                                                case 1: echo "Confirmed"; break;
                                                case 2: echo "Delivered"; break;
                                                case 3: echo "Cancelled"; break;
                                            }
                                        ?>
                                        </div>
                                    </div>                                
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 inv-label">
                                            <h3>TOTAL DUE</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <h1 class="amnt-value"><?php echo $order['total_cost'].' KD';?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table class="table table-invoice" > 
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Car Details</th>
                                    <th class="text-center">Rent type</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($car)):?>    
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <h4><?php echo $car['car_brand'].' - '.$car['car_type'].' - '.$car['car_cat'];?></h4>
                                        <p>
                                            Engine : <?php echo $car['car_engine']."<br>";?>
                                            Gear : <?php echo $car['car_gear']."<br>";?>
                                            Model : <?php echo $car['car_model']."<br>";?>
                                            Plate Number : <?php echo $car['car_plate'];?>
                                        </p>
                                    </td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">4</td>
                                    <td class="text-center">$1300.00</td>
                                </tr>
                                <?php endif;?>                                
                                </tbody>
                            </table>


                        </div>
                    </section>  
                </div>
            </div>
        </section>
    </section>
</section>
<?php include("../module/footer_mod.php");?>
</body>
<?php } ?>
</html>