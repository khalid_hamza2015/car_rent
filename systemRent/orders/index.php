<?php 
    require_once("../module/include_mod.php");
    $status     = (isset($_GET['status']) ? $_GET['status'] : 1);

    // switch ($status) {
    //     case 0:
    //         $bc     = 'Pending orders';
    //         break;
    //     case 1:
    //         $bc     = 'Confirmed orders';
    //         break;
    //     case 2:
    //         $bc     = 'Delivered orders';
    //         break;
    //     case 3:
    //         $bc     = 'Cancelled orders';
    //         break;            
        
    // }
    
?>

<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
    <?php include("../module/header_mod.php");?><!--header end-->
    <?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                        <li class="active"><?php echo 'ordres';//$bc;?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
        				<div class="panel-body">
                            <div class="adv-table editable-table ">
                                <div class="space15"></div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <?php if($_SESSION['flag_type'] == 1):?>
                                            <th>Provider</th>
                                            <?php endif; ?>
                                            <th>Customer</th>
                                            <th>Status</th>                                            
                                            <th>amount</th>                                            
                                            <th>Details</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        // check if users is admin or provider and build query
                                        if($_SESSION['flag_type'] == 2){
                                            $where = " WHERE a.provider_id='{$login_provider}'"; //  AND `status`='{$status}'                                         
                                        }

                                        elseif($_SESSION['flag_type'] == 1){                                        
                                            $where = "  INNER JOIN `providers` b ON(b.provider_id=a.provider_id)";//  WHERE a.status='{$status}' 
                                        }                                        

            							$stmt = $dbh->query("SELECT *,a.status as order_status FROM `orders` a $where");

            							$stmt->execute();
            							if($stmt->rowCount() > 0) {
                                            $ceq = 0;
            							    while($rec = $stmt->fetch()) {
                                                $id    = $rec['order_id'];
                                                $order_status   = $rec['order_status'];
                                                                                                
                                    ?>
                                    <tr>

                                        <td><?php echo ++$ceq; ?></td>
                                        <?php if($_SESSION['flag_type'] == 1):?>
                                        <td><?php echo $rec['provider_name_en']?></td>
                                        <?php endif;?>
                                        <td><?php echo $rec['customer_name']?></td>                                                                
                                        <td>
                                            <select name="order_status" class="form-control" order-id="<?php echo $id;?>" <?php if($status == 3) echo 'disabled'?> >
                                                <option value="1" <?php if($order_status == 1) echo 'selected';?>> Confirmed </option>
                                                <option value="2" <?php if($order_status == 2) echo 'selected';?>> Delivered </option>
                                                <option value="3" <?php if($order_status == 3) echo 'selected';?>> Calcelled </option>
                                            </select>
                                        </td>
                                        <td><?php echo number_format($rec['total_cost'],3).' KD';?></td>
                                        <td><a href="details.php?order_id=<?php echo $id;?>" class="btn btn-info">Details</a></td>                                    
                                    </tr>
                                    <?php 
                                            } // while
        							    } // if
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
    $(document).ready(function(){            

        // COMMISSION
        $('body').on('change','select[name=order_status]',function(){
            var value     = $(this).val();
            var order_id  = $(this).attr('order-id');            
            answer  = confirm("Are You Sure ? ");
            if (answer) {
                $.ajax({
                    url:"orderController.php",
                    type:"post",
                    data:{"order_id":order_id,"status":value},
                    success:function(response){                      
                        location.reload();
                    }
                });
                return false; 
            };
        });
        
    
    });
</script>
</body>
</html>