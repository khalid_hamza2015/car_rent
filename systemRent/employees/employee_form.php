<?php 
	ob_start();
	require_once("../module/include_mod.php");
	require_once("../car_rent_permisssion.php");
	
	$id 			= 0; if(isset($_GET['id'])) $id = $_GET['id'];	

	if($id == 0 && ! in_array('employees', $permission_add)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}
	elseif($id > 0 && ! in_array('employees', $permission_edit)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}

	$view_permessions 	= array();
	$add_permessions 	= array();
	$edit_permessions 	= array();
	$delete_permessions = array();

	if($id > 0){
		$action = 'update';			
		$bc 	= 'updade employee';
		
		// EMPLOYEE  
		$employee_query = $dbh->query("SELECT * FROM `providers`																
									WHERE `provider_id`='{$id}'
								");
		if ($employee_query->rowCount()>0) {
			$employee 			= $employee_query->fetch();	
			$add_permessions    = explode(',',$employee['permission_add']);
            $edit_permessions   = explode(',',$employee['permission_edit']);
            $view_permessions   = explode(',',$employee['permission_view']);
            $delete_permessions = explode(',',$employee['permission_delete']); 		
		}

	}else{
		$action = 'add';
		$bc 	= 'add employee';


	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">employee</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <div class="form-tab">
			                            	<form action="employeeController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">										  													    	

			                            		<!-- EMIAL -->
			                                    <div class="form-group">
													<label class="col-lg-2 control-label">Email</label>
													<div class="col-md-4">
														<input type="email" name="email" class="form-control" value="<?php echo @$employee['provider_email'];?>" required>
													</div>
												</div>   

												<!-- USERNAME  -->
			                                    <div class="form-group">
													<label class="col-lg-2 control-label">Username</label>
													<div class="col-md-4">
														<input type="text" name="username" class="form-control" value="<?php echo @$employee['provider_username'];?>" required>
													</div>
												</div>

												<!-- PASSWORD  -->
												<?php if($id == 0){?>
			                                    <div class="form-group">
													<label class="col-lg-2 control-label">Password</label>
													<div class="col-md-4">
														<input type="password" name="password" class="form-control" value="" required>
													</div>
												</div>
												<?php } ?>
												
												<!-- NAME EN -->
												<div class="form-group">
													<label class="col-lg-2 control-label">English Name</label>
													<div class="col-md-4">
														<input type="text" name="name_en" class="form-control required" value="<?php echo @$employee['provider_name_en'];?>" required>
													</div>
												</div>

												<!-- NAME AR -->
			                                    <div class="form-group">
													<label class="col-lg-2 control-label">Arabic Name</label>
													<div class="col-md-4">
														<input type="text" name="name_ar" class="form-control" value="<?php echo @$employee['provider_name_ar'];?>" required>
													</div>
												</div>	

												<!-- ADDRESS -->
												<div class="form-group">
													<label class="col-lg-2 control-label">Address</label>
													<div class="col-md-4">
														<input type="text" name="address" class="form-control" value="<?php echo @$employee['provider_address'];?>" />
													</div>
												</div>

												<!-- MOBILE -->
												<div class="form-group">
													<label class="col-lg-2 control-label">Mobile</label>
													<div class="col-md-4">
														<input type="text" name="mobile" class="form-control" value="<?php echo @$employee['provider_mobile'];?>" />
													</div>
												</div>


				                                <div class="row form-group">
				                                    <label class="col-lg-2 control-label">Permissions</label>
				                                    <div class="col-lg-6">
				                                    <?php foreach ($car_rent_permisssion as $key => $permission) { ?>
			                                            <div class="permissions row">
			                                                <label class="col-md-2"><?php echo $permission ?></label>
			                                                <div class="col-md-10">  
			                                                    <!-- View -->
			                                                    <div class="col-md-3">
			                                                        <div style="float:left;padding-right:10px" align="left">
			                                                            <input type="checkbox"  data="<?php echo 'view_'.$permission?>" data-target="<?php echo $permission?>" name="<?php echo $permission.'_view'?>" <?php if($id > 0 && in_array($permission,$view_permessions)) echo 'checked';?> >
			                                                        </div>
			                                                        <div style="float:left" align="left">View</div> 
			                                                    </div>                                                    
			                                                    <!-- Add -->
			                                                    <div class="col-md-3  <?php echo $permission.'_action'?>" id="prem_actions">
			                                                        <div style="float:left;padding-right:10px" align="left">
			                                                            <input type="checkbox" name="<?php echo $permission.'_add'?>" <?php if($id > 0 && in_array($permission,$add_permessions)) echo 'checked';?> >
			                                                        </div>
			                                                        <div style="float:left" align="left">Add</div> 
			                                                    </div>
			                                                    <!-- Edit -->
			                                                    <div class="col-md-3  <?php echo $permission.'_action'?>" id="prem_actions">
			                                                        <div style="float:left;padding-right:10px" align="left">
			                                                            <input type="checkbox" name="<?php echo $permission.'_edit'?>" <?php if($id > 0 && in_array($permission,$edit_permessions)) echo 'checked';?> >
			                                                        </div>
			                                                        <div style="float:left" align="left">Edit</div> 
			                                                    </div>
			                                                    <!-- Delete -->
			                                                    <div class="col-md-3  <?php echo $permission.'_action'?>" id="prem_actions">
			                                                        <div style="float:left;padding-right:10px" align="left">
			                                                            <input type="checkbox"name="<?php echo $permission.'_delete'?>" <?php if($id > 0 && in_array($permission,$delete_permessions)) echo 'checked';?> >
			                                                        </div>
			                                                        <div style="float:left" align="left">Delete</div> 
			                                                    </div> 
			                                                </div>
			                                            </div>
				                                    <?php    }?>
				                                    </div>
				                                </div>
												
												<div class="form-group">
				                                	<label class="col-lg-2 control-label"></label>
				                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
				                                    <div class="col-lg-6">
				                                    	<button class="btn btn-primary pull-right" type="submit" name="provider_user" value="<?php echo $action; ?>">Submit</button>
				                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
				                                    </div>
				                                </div>

					                                    
			                            	</form>
										</div>											 
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				email:{required:true,email:true},
				username:"required",
				password:"required",
				name_en:"required",
				name_ar:"required",
				address:"required",
				mobile:{required:true,number:true}				
			}
		});

		$("input[data^=view]").each(function(){                       
            if ($(this).is(':checked')) {
                var name = $(this).attr("data-target");                
                $('.'+name+'_action').removeClass('hide');             
            }else{
            	var name = $(this).attr("data-target");                
                $('.'+name+'_action').addClass('hide');
            }
        });

        $('body').on('change','input[data^=view]',function(){
            var name = $(this).attr("data-target");                
            if ($(this).is(':checked')) {
                $('.'+name+'_action').removeClass('hide');
            }else{
                $('.'+name+'_action').addClass('hide');
                $('input[name^='+name+']').attr('checked',false);
            }
        });

	});
</script>
</body>
</html>