<?php 
    require_once("../module/include_mod.php");    
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active">employees</li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <?php if(in_array('employees', $permission_add)){?>
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="employee_form.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                    <?php } ?>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <?php if(in_array('employees', $permission_view)){?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>                                       
                                        <th>Name</th>                                        
                                        <th>Username</th>                                        
                                        <th>Email</th>
                                        <th>Created at</th>                                        
                                        <th>Action</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php                                   
        							$stmt = $dbh->query("SELECT * FROM `providers`
                                                            WHERE parent_id ='{$login_provider}'
                                                            AND `status` < 3
                                                        ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['provider_id'];                                                                                                                                                            
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>                                                                        
                                    <td><?php echo $rec['provider_name_en']?></td>                                    
                                    <td><?php echo $rec['provider_username']?></td>
                                    <td><?php echo $rec['provider_email']?></td> 
                                    <td><?php echo date('Y-m-d',strtotime($rec['created_at']));?></td> 
                                    <td>
                                    <?php if(in_array('employees', $permission_edit)){?>                                       
                                        <a href="change_password.php?id=<?php echo $id;?>" class="btn btn-success">Change Password</a>
                                        <a href="<?php echo 'employee_form.php?id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="providers" table-id="provider_id" >Disable</a>
                                    <?php } elseif($rec['status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="providers" table-id="provider_id" >Enable</a>
                                    <?php }}if(in_array('employees', $permission_delete)){?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="providers" table-id="provider_id">Delete</a> 
                                    <?php } ?>
                                    </td>                                                                    
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                            <?php } // permission_view ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.commission').submit(function(e){
            e.preventDefault; 
            var data    = $(this).serialize();                
            $.ajax({
                url:'carController.php',
                type:'post',
                data:data,
                success:function(response){
                    location.reload();
                }
            });
            return false
        });
    });
</script>
</body>
</html>