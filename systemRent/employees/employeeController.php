<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');	
	
	$now 		= date('Y-m-d h:i:s');	
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	// GET PARENT ID
	if(! isset($_SESSION['parent_id'])) $_SESSION['parent_id'] 	= 0;
	$parent_id 	= $_SESSION['parent_id'];

	if($parent_id > 0){
		$login_provider 	= $parent_id;
	}else{
		$login_provider 	= $login_id;
	}
	
	// GET PROVIDER TYPE 
	$provider_type 	= $_SESSION['permission'];

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';

	if(isset($_POST)){		

		/***** CHANGE PASSWORD  STRAT *****/
		if (isset($_POST['change_password'])):
			$id  		= $_POST['id'];
			$password 	= base64_encode($_POST['password']);
			$change 	= $dbh->query(" UPDATE `providers` 
											SET 
												`provider_password`='{$password}' 
											WHERE 
												`provider_id`='{$id}'
										");
			if($change){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Password Changed Successfuly';
			}															
			header("Location:index.php");		
		/***** CHANGE PASSWORD  STRAT *****/


		/***** ADD & UPDATE PROVIDER USER  STRAT *****/
		elseif(isset($_POST['provider_user'])):				
			// GET POST DATA					
			$email 			= $_POST['email'];
			$username 		= $_POST['username'];
			$password 		= base64_encode($_POST['password']);			
			$name_en   		= $_POST['name_en'];
			$name_ar   		= $_POST['name_ar'];			
			$address 		= $_POST['address'];
			$mobile 		= $_POST['mobile'];
			$id 			= $_POST['id'];

			/***** Permessions Start *****/
	    	$add 	= array();
	    	$edit 	= array();
	    	$view 	= array();
	    	$delete = array();    	    	

	    	foreach ($_POST as $key => $value) {    		
	    		$check = explode('_',$key);

	    		if (@$check[1] == 'add') {    			
	    		    $add[]   	= $check[0];				
	    		}

	    		if (@$check[1] == 'edit') {
	    		    $edit[]   	= $check[0];				
	    		}

	    		if (@$check[1] == 'delete') {
	    		    $delete[]   	= $check[0];				
	    		}

	    		if (@$check[1] == 'view') {
	    		    $view[]   	= $check[0];				
	    		}    			

	    	}
	    	$add_permessions 	= implode(',', $add);
	    	$edit_permessions 	= implode(',', $edit);
	    	$delete_permessions = implode(',', $delete);
	    	$view_permessions 	= implode(',', $view);
	    	/***** Permessions End *****/ 						
			
			// UNIQUE USERNAME
			$uniqu 	= $dbh->query("SELECT * FROM `providers` 
										WHERE `provider_username` 	='{$username}'
										AND `status`<3
										AND `provider_id`!='{$id}'
									");
			if($uniqu->rowCount()>0){
				$_SESSION['sql_status_icon'] ='fa-exclamation';
				$_SESSION['sql_status_msg']  ='ERROR : Username already taken';										
				header('Location:index.php');
				break;
			}			
			
			/*** ADD STRAT ***/
			if($_POST['provider_user'] == 'add'){


				$add 	= $dbh->query("INSERT INTO `providers` SET
											`parent_id`						='{$login_provider}',
											`provider_type`					='{$provider_type}',		
											`provider_email` 				='{$email}',
											`provider_username` 			='{$username}',
											`provider_password` 			='{$password}',											
											`provider_name_en` 				='{$name_en}',
											`provider_name_ar` 				='{$name_ar}',											
											`provider_address` 		    	='{$address}',
											`provider_mobile`				='{$mobile}',										
											`permission_view`				='{$view_permessions}',
											`permission_add`				='{$add_permessions}',
											`permission_edit`				='{$edit_permessions}',
											`permission_delete`				='{$delete_permessions}',
											`status` 						=1,
											`created_by`					='{$login_id}',
											`created_at`					='{$now}'
										");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='user added Successfuly';
				}						 				
			}
			/*** ADD END ***/

			/*** UPDATE START ***/
			else{				
				$update = $dbh->query("UPDATE `providers` SET 
											`provider_email` 			='{$email}',
											`provider_username` 		='{$username}',																				
											`provider_name_en` 			='{$name_en}',
											`provider_name_ar` 			='{$name_ar}',											
											`provider_address` 		   	='{$address}',
											`provider_mobile`			='{$mobile}',										
											`permission_view`			='{$view_permessions}',
											`permission_add`			='{$add_permessions}',
											`permission_edit`			='{$edit_permessions}',
											`permission_delete`			='{$delete_permessions}',
											`updated_by`				='{$login_id}',
											`updated_at`				='{$now}'
											WHERE 
											`provider_id`				='{$id}'
										");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='user updated Successfuly';
				}
			}
			/*** UPDATE END ***/		

			header('Location:index.php');

		/***** ADD & UPDATE PROVIDER USER  END *****/
		
		endif;
	} // IF ISSET POST
?>
