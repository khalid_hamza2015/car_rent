<?php
	ob_start();
	require_once("../module/include_mod.php");	
	
	if(isset($_GET['id'])){
		$id = $_GET['id'];

		if($id == 0 && ! in_array('employees', $permission_add)) {
			header('Location:../main/main.php');
			$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
		}
		elseif($id > 0 && ! in_array('employees', $permission_edit)) {
			header('Location:../main/main.php');
			$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
		}

?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                     <li><a href="index.php">employee</a></li>
		                	<li class="active">Change Password</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="providerController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">

		                                    <div class="form-group">
												<label class="col-lg-2 control-label">New Password</label>
												<div class="col-md-4">
													<input type="password" id="password" name="password" class="form-control" value="" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-2 control-label">Confirm Password</label>
												<div class="col-md-4">
													<input type="password" name="con_password" class="form-control" value="" required>
												</div>
											</div>

			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                    <div class="col-lg-4">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="change_password" value="Submit">
			                                    	<a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				password:"required",
				con_password:{equalTo: "#password"}
			}
		});
	});
</script>
</body>
<?php } // if isset GET[id] ?>
</html>