<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
	<?php include("../module/header_mod.php");  include("../module/left_menu_mod.php");?>
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                     <li>Vendors Management</li>
                	<li class="active">Reset Password</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <?php if($_SESSION["flag_type"]<>4) {?><div class="clearfix" style="padding-bottom:10px"><div class="btn-group"><a class="btn btn-primary" href="index.php">Back <i class="fa fa-backward"></i></a></div></div><?php }?>
                            <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa fa-check"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                        <li class="pull-right notification-time"></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php }?>
                            <div class="space15"></div><?php 
							$name_en = '';
							$name_ar = '';
							$email = '';
							$username = '';
							$password = '';
							$description = '';
							$description_ar = '';
							$image = '';
							$banner = '';
							$sort = '';
							$tele = '';
							$mobile = '';
							$instagram = '';
							$website = '';
							$flag_type = '';
							$cate = array();
							$status_flag = '';
							$date_create = '';
							$date_update = '';
							$user_create = '';
							$user_update = '';
							$id = (! empty($_REQUEST['id'])?$_REQUEST['id']:0);
							$stmt = $dbh->prepare("SELECT * FROM `shop` WHERE `id` = {$id};");
							$stmt->execute();
							if($stmt->rowCount() > 0) {
							  	$rec = $stmt->fetch();
								$name_en = $rec['name_en'];
								$name_ar = $rec['name_ar'];
								$email = $rec['email'];
								$username = $rec['username'];
								$password = $rec['password'];
								$description = $rec['description'];
								$description_ar = $rec['description_ar'];
								$image = $rec['image'];
								$banner = $rec['banner'];
								$sort = $rec['sort'];
								$tele = $rec['tele'];
								$mobile = $rec['mobile'];
								$instagram = $rec['instagram'];
								$website = $rec['website'];
								$flag_type = $rec['flag_type'];
								$cate = explode(',',$flag_type);
								$status_flag = $rec['status_flag'];
								$date_create = $rec['date_create'];
								$date_update = $rec['date_update'];
								$user_create = $rec['user_create'];
								$user_update = $rec['user_update'];
							}?>
                            <form action="code_password.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                            	
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Name <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="name_en" class="form-control required" disabled value="<?php echo $name_en?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">New Password <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="" name="new_password" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<label class="col-lg-2 col-md-2 control-label"></label>
                                    <div class="col-lg-6"><button class="btn btn-primary" type="submit">Submit</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
<!--right sidebar end-->
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
</body>
</html>