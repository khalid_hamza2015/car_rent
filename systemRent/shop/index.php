<?php require_once("../module/include_mod.php");?>
    <link href="../js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="../js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                <li>Vendors Management</li>
                <li class="active">Vendors List</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="form_contract.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                    <div class="adv-table editable-table ">
                        <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                        <div class="alert alert-success">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul><p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
                        <div class="alert alert-danger">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul><p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="adv-table">
                         <table class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Function</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$stmt = $dbh->prepare("SELECT * FROM `shop` WHERE status_flag < 3;");
							$stmt->execute();
							if($stmt->rowCount() > 0) {
							  while($rec = $stmt->fetch()) {?>
                            <tr style=" <?php if($rec['status_flag']==2) echo 'background-color:red !important;color:#FFF !important'; else echo 'background-color:#FFF !important;color:#000 !important';?>">
                                <td><?php echo $rec['id']?></td>
                                <td><?php echo $rec['name_en']?></td>
                                <td><?php echo $rec['email']?></td>
                                <td><?php
								$function = array();
								$function = explode(',',$rec['flag_type']);
								if(in_array(1, $function)) {//Get Active?>
								<li><span>Get Active</span></li>
								<?php }?>
								<?php if(in_array(2, $function)) {//Eat Better?> 
								<li><span>Eat Better</span></li>
								<?php }?>
								<?php if(in_array(3, $function)) {//Shop?> 
								<li><span>Shop</span></li>
								<?php }?></td>
                                <td>
                                    <a href="form_contract.php?id=<?php echo $rec['id']?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status_flag']==1) {?>
                                    <a href="javascript: return void(0)" class="btn btn-success btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=2&table=shop')">Make Disable</a>
                                    <?php } elseif($rec['status_flag']==2) {?>
                                    <a href="javascript: return void(0)" class="btn btn-danger btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=1&table=shop')">Make Enable</a>
                                    <?php }?>
                                    <a href="password.php?id=<?php echo $rec['id']?>" class="btn btn-primary btn-sm">Reset Password</a>
                                    <a href="javascript: return void(0)" class="btn btn-default btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=3&table=shop')">Delete forever</a>
                                </td>
                            </tr>
                            <?php }
							}?>
                            </tbody>
                        </table> 
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<script src="../js/jquery.js"></script>
<script src="../bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="../js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>

<script type="text/javascript" language="javascript" src="../js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/data-tables/DT_bootstrap.js"></script>
<script src="../js/scripts.js"></script>
<script src="../js/dynamic_table_init.js"></script>
</body>
</html>