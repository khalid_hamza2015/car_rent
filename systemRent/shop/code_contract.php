<?php 
define('INCLUDE_CHECK',true);
require_once('../../function.php');
$uploaddir    = '../../shop/'; 
$user_id = (! empty($_SESSION['rent_id'])?$_SESSION['rent_id']:0);
                        
$id = (isset($_GET['id'])?$_GET['id']:0);
$notice = (isset($_POST['notice'])?$_POST['notice']:0);
$name_en = (! empty($_POST['name_en'])?$_POST['name_en']:"");
$name_ar = (! empty($_POST['name_ar'])?$_POST['name_ar']:"");
$email = (! empty($_POST['email'])?$_POST['email']:"");
$username = (! empty($_POST['username'])?$_POST['username']:"");
$password = (! empty($_POST['password'])?$_POST['password']:"");
$flag_type = (isset($_POST['flag_type'])?implode(',',$_POST['flag_type']):"");
$tele = (isset($_POST['tele'])?$_POST['tele']:"");
$description = (! empty($_POST['description'])?$_POST['description']:"");
$description_ar = (! empty($_POST['description_ar'])?$_POST['description_ar']:"");
$website = (! empty($_POST['website'])?$_POST['website']:"");
$instagram = (! empty($_POST['instagram'])?$_POST['instagram']:"");
$mobile = (! empty($_POST['mobile'])?$_POST['mobile']:"");
//----------------------------------------------------------------------
$Hfile=(! empty($_POST['Hfile'])?$_POST['Hfile']:"");
	
if(! empty($_POST['chkdel'])) {
	unlink($uploaddir.$Hfile);	
	$Hfile='';
}
$image_name   = $Hfile;
$image        = $_FILES["file_upload"]["name"];
$uploadedfile = $_FILES['file_upload']['tmp_name'];
if($image)  {
  $filename = stripslashes($image);
  $extension = end(explode('.', $image));
  $extension = strtolower($extension);
  if($extension == "") {
	echo ' Unknown Image extension ';
  } else {
	$image_name = rand(0000,9999).$image;
	copy($uploadedfile,$uploaddir.$image_name);
  }
}
//----------------------------------------------------------------------
$Hfile2 = (! empty($_POST['Hfile2'])?$_POST['Hfile2']:"");
if(! empty($_POST['chkdel2'])) {
	unlink($uploaddir.$Hfile2);	
	$Hfile2='';
}
$banner = $Hfile2;
$image = $_FILES["file_upload2"]["name"];
$uploadedfile = $_FILES['file_upload2']['tmp_name'];
if($image)  {
  $filename = stripslashes($image);
  $extension = end(explode('.', $image));
  $extension = strtolower($extension);
  if($extension == "") {
	echo ' Unknown Image extension ';
  } else {
	$banner = rand(0000,9999).$image;
	copy($uploadedfile,$uploaddir.$banner);
  }
} 						
//----------------------------------------------------------------------
if(trim($name_en)<>"") {
	if($id==0 ) {
		$stmt = $dbh->prepare(" INSERT INTO `shop` SET 
												`name_en` = :name_en , 
												`name_ar` = :name_ar , 
												`email` = :email , 
												`username` = :username , 
												`password` = :password , 
												`description` = :description , 
												`description_ar` = :description_ar , 
												`image` = :image , 
												`banner` = :banner , 
												`sort` = 0 , 
												`tele` = :tele , 
												`mobile` = :mobile , 
												`instagram` = :instagram , 
												`website` = :website , 
												`flag_type` = :flag_type , 
												`status_flag` = 1 , 
												`date_create` = NOW() , 
												`date_update` = '' , 
												`user_create` = :user_create , 
												`user_update` = '' ; ");
		$stmt->bindParam(':name_en', $name_en);
		$stmt->bindParam(':name_ar', $name_ar);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':tele', $tele);
		$stmt->bindParam(':description', $description);
		$stmt->bindParam(':description_ar', $description_ar);
		$stmt->bindParam(':image', $image_name);
		$stmt->bindParam(':banner', $banner);
		$stmt->bindParam(':mobile', $mobile);
		$stmt->bindParam(':instagram', $instagram);
		$stmt->bindParam(':website', $website);
		$stmt->bindParam(':flag_type', $flag_type);
		$stmt->bindParam(':user_create',$user_id);
		if (!$stmt->execute()) {
			print_r($st->errorInfo()); exit;
		}
	} elseif($id>0) {
		$stmt = $dbh->prepare(" UPDATE `shop` SET 
							    			`name_en` = :name_en , 
											`name_ar` = :name_ar , 
											`email`          = :email , 
											`username`       = :username , 
											`description`    = :description , 
											`description_ar` = :description_ar , 
											`image`          = :image , 
											`banner`         = :banner , 
											`sort`           = 0 , 
											`tele`           = :tele , 
											`mobile`         = :mobile , 
											`instagram`      = :instagram , 
											`website`        = :website , 
											`flag_type`      = :flag_type , 
											`date_update`    = NOW() , 
											`user_update`    = :user_create 
								WHERE `id` = :id ");
		$stmt->bindParam(':name_en', $name_en);
		$stmt->bindParam(':name_ar', $name_ar);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':tele', $tele);
		$stmt->bindParam(':description', $description);
		$stmt->bindParam(':description_ar', $description_ar);
		$stmt->bindParam(':image', $image_name);
		$stmt->bindParam(':banner', $banner);
		$stmt->bindParam(':mobile', $mobile);
		$stmt->bindParam(':instagram', $instagram);
		$stmt->bindParam(':website', $website);
		$stmt->bindParam(':flag_type', $flag_type);
		$stmt->bindParam(':user_create',$user_id);
		$stmt->bindParam(':id',$id, PDO::PARAM_INT);
		$stmt->execute();
	}
}?>
<script language="javascript" type="text/javascript">document.location='<?php if($_SESSION["flag_type"]==4) echo "form_contract.php?done=1&id=".$id; else echo "index.php?done=1";?>'</script>