<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
	<?php include("../module/header_mod.php");  include("../module/left_menu_mod.php");?>
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                     <li>Vendors Management</li>
                	<li class="active">Add/Edit</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <?php if($_SESSION["flag_type"]<>4) {?><div class="clearfix" style="padding-bottom:10px"><div class="btn-group"><a class="btn btn-primary" href="index.php">Back <i class="fa fa-backward"></i></a></div></div><?php }?>
                            <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa fa-check"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                        <li class="pull-right notification-time"></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php }?>
                            <div class="space15"></div><?php 
							$name_en = '';
							$name_ar = '';
							$email = '';
							$username = '';
							$password = '';
							$description = '';
							$description_ar = '';
							$image = '';
							$banner = '';
							$sort = '';
							$tele = '';
							$mobile = '';
							$instagram = '';
							$website = '';
							$flag_type = '';
							$cate = array();
							$status_flag = '';
							$date_create = '';
							$date_update = '';
							$user_create = '';
							$user_update = '';
							$id = (! empty($_REQUEST['id'])?$_REQUEST['id']:0);
							$stmt = $dbh->prepare("SELECT * FROM `shop` WHERE `id` = {$id};");
							$stmt->execute();
							if($stmt->rowCount() > 0) {
							  	$rec = $stmt->fetch();
								$name_en = $rec['name_en'];
								$name_ar = $rec['name_ar'];
								$email = $rec['email'];
								$username = $rec['username'];
								$password = $rec['password'];
								$description = $rec['description'];
								$description_ar = $rec['description_ar'];
								$image = $rec['image'];
								$banner = $rec['banner'];
								$sort = $rec['sort'];
								$tele = $rec['tele'];
								$mobile = $rec['mobile'];
								$instagram = $rec['instagram'];
								$website = $rec['website'];
								$flag_type = $rec['flag_type'];
								$cate = explode(',',$flag_type);
								$status_flag = $rec['status_flag'];
								$date_create = $rec['date_create'];
								$date_update = $rec['date_update'];
								$user_create = $rec['user_create'];
								$user_update = $rec['user_update'];
							}?>
                            <form action="code_contract.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                            	
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">English Title <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="name_en" class="form-control required" value="<?php echo $name_en?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Arabic Title <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="name_ar" class="form-control" value="<?php echo $name_ar?>" required>
                                    </div>
                                </div>
                                <?php if($_SESSION["flag_type"]<>4) {?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Function</label>
                                    <div class="col-md-2"><label class="checkbox"><input type="checkbox" <?php if(in_array(1, $cate)) echo 'checked="checked"';?> value="1" name="flag_type[]" /> Get Active</label></div>
                                    <div class="col-md-2"><label class="checkbox"><input type="checkbox" <?php if(in_array(2, $cate)) echo 'checked="checked"';?> value="2" name="flag_type[]" /> Eat Better</label></div>
                                    <div class="col-md-2"><label class="checkbox"><input type="checkbox" <?php if(in_array(3, $cate)) echo 'checked="checked"';?> value="3" name="flag_type[]" /> Shop</label></div>
                                </div>
								<?php } else {?>
                                <input type="hidden" name="flag_type" value="<?php echo $flag_type?>">
                                <?php }?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Email <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="email" name="email" class="form-control" value="<?php echo $email?>" required>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Username <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="username" id="username" class="form-control" value="<?php echo $username?>" required>
                                        <span id="availability"></span>
                                    </div>
                                </div>
                                <?php if($id==0) {?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Password <span class="required">*</span></label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="" name="password" class="form-control" required>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Telephone</label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="tele" class="form-control" value="<?php echo $tele?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Mobile</label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="mobile" class="form-control" value="<?php echo $mobile?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-lg-2 col-md-2 control-label">Logo:</label>
                                  <div class="col-lg-6">
                                    <input type="hidden" name="Hfile" value="<?php echo $image;?>">
                                    <?php if((trim($image)) and (file_exists("../../shop/".$image))) {?>
                                        <a href="<?php echo "../../shop/".$image;?>" target="_new" style="text-decoration:underline">Show Image</a>
                                        <div class="clearfix"></div>
                                        <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel"></div>
                                        <div style="width:90% !important;float:left" align="left">delete file.</div>
                                        <div class="clearfix"></div>
                                    <?php }?>
                                    <input type="file" name="file_upload" <?php if($id ==0) echo 'required';?>>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-lg-2 col-md-2 control-label">Banner:</label>
                                  <div class="col-lg-6">
                                    <input type="hidden" name="Hfile2" value="<?php echo $banner;?>">
                                    <?php if((trim($banner)) and (file_exists("../../shop/".$banner))) {?>
                                        <a href="<?php echo "../../shop/".$banner;?>" target="_new" style="text-decoration:underline">Show Banner</a>
                                        <div class="clearfix"></div>
                                        <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel2"></div>
                                        <div style="width:90% !important;float:left" align="left">delete file.</div>
                                        <div class="clearfix"></div>
                                    <?php }?>
                                    <input type="file" name="file_upload2">
                                  </div>
                                </div>
								
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">English Desc</label>
                                    <div class="col-md-9"><textarea name="description" class="form-control" rows="10" required><?php echo $description?></textarea></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Arabic Desc</label>
                                    <div class="col-md-9"><textarea name="description_ar" class="form-control" rows="10" required><?php echo $description_ar;?></textarea></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Website</label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="website" class="form-control required" value="<?php echo $website?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-md-2 control-label">Instagram</label>
                                    <div class="col-lg-5 col-md-6">
                                        <input type="text" name="instagram" class="form-control" value="<?php echo $instagram?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                	<label class="col-lg-2 col-md-2 control-label"></label>
                                    <div class="col-lg-6"><button class="btn btn-primary" type="submit" id="register" <?php if($id==0) echo "disabled"?>>Submit</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        </section>
    </section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
<!--right sidebar end-->
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script>  
 $(document).ready(function(){  
   $('#username').blur(function(){

     var username = $(this).val();

     $.ajax({
      url:'check.php',
      method:"POST",
      data:{user_name:username,id:<?php echo $id?>},
      success:function(data)
      {
       if(data != '0')
       {
        $('#availability').html('<span class="text-danger">Username not available</span>');
        $('#register').attr("disabled", true);
       }
       else
       {
        $('#availability').html('<span class="text-success">Username Available</span>');
        $('#register').attr("disabled", false);
       }
      }
     })

  });
 });  
</script>
</body>
</html>