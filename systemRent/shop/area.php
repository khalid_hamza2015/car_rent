<?php require_once("../module/include_mod.php");?>
</head>
<body>
<section id="container">
<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<?php 
$datasetShop = $classObj->getTableDataByID("shop",$login_id);
if($datasetShop) {
	$recShop  = mysql_fetch_array($datasetShop);
	if((isset($_GET['del'])) and (isset($_GET['id'])) and ($_GET['del']==1)) {
		mysql_query(" DELETE FROM `shop_areas` WHERE `id` = '{$_GET['id']}' ;");
	}?>
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-sm-12">
			<?php if((isset($_REQUEST['update'])) and ($_REQUEST['update']==1)) {?>
            <div class="alert alert-success ">
                <span class="alert-icon"><i class="fa fa-check"></i></span>
                <div class="notification-info">
                    <ul class="clearfix notification-meta">
                        <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                        <li class="pull-right notification-time"></li>
                    </ul>
                    <p><br></p>
                </div>
            </div>
            <?php }?>
            <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
            <div class="alert alert-success ">
                <span class="alert-icon"><i class="fa fa-check"></i></span>
                <div class="notification-info">
                    <ul class="clearfix notification-meta">
                        <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                        <li class="pull-right notification-time"></li>
                    </ul>
                    <p><br></p>
                </div>
            </div>
            <?php }?>
            <header class="panel-heading"><?php echo $recShop['name_en']?> - Areas</header>        
            <section class="panel">
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <div class="clearfix" style="padding-bottom:10px">
                            <form class="cmxform form-horizontal" id="signupForm" method="post" enctype="multipart/form-data" action="add_area.php">
                                <div class="form-group">
                                    <div class="col-lg-3">
                                      <select name="area_id" class="form-control" required>
                                           <option value="">Please Select Area</option><?php 
                                            $datasetmainCategory = $classObj->checkByTableNameWithCondition("areas"," `status_flag` = 1 ORDER BY `name_en`;");
                                            while($area = mysql_fetch_array($datasetmainCategory)) {
												$dsChk = mysql_query(" SELECT * FROM `shop_areas` WHERE `shop_id` = '{$login_id}' AND `area_id` = {$area['id']};");
				                            	if(mysql_num_rows($dsChk) == 0)  {?>
                                                <option value="<?php echo $area['id']?>"><?php echo $area['name_en']?></option><?php 
												}
                                            }?>
                                      </select>
                                  	</div>
                                    <div class="col-lg-3"><input type="number" name="price" step="any" class="form-control" placeholder="Delivery Charge / KWD" required /> </div>
                                    
                                    <div class="col-lg-6"><button class="btn btn-primary" type="submit">Save</button></div>
                                </div>
                            </form>
                        </div>
                        <div class="space15"></div>
                            <table class="table table-striped table-hover">
                                <thead>
                                	<tr>
                                        <th>Area</th>
                                        <th>Delivery Charge</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody><?php 
								$dataset = mysql_query(" SELECT B.`name_en` , A.`charge_delivery` , A.`id` FROM `shop_areas` A 
														 INNER JOIN `areas` B ON (A.`area_id` = B.`id`) 
														 WHERE A.`shop_id` = '{$login_id}';");
                            	if(mysql_num_rows($dataset)>0)  {
								while($r = mysql_fetch_array($dataset)) {?>
                                <tr>
                                    <td><?php echo $r[0]?></td>
                                    <td><?php echo $r[1]." KWD"?></td>
                                    <td><a class="btn btn-danger" href="javascript: return void(0)" onClick="javascript: confirmDel('area.php?id=<?php echo $r[2]?>&del=1')">Delete</a></td>
                                </tr>
                                <?php }//End Loop
								}//end if($dataset)?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </section>
</section>
<?php }?>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>
</html>