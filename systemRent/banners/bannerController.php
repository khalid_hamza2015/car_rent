<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	$uploaddir  ='../../images/banners/';
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){					
		
		// GET POST DATA			
		$title_en   	= $_POST['title_en'];
		$title_ar   	= $_POST['title_ar'];
		$url   			= $_POST['url'];
		$desc_en   		= $_POST['desc_en'];
		$desc_ar   		= $_POST['desc_ar'];		
		$id 			= $_POST['id'];	

		/* LOGO */			
		$Hfile 		='';
		if(isset($_REQUEST['Hfile']))
			$Hfile = $_REQUEST['Hfile'];
			
		if(isset($_REQUEST['chkdel'])) {
			unlink($uploaddir.$Hfile);	
			$Hfile='';
		}

		$image_name   = $Hfile;
		$tempimage    = $_FILES['file_upload']['name'];
		$image        = $_FILES["file_upload"]["name"];
		$uploadedfile = $_FILES['file_upload']['tmp_name'];

		if($image)  {
		  	$filename = stripslashes($image);
		  	$extension = end(explode('.', $image));
		  	$extension = strtolower($extension);
		  	if($extension == "") {
				echo ' Unknown Image extension ';
		  	} else {
				$image_name = rand(0000,9999).$image;
				copy($uploadedfile,$uploaddir.$image_name);
		  	}
		} 							
		/* LOGO */			
			
		/***** ADD *****/
		if(isset($_POST['add'])){			
			$add 	= $dbh->query("INSERT INTO `banners` SET
										`banner_title_en`	='{$title_en}',
										`banner_title_ar`	='{$title_ar}',
										`banner_url` 		='{$url}',
										`banner_image` 		='{$image_name}',
										`banner_desc_en`	='{$desc_en}',
										`banner_desc_ar`	='{$desc_ar}',
										`status` 			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");			
			if($add){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Banner added successfuly';
			}

		} // add

		/***** UPDATE *****/
		elseif ($_POST['update']) {		
			$update = $dbh->query("UPDATE `banners` SET										
										`banner_title_en`	='{$title_en}',
										`banner_title_ar`	='{$title_ar}',
										`banner_url` 		='{$url}',
										`banner_image` 		='{$image_name}',
										`banner_desc_en`	='{$desc_en}',
										`banner_desc_ar`	='{$desc_ar}',
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`banner_id`			='{$id}'
									");
			if($update){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Banner updated successfuly';
			}

		}//update		

		header('Location:index.php');
		
	} // IF ISSET POST
?>
