<?php 
	require_once("../module/include_mod.php");
	$path 	= "../../images/banners/";

	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

		$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

		if($id > 0){
			$action = 'update';			
			$bc 	= 'updade banner';
			
			// BANNER 
			$banner_query 	= $dbh->query("SELECT * FROM `banners` WHERE `banner_id`='{$id}'");
			if ($banner_query->rowCount()>0) {
				$banner 	= $banner_query->fetch();		
			}

		}else{
			$action = 'add';
			$bc 	= 'add banner';


		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">banners</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="bannerController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">														                            

			                            	<!-- TITLE EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Title</label>
												<div class="col-md-4">
													<input type="text" name="title_en" class="form-control required" value="<?php echo @$banner['banner_title_en'];?>" required>
												</div>
											</div>

											<!-- TITLE AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Title</label>
												<div class="col-md-4">
													<input type="text" name="title_ar" class="form-control " value="<?php echo @$banner['banner_title_ar'];?>" >
												</div>
											</div>

											<!-- URL -->
											<div class="form-group">
												<label class="col-lg-2 control-label">URL</label>
												<div class="col-md-4">
													<input type="url" name="url" class="form-control " value="<?php echo @$banner['banner_url'];?>" >
												</div>
											</div>

											 <!-- IMAGE -->
		                                    <div class="form-group">
		                                      	<label class="col-lg-2 control-label">Image</label>
		                                      	<div class="col-lg-6">
			                                        <input type="hidden" name="Hfile" value="<?php echo @$banner['banner_image'];?>">
			                                        <?php if((trim(@$banner['banner_image'])) and (file_exists($path.@$banner['banner_image']))) {?>
			                                            <a href="<?php echo $path.@$banner['banner_image'];?>" target="_new" style="text-decoration:underline">Show Image</a>
			                                            <div class="clearfix"></div>
			                                            <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel"></div>
			                                            <div style="width:90% !important;float:left" align="left">delete file.</div>
			                                            <d="clearfix"><iv class/div>
			                                        <?php }?>
			                                        <input type="file" name="file_upload">
		                                      	</div>
		                                    </div>

											<!-- DESCRIPTION EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_en" class="form-control" rows="4" required><?php echo @$banner['banner_desc_en'];?></textarea></div>
			                                </div>

			                                <!-- DESCRIPTION AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_ar" class="form-control" rows="4" ><?php echo @$banner['banner_desc_ar'];;?></textarea></div>
			                                </div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">			                                	
			                                    <div class="col-lg-11">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				title_en:"required",
				url:{required:true,url:true},
				desc_en:"required"
			}
		});
	});
</script>
</body>
</html>