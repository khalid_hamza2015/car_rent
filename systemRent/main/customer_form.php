<?php 
	require_once("../module/include_mod.php");
	if (! in_array('orders', $permission_add)) {
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
		header('Location:../main/main.php');
	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li class="active">add customer</li>		                  		                	
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div> 
			                            <!-- ALERT -->
			                            <?php if(isset($_SESSION['sql_status_msg'])){?>
			                            <div class="alert alert-success">
			                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
			                                <div class="notification-info">
			                                    <ul class="clearfix notification-meta">
			                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
			                                    </ul><p><br></p>
			                                </div>
			                            </div>
			                            <?php 
			                        		unset($_SESSION['sql_status_msg']);
			                        		}
			                        	?>                   
			                            <form action="orderController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">														                            

											<!-- NAME -->
											<div class="form-group">
												<label class="col-lg-2 control-label"> Name</label>
												<div class="col-md-4">
													<input type="text" name="name" class="form-control required" required>
												</div>
											</div>

											<!-- NAME -->
											<div class="form-group">
												<label class="col-lg-2 control-label"> Mobile</label>
												<div class="col-md-4">
													<input type="text" name="mobile" class="form-control required" required>
												</div>
											</div>

			                            	<!-- EMIAL -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Email</label>
												<div class="col-md-4">
													<input type="email" name="email" class="form-control required" required>
												</div>
											</div>   

											<!-- USERNAME  -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Username</label>
												<div class="col-md-4">
													<input type="text" name="username" class="form-control required" required>
												</div>
											</div>

											<!-- PASSWORD  -->											
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Password</label>
												<div class="col-md-4">
													<input type="password" name="password" class="form-control required" value="" required>
												</div>
											</div>

											<!-- GENDER -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Type</label>
												<div class="col-md-4">
													<select name="type" class="form-control" required>
														<option value="">Please select gender</option>
														<option value="1">Male</option>
														<option value="2">Female</option>														
													</select>													
												</div>
											</div>
											
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                				                                			                                
			                                    <div class="col-lg-4">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="add_customer" value="Submit">
			                                    	<a href="main.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				name:"required",
				email:{required:true,email:true},
				mobile:{required:true,number:true},
				username:"required",
				password:"required",
				type:"required"
			}
		});
	});
</script>
</body>
</html>