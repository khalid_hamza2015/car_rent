<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');	
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();
	$errors 	= array();

	// GET PARENT ID
	if(! isset($_SESSION['parent_id'])) $_SESSION['parent_id'] 	= 0;
	$parent_id 	= $_SESSION['parent_id'];

	if($parent_id > 0){
		$login_provider 	= $parent_id;
	}else{
		$login_provider 	= $login_id;
	}

	if($_SESSION['flag_type'] == 1){
		$login_provider 	= 0;
	}

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){

		/*****  SEARCH ABOUT CUSTOMER STRAT *****/
		if(isset($_POST['search'])){
			// GET POST DATA
			$mobile 	= $_POST['mobile'];

			$search 	= $dbh->query("SELECT * FROM `members` 
										WHERE `txt_phone`='{$mobile}' 
										AND `provider_id`='{$login_provider}' 
									");
			if($search->rowCount() > 0){
				$customer 	= $search->fetch();
				header('Location:order_form.php?customer_id='.$customer['id']);die();
			}else{
				$_SESSION['sql_status_msg'] = "Customer not found please fill out below fields";
				header('Location:customer_form.php');die();		
			}
		}		
		/*****  SEARCH ABOUT CUSTOMER END *****/



		/*****  ADD CUSTOMER STRAT *****/
		elseif(isset($_POST['add_customer'])){		
			// GET POST DATA
			$name 		= $_POST['name'];
			$mobile 	= $_POST['mobile'];
			$email 		= $_POST['email'];
			$username 	= $_POST['username'];
			$password 	= base64_encode($_POST['password']);
			$type 		= $_POST['type'];

			// UNIQUE USERNAME
			$uniqu 	= $dbh->query("SELECT * FROM `members` 
										WHERE `username` ='{$username}'
										AND `status_flag`<3
										AND `provider_id`='{$login_provider}'
									");
			if($uniqu->rowCount()>0){
				$_SESSION['sql_status_icon'] = 'fa-exclamation';
				$_SESSION['sql_status_msg']  = 'ERROR : Username already taken';
				$errors['username']			 = 'ERROR : Username already taken'; 
				header('Location:customer_form.php');die();
			}

			// UNIQUE MOBILE
			$uniqu 	= $dbh->query("SELECT * FROM `members` 
										WHERE `txt_phone` ='{$mobile}'
										AND `status_flag` <3
										AND `provider_id` ='{$login_provider}'										
									");
			if($uniqu->rowCount()>0){
				$_SESSION['sql_status_icon'] = 'fa-exclamation';
				$_SESSION['sql_status_msg']  = 'ERROR : Mobile number already taken';
				$errors['mobile']			 = 'ERROR : Mobile number already taken'; 
				header('Location:customer_form.php');die();
			}

			if(count($errors) == 0){
				$add 	= $dbh->query("INSERT INTO `members` SET 
										`provider_id`	='{$login_provider}',
										`txt_name`		='{$name}',
										`username`		='{$username}',
										`txt_phone`		='{$mobile}',
										`txt_email`		='{$email}',
										`gender`		='{$type}',
										`password`		='{$password}',
										`status_flag`	=1,
										`date_create`	='{$now}'	
									");
				if($add){
					$customer_id = $dbh->lastInsertId();
					header('Location:order_form.php?customer_id='.$customer_id);die();
				}else{
					header('Location:customer_form.php');die();
				}
			}
		}	
		/*****  ADD CUSTOMER END *****/

		/*****  GET CAR INFO STRAT *****/		
		elseif(isset($_POST['car_info'])){

			$result 	= array(); 
			// GET POST DATA
			$car_id 	= $_POST['car_id'];

			// GET CAR FEATURES & COMMISIION
			$features	 	= "";
			$car_features 	= array();
			$commission 	= 0;
			$commission_type= 0;
			$car_query 		= $dbh->query("SELECT * FROM `cars` WHERE `car_id`='{$car_id}'");
			if($car_query->rowCount() > 0){
				$car 		= $car_query->fetch();
				$car_features	= explode(',', $car['car_features']);
				$commission 	= $car['commission_value'];
				$commission_type= $car['commission_type'];
			}

			foreach ($car_features as $rec) {
				$feature_query = $dbh->query("SELECT * FROM `features` WHERE `feature_id`='{$rec}'");
				if($feature_query->rowCount() > 0){
					$feature 	= $feature_query->fetch();
					$features  .= "<div class='col-md-12'><i class='fa fa-check'></i>".$feature['feature_name_en']."</div>";
				}
			}										

			// CHECK IF CAR HAS OFFER
			$car_offer 	= $dbh->query("SELECT * FROM `offers` WHERE `offer_for`!=3 ");
			if($car_offer->rowCount() > 0){
				$offer 	= $car_offer->fetch();
				$offer_id 		= $offer['offer_id'];
				$offer_price 	= $offer['offer_price'];
				$price_type 	= $offer['price_type'];
			}else{
				$offer_id		= 0;
				$offer_price	= 0;
				$price_type 	= 0;
			}

			// GET CARS RENT PRICES
			$types 	= "";
			$rent_type 	= $dbh->query("SELECT * FROM `car_rent_cost` a 
										INNER JOIN `features` b ON(b.feature_id=a.rent_type)
										WHERE a.car_id='{$car_id}'
									");
			if($rent_type->rowCount() > 0){
				while($rec = $rent_type->fetch()){
					// GET PRICE AFTER OFFER
					if($offer_id > 0){
						if($price_type == 1){
							$price = $rec['rent_cost'] - $offer_price;
						}
						else{
							$price = $rec['rent_cost'] *(1-($offer_price/100));	
						}
					}else{
						$price = $rec['rent_cost'];
					}

					$types .="<label class='radio-inline col-md-12'>";
					$types .="<input name='type' type='radio' value='".$rec['rent_type'].'_'.$price."' ><span class='type_name'>".$rec['feature_name_en']."</span>";
					
					if($offer_id > 0){
						$types .="<span class='price old'>".$rec['rent_cost']." KD</span>";
						$types .="<span class='price new'>".$price." KD</span>";
					}
					else{
						$types .="<span class='price new'>".$rec['rent_cost']." KD</span>";
					}
														
				}
			}


			// RETURN RESULR
			$result['commission']		= $commission;
			$result['commission_type'] 	= $commission_type;
			$result['features']			= $features;
			$result['offer_id']			= $offer_id;
			$result['types']			= $types;

			echo json_encode($result);

		}	
		/*****  GET CAR INFO END *****/

		/*****  ADD ORDER STRAT *****/
		elseif(isset($_POST['add_order'])){			
			// GET POST DATA	
			$car 				= $_POST['car'];
			$customer_id 		= $_POST['customer_id'];
			$offer_id 			= $_POST['offer_id'];
			$commission 		= $_POST['commission'];
			$commission_type 	= $_POST['commission_type'];
			$status 			= $_POST['status'];
			$type 				= $_POST['type'];
			$type 				= explode('_', $type);
			$rent_cost_id 		= $type[0];
			$order_cost 		= $type[1];		

			$delivery_service 		= (isset($_POST['delivery_service']) 	? $_POST['delivery_service'] : 0);
			$delivery_with_driver 	= (isset($_POST['delivery_with_driver'])? $_POST['delivery_with_driver'] : 0);
			$delivery_to_airport 	= (isset($_POST['delivery_to_airport']) ? $_POST['delivery_to_airport'] : 0);

			$place_from 			= (isset($_POST['place_from']) ? $_POST['place_from'] : '');
			$place_to 				= (isset($_POST['place_to'])   ? $_POST['place_to'] : '');
			$time_from 				= (isset($_POST['time_from'])  ? $_POST['time_from'] : '');
			$time_to 				= (isset($_POST['time_to'])    ? $_POST['time_to'] : '');

			if($commission_type == 1){
				$total_cost = $order_cost + $commission;
			}
			else{
				$total_cost = $order_cost + ($order_cost *($commission/100));	
			}

			// CUSTOMER DETAILS
			$c_name 		= '';
			$c_mobile 		= '';
			$c_email 		= '';
			$customer_query = $dbh->query("SELECT * FROM `members` WHERE `id`='{$customer_id}' ");
			if($customer_query->rowCount() > 0){
				$customer 	= $customer_query->fetch();
				$c_name 	= $customer['txt_name'];
				$c_mobile 	= $customer['txt_phone'];
				$c_email 	= $customer['txt_email'];			
			}


			if($_SESSION['flag_type'] == 1){
				$order_by 	= 1;
			}else{
				$order_by 	= 2;
			}

			$add = $dbh->query("INSERT INTO `orders` SET 
									`order_by` 				='{$order_by}',
									`provider_id`			='{$login_provider}',
									`car_id`				='{$car}',
									`rent_cost_id`			='{$rent_cost_id}',
									`customer_id`			='{$customer_id}',
									`customer_mobile`		='{$c_mobile}',
									`customer_name`			='{$c_name}',
									`customer_email`		='{$c_email}',
									`order_place_from`		='{$place_from}',
									`order_place_to`		='{$place_to}',
									`order_time_from`		='{$time_from}',
									`order_time_to`			='{$time_to}',
									`order_cost`			='{$order_cost}',
									`total_cost`			='{$total_cost}',
									`offer_id`				='{$offer_id}',
									`commission`			='{$commission}',
									`commission_type`		='{$commission_type}',
									`delivery_service`		='{$delivery_service}',
									`delivery_to_airport`	='{$delivery_to_airport}',
									`delivery_with_driver`	='{$delivery_with_driver}'
									`status`				='{$status}',
									`created_by`			='{$login_id}',
									`created_at`			='{$now}'			
								");	
			if($add){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='orders added Successfuly';
			}
			
			// REDIRECT
			header('Location:main.php');		

		}
		/*****  ADD ORDER STRAT *****/	

	} // IF ISSET POST
?>
