<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<?php 
    if(isset($_GET['cart_id'])){
        $cart_id = $_GET['cart_id'];

        $cart_query = $dbh->query("SELECT * FROM `cart_items` WHERE `id`='{$cart_id}' ");
        if($cart_query->rowCount()>0){
            $cart       = $cart_query->fetch();
            $item_id    = $cart['item_id'];
            $order_id   = $cart['order_id'];
            $member_id  = $cart['member_id'];

            $total      = 0;            
?>
<body>
<section id="container">
	<?php include("../module/header_mod.php");?><!--header end-->
    <?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                        <li class="active">Orders Details</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <div class="panel-body"> 
                            
                            <!-- check if item is class  -->
                            <h3>Oreder Information</h3>
                            <?php                                
                                if($cart['cart_type'] == 1){
                                    $class_query = $dbh->query("SELECT * FROM `courses` WHERE `id`='{$item_id}' ");
                                    if($class_query->rowCount()>0){
                                        $class   = $class_query->fetch();
                            ?>
                            <table class="table table-bordered table-responsive cart_summary">
                                <thead>
                                    <tr>
                                        <th style="width:20%">Class</th>
                                        <th style="width:35%">Product</th>                                        
                                        <th style="text-align:center !important;width:10%">Price</th>
                                        <th style="text-align:center !important;width:10%">Qty</th>
                                        <th style="text-align:center !important;width:10%">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php if((trim($class['img'])) and (file_exists("../../course_img/".$class['img']))) {?><img src="<?php echo "../../resize/".$class['img']?>" style="height:100px"><?php }?></td>
                                        <td>
                                            <p><?php echo $class['name_en']?></p>
                                            <p><i class="fa fa-calendar"></i> <?php echo "<strong> From </strong>".date("d F Y",strtotime($class['date_course']))?></p>
                                            <p><i class="fa fa-calendar"></i> <?php echo "<strong> To </strong>".date("d F Y",strtotime($class['date_course_to']))?></p>
                                            <p><i class="fa fa-clock-o"></i> <?php echo date("H:i",strtotime($class['start_time']))." - ".date("H:i",strtotime($class['end_time']))?></p>
                                        </td>
                                        <td style="text-align:center !important"><?php echo $class['price']?></td>
                                        <td style="text-align:center !important"><?php echo $cart['qty']?></td>
                                        <td style="text-align:center !important"><?php echo ($cart['qty']*$class['price'])?></td>
                                    </tr>
                                </tbody>
                            </table> 
                            <?php } // if class_query->rowcount()>0 ?>


                            <!-- PARENT DETAILS -->
                            <?php 
                                $parent_query = $dbh->query("SELECT * FROM `cart_contacts`
                                                                WHERE `cart_id`='{$cart_id}'
                                                                AND `type`=1
                                                            ");
                                if($parent_query->rowCount()>0){
                                    $parent     = $parent_query->fetch();
                            ?>
                            <h3>Parent Information</h3>
                            <table class="table table-bordered table-responsive cart_summary">
                                <thead>                                    
                                    <tr>
                                        <th>Name</th>
                                        <th>Relation</th>                                        
                                        <th>Mobile</th>
                                        <th>Emergency Name</th>
                                        <th>Emergency Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $parent['name'];?></td>
                                        <td><?php echo $parent['relation'];?></td>
                                        <td><?php echo $parent['mobile'];?></td>
                                        <td><?php echo $parent['emergency_name'];?></td>
                                        <td><?php echo $parent['emergency'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } // if parent_query->rowCount()>0 ?>
                            <!-- PARENT DETAILS -->


                            <!-- KIDS DETAILS -->
                            <?php 
                                $kids_query = $dbh->query("SELECT * FROM `cart_contacts`
                                                                WHERE `cart_id`='{$cart_id}'
                                                                AND `type`=2
                                                            ");
                                if($kids_query->rowCount()>0){
                                    $kids     = $kids_query->fetchall();
                            ?>
                            <h3>Kids Information</h3>
                            <table class="table table-bordered table-responsive cart_summary">
                                <thead>                                    
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Birthday</th>                                        
                                        <th>Age</th>
                                        <th>Allergies</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $ceq = 0;
                                        foreach ($kids as $kid) {                                                                                
                                    ?>
                                    <tr>
                                        <td><?php echo ++$ceq;?></td>
                                        <td><?php echo $kid['name'];?></td>
                                        <td><?php echo $kid['birthday'];?></td>
                                        <td><?php echo $kid['age'];?></td>
                                        <td><?php echo $kid['allegries'];?></td>
                                    </tr>
                                    <?php } // forech kids ?>
                                </tbody>
                            </table>
                            <?php        

                                } // if kids_query->rowCount()>0
                            ?>
                            <!-- KIDS DETAILS -->


                            <!-- item is rent -->
                            <?php                                    
                                } // if cart_type == 1
                                else{ 
                                    $ddl_size   = $cart['ddl_size'];
                                    $rent_query = $dbh->query("SELECT * FROM `product` WHERE `id` ='{$item_id}'");
                                    
                                    if($rent_query->rowCount()>0){
                                        $rent   = $rent_query->fetch();
                                        
                                        $product_item_q = $dbh->query("SELECT * FROM `product_items` WHERE `id` ='{$ddl_size}' ");
                                        if($product_item_q->rowCount()>0){
                                            $product_item   = $product_item_q->fetch();

                                            $total += number_format($cart['qty']*$product_item['price'],3);                                            
                                        
                            ?>
                            <table class="table table-bordered table-responsive cart_summary">
                                <thead>
                                    <tr>
                                        <th>Rent</th>
                                        <th>Product</th>                                        
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php if((trim($rent['img'])) and (file_exists("../../items_img/".$rent['img']))) {?><img src="<?php echo "../../resize/".$rent['img']?>" style="height:100px"><?php }?></td>
                                        <td>
                                            <p><?php echo "<strong>Name</strong>  ".$rent['name_en']?></p>
                                            <p><?php echo "<strong>Date/Time</strong>  ".date("d F Y",strtotime($cart['book_date']))." / ".date("H:i",strtotime($cart['from_time']))." - ".date("H:i",strtotime($cart['to_time']))?></p>
                                            <p><?php echo "<strong>".$product_item['label_eng']."</strong> ".$product_item['size']?></p>
                                            <p>
                                                <?php 
                                                    $dsArea = $dbh->prepare("SELECT * FROM `areas` WHERE `id` = {$cart['area_id']};");
                                                    $dsArea->execute();
                                                    if($dsArea->rowCount()) {
                                                    $rec_area = $dsArea->fetch();
                                                    echo "<strong>Location</strong>  ".$rec_area['name_en'];
                                                }?>
                                            </p>
                                        </td>                                        
                                        <td><?php echo number_format($product_item['price'],3)?></td>
                                        <td><?php echo $cart['qty'];?></td>
                                        <td><?php echo number_format($cart['qty']*$product_item['price'],3)?></td>
                                    <tr>
                                </tbody>
                            </table>       	
                            <?php   } // if product_item_q->rowcount > 0 ?>
                            

                            <!-- ADD ON INFORMATION  -->
                            <?php 
                                $add_on_query = $dbh->query("SELECT * FROM `cart_addon` c_a
                                                                INNER JOIN `add_on` a_o ON(a_o.addon_id=c_a.addon_id)
                                                                WHERE c_a.cart_item_id ='{$cart_id}'
                                                            ");
                                if($add_on_query->rowCount()>0){
                                    $adds   = $add_on_query->fetchall();                                    
                            ?>
                            <h3>Addon Information</h3>
                            <table class="table table-bordered table-responsive cart_summary">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Add On</th>                                        
                                        <th>Qunatity</th>
                                        <th>Price</th> 
                                        <th>Total</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $adds_counter = 0;
                                        foreach ($adds as $add) {
                                            $total += number_format($add['price']*$add['qty'],3);                                            
                                    ?>
                                    <tr>
                                        <td><?php echo ++$adds_counter;?></td>
                                        <td><?php echo $add['name_en'];?></td>
                                        <td><?php echo $add['qty'];?></td>
                                        <td><?php echo $add['price'];?></td>
                                        <td><?php echo number_format($add['price']*$add['qty'],3);?></td>
                                    </tr>
                                    <?php } // foreach adds ?>
                                </tbody>
                            </table>
                            <?php } // if add_on query->rowcount > 0 ?>
                            <!-- ADD ON INFORMATION  --> 

                           <!-- MAP -->
                           <h3>Location Information</h3>
                           <div class="map">
                                <iframe
                                    width="100%"
                                    height="400px"
                                    frameborder="0" style="border:0"
                                    src="http://maps.google.com/maps?q=<?php echo $cart['latitude'].','.$cart['longitude']?>&z=13&output=embed" allowfullscreen>
                                </iframe> 
                            </div>
                            <!-- MAP -->


                            <?php                                
                                    } // if rent_query->rowcount>0
                                } // else cart_type != 1
                            ?>


                            <!-- ADDRESS DETAILS -->
                            <?php 
                                $order_query = $dbh->query("SELECT * FROM `invoices_payments`
                                                                WHERE `id`='{$order_id}'
                                                                LIMIT 1
                                                            ");
                                if($order_query->rowCount()>0){
                                    $order  = $order_query->fetch();
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                
                                    <div class="clearfix"></div>
                                    <h3>Address Details</h3>
                                    <div style="clear:both"></div>

                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Area</label>
                                        <input type="text" class="form-control" name="area" value="<?php echo $order['area'];?>">
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Block</label>
                                        <input type="text" class="form-control" name="block" value="<?php echo $order['block'];?>">
                                    </div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Street Name/Number</label>
                                        <input type="text" class="form-control" name="street" value="<?php echo $order['street'];?>">
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Avenue</label>
                                        <input type="text" class="form-control" name="jadda" value="<?php echo $order['jadda'];?>">
                                    </div>   
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>House</label>
                                        <input type="text" class="form-control" name="house" value="<?php echo $order['house'];?>">
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>                                  
                            <?php } ?>
                            <!-- ADDRESS DETAILS -->

                            <!-- CUSTOMER DETAILS -->
                            <?php 
                                $member_query = $dbh->query("SELECT * FROM `members`
                                                                WHERE `id`='{$member_id}'
                                                                LIMIT 1
                                                            ");
                                if($member_query->rowCount()>0){
                                    $member  = $member_query->fetch();
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                
                                    <div class="clearfix"></div>
                                    <h3>Customer Details</h3>
                                    <div style="clear:both"></div>

                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" value="<?php echo $member['txt_name'];?>">
                                    </div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" value="<?php echo $member['last_name'];?>">
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Mobile</label>
                                        <input type="text" class="form-control" value="<?php echo $member['txt_phone'];?>">
                                    </div>
                                    <div class="form-group col-md-6" style="padding:0 2px;">
                                        <label>Email</label>
                                        <input type="text" class="form-control" value="<?php echo $member['txt_email'];?>">
                                    </div>                                    
                                    <div style="clear:both"></div>
                                </div>
                            </div>                                  
                            <?php } ?>
                            <!-- ADDRESS DETAILS -->

                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</section>
<script src="../js/jquery.js"></script>
<script src="../js/jquery-1.8.3.min.js"></script>
<script src="../bs3/js/bootstrap.min.js"></script>
<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/fullcalendar/fullcalendar.min.js"></script>
<script src="../js/bootstrap-switch.js"></script>
<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="../js/select2/select2.js"></script>
<script src="../js/select-init.js"></script>
<script src="../js/scripts.js"></script>
<script src="../js/toggle-init.js"></script>
<script src="../js/advanced-form.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/sparkline/jquery.sparkline.js"></script>
<script src="../js/flot-chart/jquery.flot.js"></script>
<script src="../js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../js/flot-chart/jquery.flot.resize.js"></script>
<script src="../js/flot-chart/jquery.flot.pie.resize.js"></script>
<?php 
        } // if cart_query->rowcount() >0
    } // if isset GET[cart_id]    
?>
</body>
</html>