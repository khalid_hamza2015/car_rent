<?php 
	ob_start();
	require_once("../module/include_mod.php");

	if (! in_array('orders', $permission_add)) {
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
		header('Location:../main/main.php');
	}

	$customer_id = (isset($_GET['customer_id']) ? $_GET['customer_id'] : 0);

	$customer_query = $dbh->query("SELECT * FROM `members` WHERE `id`='{$customer_id}' ");
	if($customer_query->rowCount() > 0){
		$customer 	= $customer_query->fetch();
	}else{
		$_SESSION['sql_status_msg'] = "Customer not found please fill out below fields";
		header('Location:customer_form.php');die();		
	}

	if($_SESSION['flag_type'] == 1){
		$where 	= " a.car_owner=1 AND a.provider_id=0";
	}else{
		$where 	= " a.car_owner=2 AND a.provider_id='{$login_provider}'";
	}

	// CARS
	$cars 	= array();
	$car_query 	= $dbh->query("SELECT *,a.status as car_status,
									b.cat_name_en as car_brand,
                                    c.cat_name_en as car_type,
                                    e.cat_name_en as car_cat
                                    FROM `cars` a
                                    INNER JOIN `car_brands` b ON(b.cat_id=a.car_brand)
                                    INNER JOIN `car_brands` c ON(c.cat_id=a.car_type)
                                    INNER JOIN `car_brands` e ON(e.cat_id=a.car_cat)
                                    INNER JOIN `car_traffic` d ON(d.car_id=a.car_id)
                                    WHERE a.status=1
                                    AND a.approved=1
                                    AND $where
                            ");
	if($car_query->rowCount() > 0){
		$cars 		= $car_query->fetchall();		
	}                        
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
	<style type="text/css">
		th{
			background: #ccc;
		}
		table,th,td{
			border: 1px solid #fff;
		}
		tbody tr, tbody td{
			border: 1px solid #ccc;	
		}
		.form-group label {
			text-align: left !important;
		}
		span.type_name{
			display: inline-block;
			padding-left: 5px;
		}
		span.price{
			display: inline-block;
		}
		label.radio-inline{
			padding-left: 0;
		}
		span.price{
			display: block;
		}
		span.old{
			color: red;
			text-decoration: line-through;
		}
	</style>
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li class="active">new order</li>		                  		                	
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                    	<div class="customer_info col-md-12">
		                    		<h3>Customer info</h3>
		                    		<table class="table table-striped">
		                                <thead>
		                                    <tr>		                                        
		                                        <th>Name</th>		                                        
		                                        <th>Mobile</th>
		                                        <th>Gender</th>		                                       
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                	<tr>
		                                		<td><?php echo $customer['txt_name'];?></td>
		                                		<td><?php echo $customer['txt_phone'];?></td>
		                                		<td><?php echo ($customer['gender'] == 1 ? 'Male': 'Female');?></td>
		                                	</tr>
		                                </tbody>
		                            </table>		                    		
		                    	</div>
		                    	<div class="clearfix"></div>

		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>			                                         
			                            <form action="orderController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">														                            

											<!-- CAR -->											
											<div class="form-group col-md-12">
												<label class="col-lg-2 control-label">Car</label>
												<div class="col-md-4">
													<select name="car" class="form-control" required>
														<option value="">Please select car</option>
														<?php foreach($cars as $car):?>
														<option value="<?php echo $car['car_id']?>"><?php echo $car['car_brand'].' - '.$car['car_type'].' - '.$car['car_cat'].' - '.$car['car_plate']; ?></option>
														<?php endforeach ?>
													</select>													
												</div>
											</div>
											<div id="featurs"></div>
											<div class="clearfix"></div>
											<div id="types"></div>

											<!-- DELIVERY SERVICE -->
		                                    <div class="form-group col-md-12">
												<label class="col-lg-2 control-label">Delivery Service</label>
												<div class="col-md-9">																
													<label class="checkbox-inline">
														<input name="delivery_service" type="checkbox" value="1">																	
													</label>																											
												</div>
											</div>

											<!-- PLACE -->
											<div id="delivery_service" class="col-md-12">												
		                                        <div class="form-group">
		                                            <label class="col-lg-2 control-label">Place</label>
		                                             <div class="col-lg-4" style="padding-left:0">
			                                            <div class="col-lg-6" style="padding-left:10px">
			                                                <input type="text" name="place_from" class="form-control" placeholder="From">
			                                            </div>
			                                            <div class="col-lg-6">
			                                                <input type="text" name="place_to" class="form-control" placeholder="To">
			                                            </div>
		                                             </div>
		                                        </div>
											</div>
											
											<!-- DELIVERY WITH DRIVER -->
		                                    <div class="form-group col-md-12">
												<label class="col-lg-2 control-label">Delivery With Driver</label>
												<div class="col-md-9">																
													<label class="checkbox-inline">
														<input name="delivery_with_driver" type="checkbox" value="1" >																	
													</label>																											
												</div>
											</div>

											<!-- DELIVERY TO AIR PORT -->
		                                    <div class="form-group col-md-12">
												<label class="col-lg-2 control-label">Delivery To airport </label>
												<div class="col-md-9">																
													<label class="checkbox-inline">
														<input name="delivery_to_airport" type="checkbox" value="1">																	
													</label>																											
												</div>
											</div>	

											<!-- TIME -->
											<div id="delivery_to_airport" class="col-md-12">												
		                                        <div class="form-group">
		                                            <label class="col-lg-2 control-label">Time</label>
		                                            <div class="col-lg-4" style="padding-left:0">
			                                            <div class="col-lg-6" style="padding-left:10px">
			                                                <input type="time" name="time_from" class="form-control" placeholder="From">
			                                            </div>
			                                            <div class="col-lg-6">
			                                                <input type="time" name="time_to" class="form-control" placeholder="To">
			                                            </div>
		                                            </div>
		                                        </div>
											</div>

											<div class="form-group col-md-12">
												<label class="col-lg-2">Status</label>
												<div class="col-lg-4">
													<select name="status" class="form-control">
		                                                <option value="0"> Pending </option>
		                                                <option value="1"> Confirmed </option>
		                                                <option value="2"> Delivered </option>
		                                                <option value="3"> Calcelled </option>
		                                            </select>
												</div>
											</div>										
											
			                               	<div class="form-group col-md-12">
			                                	<label class="col-lg-2 control-label"></label>			                                				                                			                                
			                                    <div class="col-lg-4">
													<input type="hidden" name="commission" value="0">
													<input type="hidden" name="commission_type" value="0">
													<input type="hidden" name="offer_id" value="0">
													<input type="hidden" name="customer_id" value="<?php echo $customer_id;?>">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="add_order" value="Submit">
			                                    	<a href="main.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				car:"required",
			}
		});

		/***** GET RENT COSTS & FEATURES *****/
		$('body').on('change','select[name=car]',function(){
			var car_id 	= $(this).val();
			var action 	= $('form').attr('action');
			$('#types').empty();
			$('#featurs').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'car_info':1,'car_id':car_id},
				success:function(response){
					var result 		= JSON.parse(response);
					var commission  	= result.commission;
					var commission_type = result.commission_type;
					var offer_id  		= result.offer_id;
					var features  		= result.features;
					var types  			= result.types;	
					// SET IPUTS VALUES
					$('input[name=commission]').val(commission);
					$('input[name=commission_type]').val(commission_type);
					$('input[name=offer_id]').val(offer_id);

					// SET TYPES
					var typs_txt 	= "<div class='col-md-12 form-group'><label class='col-lg-2 control-label'>Rent type</label><div class='col-md-9'>"+types+"</div></div>";
					$('#types').append(typs_txt);

					var features_txt = "<div class='col-md-12 form-group'><label class='col-md-2 control-label'>Features</label><div class='col-md-4'>"+features+"</div></div>";
					$('#featurs').append(features_txt);
				}
			});
			return false;
		});

		/***** SHOW & HIDE PLACE & TIME INPUTS ******/
		$("input[type=checkbox]").each(function(){                       
            var name = $(this).attr("name");                
            if ($(this).is(':checked')) {
                $('#'+name).removeClass('hide');             
            }else{
            	$('#'+name).addClass('hide');
            }
        });

        $('body').on('change','input[type=checkbox]',function(){
            var name = $(this).attr("name");                
            if ($(this).is(':checked')) {
                $('#'+name).removeClass('hide');             
            }else{
            	$('#'+name).addClass('hide');
            }
        });

	});
</script>
</body>
</html>