<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
<style>
.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td { padding:2px; }
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC; }
</style>
</head>
<style type="text/css">
    .search_form{
        padding: 20px 10px;
    }
</style>
<body>
<section id="container">
<?php include("../module/header_mod.php");?>
<?php include("../module/left_menu_mod.php");?>
<section id="main-content">
    <section class="wrapper">
		<section class="panel">
            <?php if(isset($_SESSION['permission_msg'])){?>
            <div class="alert alert-success">
                <span class="alert-icon"><i class="fa fa-exclamation"></i></span>
                <div class="notification-info">
                    <ul class="clearfix notification-meta">
                        <li class="pull-left notification-sender"><?php echo $_SESSION['permission_msg']?></li>
                    </ul><p><br></p>
                </div>
            </div>
            <?php 
                unset($_SESSION['permission_msg']);            
                }
            ?>  
            <?php if(isset($_SESSION['sql_status_msg'])){?>
            <div class="alert alert-success">
                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                <div class="notification-info">
                    <ul class="clearfix notification-meta">
                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                    </ul><p><br></p>
                </div>
            </div>
            <?php 
                 unset($_SESSION['sql_status_msg']);  
                }
            ?>            
            <div class="space15"></div>
            <?php if(in_array('orders', $permission_add)){?>
            <div class="search_form">
                <form action="orderController.php" method="post" class="required search_form">                
                    <div class="row form-group">
                        <label class="col-lg-3 control-label">Customer mobile Number</label>
                        <div class="col-lg-6">
                            <input type="text" name="mobile" value="" class="form-control" required >                                        
                        </div>           
                        <div class="col-md-3">                                                                            
                            <input type="submit" name="search" value="Search" class="btn btn-info  space_left">                                        
                        </div>
                    </div>
                </form>  
            </div>
            <?php } ?>
        </section>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('form').validate({
            rules:{
                mobile:{required:true,number:true,maxlenght:8,minlength:8}               
            }
        });
    });
</script>
</body>
</html>