<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
	<?php include("../module/header_mod.php");?><!--header end-->
    <?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active">Orders</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">            	
                    <h3>Order# <?php echo $_GET['order_id']?></h3>
                    <?php
                    $total = 0;
                    $sql_invoices_payments = $dbh->prepare("SELECT * FROM `invoices_payments` WHERE `id`=?");
                    $sql_invoices_payments->execute(array($_GET['order_id']));
                    if($sql_invoices_payments->rowCount() > 0) {
                        $rip = $sql_invoices_payments->fetch();
                        $txt_name = "";
                        $last_name = "";
                        $txt_phone = "";
                        $area = "";
                        $block = "";
                        $street = "";
                        $jadda = "";
                        $house = "";
                        $datasetMemberCart = $classObj->getTableDataByID("members",$rip['member_id']); 
                        if($datasetMemberCart) {
                            $res = mysql_fetch_array($datasetMemberCart); 
                            $txt_name = $res['txt_name'];
                            $last_name = $res['last_name'];
                            $txt_phone = $res['txt_phone'];
                            $area = $res['area'];
                            $block = $res['block'];
                            $street = $res['street'];
                            $jadda = $res['jadda'];
                            $house = $res['house'];
                        }?>
                        <table class="table table-hover general-table">
                            <tbody>
                                <tr>
                                    <th style="width:20%">Order Date</th>
                                    <td><?php echo $rip['date_create'];?></td>
                                </tr>                                        
                                <tr>
                                    <th style="width:20%">Name</th>
                                    <td><?php echo $txt_name?></td>
                                </tr>                                        
                                <tr>
                                    <th style="width:20%">Mobile</th>
                                    <td><?php echo $txt_phone?></td>
                                </tr>                                        
                                <tr>
                                    <th>Payment Method</th>
                                    <td><?=($rip['method_type']==2?"Credit Card":"Cash")?></td>                                        
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td><?php 
                                    switch($rip['order_status']) {//1:  , 2: 
                                        case 1: echo "Pending"; break;
                                        case 2: echo "Payment"; break;
                                        case 3: echo "Cancel"; break;
                                    }?></td>
                                </tr>
                                <?php /*?><tr>
                                    <th>Amount</th>
                                    <td><?php echo $rip['amt'];?></td>
                                </tr><?php */?>
                                <?php if($rip['method_type']==2) {?>
                                <tr>
                                    <th>Tap Reference Number</th>
                                    <td><?php echo $rip['ref']?></td>
                                </tr>
                                <tr>
                                    <th>Result</th>
                                    <td><?php echo $rip['result']?></td>
                                </tr>
                                <tr>
                                    <th>Tap Payment ID</th>
                                    <td><?php echo $rip['payid']?></td>
                                </tr>
                                <tr>
                                    <th>Payment Card Type</th>
                                    <td><?php echo $rip['crdtype']?></td>
                                </tr>
                                <tr>
                                    <th>Payment Card</th>
                                    <td><?php echo $rip['crd']?></td>
                                </tr>
                                <tr>
                                    <th>Delivery Charges</th>
                                    <td><?php 
                                    $Delivery_amt = 0;
                                    $datasetDelivery = $dbh->prepare(" 
                                    SELECT sa.`charge_delivery`
                                    FROM   `cart_items` ci 
                                    INNER JOIN `shop_areas` sa ON (ci.`shop_id` = sa.`shop_id`)  
                                    WHERE 
                                        ci.`area_id` = sa.`area_id` AND 
                                        ci.`shop_id` = sa.`shop_id` AND 
                                        ci.`cart_type` = 2 AND 
                                        ci.`order_id` = {$_GET['order_id']}
                                    GROUP BY ci.`shop_id`,ci.`area_id`;"); 
                                    $datasetDelivery->execute(); 
                                    if($datasetDelivery->rowCount()) {
                                        while($rec_delivery = $datasetDelivery->fetch()) {
                                            $Delivery_amt = $Delivery_amt + $rec_delivery[0];
                                        }
                                    }
                                    echo number_format($Delivery_amt,3)?></td>
                                </tr>
                                <?php /*?><tr><th>Copun</th><td><?php echo $rip['copun'];?></td></tr><?php */?>
                                <tr>
                                    <th>Discount</th>
                                    <td><?php echo $rip['discount_copun'];?></td>
                                </tr>                        
                                <tr>
                                    <th>Total</th>
                                    <td><?php echo number_format($rip['amt'],3)?></td>
                                </tr>
                                <?php }?>
                                <?php /*?><tr>
                                    <th>Notes</th>
                                    <td><?php echo $rip['order_notes']?></td>
                                </tr><?php */?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                            $total = 0;
                            $total_class = 0;
                            $total_rent = 0;
                            $sqlArray  = " SELECT ci.id , ci.qty , C.*
                                           FROM   `cart_items` ci 
                                           INNER JOIN `courses` C ON (ci.`item_id` = C.`id`)
                                           WHERE  ci.`cart_type` = 1 AND ci.`order_id` = {$_GET['order_id']};";
                            $dataset_cart = $dbh->prepare($sqlArray); 
                            $dataset_cart->execute(); 
                            if($dataset_cart->rowCount()) {?>
                            <div class="row-none">
                                <div class="page-order">
                                    <div class="order-detail-content">
                                        <table class="table table-bordered table-responsive cart_summary">
                                            <thead>
                                                <tr>
                                                    <th style="width:20%">Classes</th>
                                                    <th style="width:35%">Product</th>
                                                    <th style="width:15%">Shop</th>
                                                    <th style="text-align:center !important;width:10%">Price</th>
                                                    <th style="text-align:center !important;width:10%">Qty</th>
                                                    <th style="text-align:center !important;width:10%">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody><?php 
                                            while($cart_rec = $dataset_cart->fetch()) {
                                                $total_class = $total_class + ($cart_rec['qty']*$cart_rec['price']);?>
                                                <tr>
                                                    <td><?php if((trim($cart_rec['img'])) and (file_exists("../../course_img/".$cart_rec['img']))) {?><img src="<?php echo "../../resize/".$cart_rec['img']?>" style="height:100px"><?php }?></td>
                                                    <td>
                                                        <p><?php echo $cart_rec['name_en']?></p>
                                                        <p><i class="fa fa-calendar"></i> <?php echo "<strong> From </strong>".date("d F Y",strtotime($cart_rec['date_course']))?></p>
                                                        <p><i class="fa fa-calendar"></i> <?php echo "<strong> To </strong>".date("d F Y",strtotime($cart_rec['date_course_to']))?></p>
                                                        <p><i class="fa fa-clock-o"></i> <?php echo date("H:i",strtotime($cart_rec['start_time']))." - ".date("H:i",strtotime($cart_rec['end_time']))?></p>
                                                    </td>
                                                    <td><?php 
                                                    $dataset_cat = $classObj->checkByTableNameWithCondition("shop"," `id` = {$cart_rec['shop_id']};");
                                                    if($dataset_cat) {
                                                      $cat_rec = mysql_fetch_array($dataset_cat);
                                                      echo $cat_rec['name_en'];
                                                    }?></td>
                                                    <td style="text-align:center !important"><?php echo $cart_rec['price']?></td>
                                                    <td style="text-align:center !important"><?php echo $cart_rec['qty']?></td>
                                                    <td style="text-align:center !important"><?php echo ($cart_rec['qty']*$cart_rec['price'])?></td>
                                                </tr>
                                            <?php }?>  
                                            </tbody>  
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            <?php
                            $sqlArray  = " SELECT C.* , ci.*
                                           FROM   `cart_items` ci 
                                           INNER JOIN `product` C ON (ci.`item_id` = C.`id`)
                                           WHERE  ci.`cart_type` = 2 AND ci.`order_id` = {$_GET['order_id']};";
                            $dataset_cart = $dbh->prepare($sqlArray); 
                            $dataset_cart->execute(); 
                            if($dataset_cart->rowCount()) {?>
                            <div class="row-none">
                                <div class="page-order">
                                    <div class="order-detail-content">
                                        <table class="table table-bordered table-responsive cart_summary">
                                            <thead>
                                                <tr>
                                                    <th style="width:20%">Rent</th>
                                                    <th style="width:35%">Product</th>
                                                    <th style="width:15%">Shop</th>
                                                    <th style="text-align:center !important;width:10%">Price</th>
                                                    <th style="text-align:center !important;width:10%">Qty</th>
                                                    <th style="text-align:center !important;width:10%">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody><?php 
                                            while($cart_rec = $dataset_cart->fetch()) {
                                                $size_name = "";
                                                $datasetPrice = $dbh->prepare('SELECT * FROM `product_items` WHERE `id` = :ddl_size');
                                                $datasetPrice->bindParam(':ddl_size',$cart_rec['ddl_size']);
                                                $datasetPrice->execute();
                                                if($datasetPrice->rowCount() > 0){
                                                    $recPrice   = $datasetPrice->fetch();
                                                    $size_name = $recPrice['size'];
                                                }?>
                                                <tr>
                                                    <td><?php if((trim($cart_rec['img'])) and (file_exists("../../items_img/".$cart_rec['img']))) {?><img src="<?php echo "../../resize/".$cart_rec['img']?>" style="height:100px"><?php }?></td>
                                                    <td>
                                                        <p><?php echo "<strong>Name</strong>  ".$cart_rec['name_en']?></p>
                                                        <p><?php echo "<strong>Date/Time</strong>  ".date("d F Y",strtotime($cart_rec['book_date']))." / ".date("H:i",strtotime($cart_rec['from_time']))." - ".date("H:i",strtotime($cart_rec['to_time']))?></p>
                                                        <p><?php echo "<strong>".$cart_rec['label_eng']."</strong> ".$size_name?></p>
                                                        <p><?php 
                                                        $dsArea = $dbh->prepare("SELECT * FROM `areas` WHERE `id` = {$cart_rec['area_id']};");
                                                        $dsArea->execute();
                                                        if($dsArea->rowCount()) {
                                                            $rec_area = $dsArea->fetch();
                                                            echo "<strong>Location</strong>  ".$rec_area['name_en'];
                                                        }?></p>
                                                    </td>
                                                    <td><?php 
                                                    $dataset_cat = $classObj->checkByTableNameWithCondition("shop"," `id` = {$cart_rec['shop_id']};");
                                                    if($dataset_cat) {
                                                      $cat_rec = mysql_fetch_array($dataset_cat);
                                                      echo $cat_rec['name_en'];
                                                    }?></td>
                                                    <td style="text-align:center !important"><?php echo number_format($cart_rec['price'],3)?></td>
                                                    <td style="text-align:center !important"><?php echo $cart_rec['qty']?></td>
                                                    <td style="text-align:center !important"><?php echo number_format($cart_rec['price'],3)?></td>
                                                </tr>
                                            <?php }?>  
                                            </tbody>   
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php }?> 
                            
                            <div class="clearfix"></div>
                            <h3>Address Details</h3>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>First Name</label>
                                <input type="text" class="form-control" value="<?php if(trim($rip['txt_name'])<>"") echo $rip['txt_name']; else echo $txt_name?>">
                            </div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Last Name</label>
                                <input type="text" class="form-control" value="<?php if(trim($rip['last_name'])<>"") echo $rip['last_name']; else echo $last_name;?>">
                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Mobile</label>
                                <input type="text" class="form-control" value="<?php if(trim($rip['txt_phone'])<>"") echo $rip['txt_phone']; else echo $txt_phone;?>">
                            </div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Area</label>
                                <input type="text" class="form-control" name="area" value="<?php if(trim($rip['area'])<>"") echo $rip['area']; else echo $area;?>">
                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Block</label>
                                <input type="text" class="form-control" name="block" value="<?php if(trim($rip['block'])<>"") echo $rip['block']; else echo $block;?>">
                            </div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Street Name/Number</label>
                                <input type="text" class="form-control" name="street" value="<?php if(trim($rip['street'])<>"") echo $rip['street']; else echo $street;?>">
                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>Avenue</label>
                                <input type="text" class="form-control" name="jadda" value="<?php if(trim($rip['jadda'])<>"") echo $rip['jadda']; else echo $jadda;?>">
                            </div>   
                            <div class="form-group col-md-6" style="padding:0 2px;">
                                <label>House#</label>
                                <input type="text" class="form-control" name="house" value="<?php if(trim($rip['house'])<>"") echo $rip['house']; else echo $house;?>">
                            </div>
                            <div style="clear:both"></div>
                            </div>
                        </div>                                  
                        <?php }?>
                    </div>
                    </section>
                </div>
            </div>
    </section>
 </section>
</section>
<script src="../js/jquery.js"></script>
<script src="../js/jquery-1.8.3.min.js"></script>
<script src="../bs3/js/bootstrap.min.js"></script>
<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/fullcalendar/fullcalendar.min.js"></script>
<script src="../js/bootstrap-switch.js"></script>
<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="../js/select2/select2.js"></script>
<script src="../js/select-init.js"></script>
<script src="../js/scripts.js"></script>
<script src="../js/toggle-init.js"></script>
<script src="../js/advanced-form.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/sparkline/jquery.sparkline.js"></script>
<script src="../js/flot-chart/jquery.flot.js"></script>
<script src="../js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../js/flot-chart/jquery.flot.resize.js"></script>
<script src="../js/flot-chart/jquery.flot.pie.resize.js"></script>
</body>
</html>