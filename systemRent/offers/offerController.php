<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');	
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	// GET PARENT ID
	if(! isset($_SESSION['parent_id'])) $_SESSION['parent_id'] 	= 0;
	$parent_id 	= $_SESSION['parent_id'];

	if($parent_id > 0){
		$login_provider 	= $parent_id;
	}else{
		$login_provider 	= $login_id;
	}

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){							
		// GET POST DATA			
		$for   			= $_POST['for'];
		$quantity   	= $_POST['quantity'];
		$valid_from   	= $_POST['valid_from'];
		$valid_to   	= $_POST['valid_to'];
		$price 			= $_POST['price'];
		$price_type		= $_POST['price_type'];
		$id 			= $_POST['id'];	
		$car   			= (isset($_POST['car']) ? $_POST['car'] : array());
		$car 			= implode(',',$car);

		// CHECK USER TYPE
		if($_SESSION['flag_type'] == 1){
			$offer_by 		= 1; // ADMIN
			$provider_id 	= 0;
			$approved 		= 1;
		}else{
			$offer_by 		= 2; // PROVIDER
			$provider_id 	= $login_provider;
			$approved 		= 0;
		}						
			
		/***** ADD *****/
		if(isset($_POST['add'])){			
			$add 	= $dbh->query("INSERT INTO `offers` SET
										`offer_by`			='{$offer_by}',
										`provider_id`		='{$provider_id}',
										`car_id`			='{$car}',
										`offer_for` 		='{$for}',
										`offer_quantity` 	='{$quantity}',
										`offer_valid_from`	='{$valid_from}',
										`offer_valid_to`	='{$valid_to}',
										`offer_price`		='{$price}',
										`price_type`		='{$price_type}',
										`approved`			='{$approved}',										
										`status` 			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");			
			if($add){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='offer added successfuly';
			}

		} // add

		/***** UPDATE *****/
		elseif ($_POST['update']) {		
			$update = $dbh->query("UPDATE `offers` SET										
										`offer_by`			='{$offer_by}',
										`provider_id`		='{$provider_id}',
										`car_id`			='{$car}',
										`offer_for` 		='{$for}',
										`offer_quantity` 	='{$quantity}',
										`offer_valid_from`	='{$valid_from}',
										`offer_valid_to`	='{$valid_to}',
										`offer_price`		='{$price}',
										`price_type`		='{$price_type}',
										`approved`			='{$approved}',	
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`offer_id`			='{$id}'
									");
			if($update){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='offer updated successfuly';
			}

		}//update		

		header('Location:index.php');
		
	} // IF ISSET POST
?>
