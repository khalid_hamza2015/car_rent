<?php 
	ob_start();
	require_once("../module/include_mod.php");

	$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

	if($id == 0 && ! in_array('offers', $permission_add)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}
	elseif($id > 0 && ! in_array('offers', $permission_edit)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}

	// CARS
	$cars 		= array();
	$offer_cars = array();
	if($_SESSION['flag_type'] == 1){
		$where 	= " WHERE a.status < 3";
	}else{
		$where 	= " WHERE a.car_owner=2 AND a.provider_id='{$login_provider}' AND a.status < 3";
	}
	$car_query 	= $dbh->query("SELECT *,a.status as car_status,
									b.cat_name_en as car_brand,
                                    c.cat_name_en as car_type,
                                    e.cat_name_en as car_cat
                                    FROM `cars` a
                                    INNER JOIN `car_brands` b ON(b.cat_id=a.car_brand)
                                    INNER JOIN `car_brands` c ON(c.cat_id=a.car_type)
                                    INNER JOIN `car_brands` e ON(e.cat_id=a.car_cat)
                                    INNER JOIN `car_traffic` d ON(d.car_id=a.car_id)
                                    $where
                                    AND a.approved=1
                            ");
	if($car_query->rowCount() > 0){
		$cars 		= $car_query->fetchall();		
	}
	

	if($id > 0){
		$action = 'update';			
		$bc 	= 'updade offer';
		
		// OFFER 
		$offer_query 	= $dbh->query("SELECT * FROM `offers` WHERE `offer_id`='{$id}'");
		if ($offer_query->rowCount()>0) {
			$offer 		= $offer_query->fetch();
			$offer_cars	= explode(',', $offer['car_id']);
		}

	}else{
		$action = 'add';
		$bc 	= 'add offer';


	}

?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">offers</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="offerController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">														                            

		                            		<!-- FOR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">For</label>
												<div class="col-md-4">																
													<select name="for" class="form-control" required>
														<option value="">Please select</option>														
														<option value="1" <?php if(@$offer['offer_for'] == 1) echo 'selected'; ?> > All</option>
														<option value="2" <?php if(@$offer['offer_for'] == 2) echo 'selected'; ?> > My customers</option>
														<option value="3" <?php if(@$offer['offer_for'] == 3) echo 'selected'; ?> > traviling offices</option>
														<option value="4" <?php if(@$offer['offer_for'] == 4) echo 'selected'; ?> > All customers</option>														
													</select>
												</div>
											</div>

											<!-- QUANTITY -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Quantity</label>
												<div class="col-md-4">
													<input type="text" name="quantity" class="form-control required" value="<?php echo @$offer['offer_quantity'];?>" required>
												</div>
											</div>

											<!-- DATE STRAT -->
											<div class="form-group">
				                            	<label class="col-lg-2 control-label">Valid from</label>
				                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date=""  class="col-md-4 input-append date dpYears">
				                                    <input type="text" name="valid_from" readonly="" value="<?php echo @$offer['offer_valid_from'];?>" size="16" class="form-control">
				                                    <span class="input-group-btn add-on">
				                                        <button class="btn btn-primary" type="button" style="margin-left:-30px;height:34px"><i class="fa fa-calendar"></i></button>
				                                    </span>
				                                </div>
				                            </div>

				                            <!-- DATE END -->
											<div class="form-group">
				                            	<label class="col-lg-2 control-label">Valid to</label>
				                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date=""  class="col-md-4 input-append date dpYears">
				                                    <input type="text" name="valid_to" readonly="" value="<?php echo @$offer['offer_valid_to'];?>" size="16" class="form-control">
				                                    <span class="input-group-btn add-on">
				                                        <button class="btn btn-primary" type="button" style="margin-left:-30px;height:34px"><i class="fa fa-calendar"></i></button>
				                                    </span>
				                                </div>
				                            </div>

				                            <!-- PRICE -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Price</label>
												<div class="col-md-3">
													<input type="text" name="price" class="form-control required" value="<?php echo @$offer['offer_price'];?>" required>
												</div>
												<div class="col-md-1">
                                                    <select name="price_type" class="form-control">
                                                        <option value="1" <?php if(@$offer['price_type'] == 1) echo 'selected'; ?> >KD</option>
                                                        <option value="2" <?php if(@$offer['price_type'] == 2) echo 'selected'; ?> >Percent</option>
                                                    </select>
                                                </div>
											</div>                                          

				                            <!-- FEATURES -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Cars</label>
												<div class="col-md-9">
													<?php foreach ($cars as $car):?>
													<label class="checkbox-inline col-md-12" style="margin-left:0">
														<input name="car[]" type="checkbox" value="<?php echo $car['car_id']?>" <?php if(in_array($car['car_id'], $offer_cars)) echo 'checked';?>>
														<?php echo $car['car_brand'].' - '.$car['car_type'].' - '.$car['car_cat'].' - '.$car['car_plate'];?>
													</label>
													<?php endforeach; ?>													
												</div>
											</div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">			                                	
			                                    <div class="col-lg-4">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../js/advanced-form.js"></script> 
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				title_en:"required",
				url:{required:true,url:true},
				desc_en:"required"
			}
		});
	});
</script>
</body>
</html>