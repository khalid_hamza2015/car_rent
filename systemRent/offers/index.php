<?php
    ob_start(); 
    require_once("../module/include_mod.php");
    

?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active">offers</li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <?php if(in_array('offers', $permission_add)){?>
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="offer_form.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                    <?php } ?>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <?php if(in_array('offers', $permission_view)){?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <?php if($_SESSION['flag_type'] == 1): ?>
                                        <th>Provider</th>
                                        <th>Approved</th>
                                        <?php else:?>
                                        <th>Admin Status</th>
                                        <?php endif ?>
                                        <th>For</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Quantity</th>                                         
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if($_SESSION['flag_type'] == 1){
                                        $where  = " INNER JOIN `providers` b ON(b.provider_id=a.provider_id) WHERE a.status < 3";
                                    }else{
                                        $where = " WHERE a.status < 3 AND `provider_id`='{$login_provider}'";
                                    }
                                    if(isset($_GET['pendding'])){
                                        $where .= " AND a.approved=0";
                                    }
                                    
        							$stmt = $dbh->query("SELECT *,a.status as of_status FROM `offers` a $where ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id     = $rec['offer_id'];
                                            $of_for = $rec['offer_for'];

                                            // CHECK APPROVED STATUS
                                            if($rec['approved'] == 0){                                                
                                                $st_class   = 'btn-danger';
                                                $icon       = '<i class="fa fa-times-circle"> Pendding</i>';
                                            }else{                                                
                                                $st_class   = 'btn-success';
                                                $icon       = '<i class="fa fa-check-circle"> Approved</i>';
                                            }     

                                            // OFFER FOR 
                                            switch ($of_for) {
                                                case 1:
                                                    $for    = 'All';
                                                    break;
                                                case 2:
                                                    $for    = 'provider customers';
                                                    break; 
                                                case 3:
                                                    $for    = 'traviling offices';
                                                    break; 
                                                case 4:
                                                    $for    = 'All customers';
                                                    break;
                                            }                     
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <?php if($_SESSION['flag_type'] == 1): ?>
                                    <td><?php echo $rec['provider_name_en']; ?></td>
                                    <td>
                                        <?php 
                                            if($rec['approved'] == 0){
                                                echo '<i class="fa fa-times-circle fa_ red" id="approved" value="1" value-id="'.$id.'" table="offers" table-id="offer_id"></i>';
                                            }else{
                                                echo '<i class="fa fa-check-circle fa_ blue" id="approved" value="0" value-id="'.$id.'" table="offers" table-id="offer_id"></i>';
                                            }
                                        ?>
                                    </td>
                                    <?php else:?>
                                    <td><?php echo '<span class="btn '.$st_class.'">'.$icon.'</span>'; ?></td>
                                    <?php endif ?>
                                    <td><?php echo $for?></td>
                                    <td><?php echo $rec['offer_valid_from']?></td>
                                    <td><?php echo $rec['offer_valid_to']?></td> 
                                    <td><?php echo $rec['offer_quantity']?></td> 
                                    <td>
                                    <?php if(in_array('offers', $permission_edit)){?>                                                                      
                                        <a href="<?php echo 'offer_form.php?id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['of_status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="banners" table-id="banner_id" >Disable</a>
                                    <?php } elseif($rec['of_status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="banners" table-id="banner_id" >Enable</a>
                                    <?php }} if(in_array('offers', $permission_delete)){?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="banners" table-id="banner_id">Delete</a> 
                                    <?php } ?>
                                    </td>                                        
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                            <?php } // permission_view  ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>

</html>