<aside>
    <div id="sidebar" class="nav-collapse" style="position:absolute !important">
        <div class="leftside-navigation">
        	<ul class="sidebar-menu" id="nav-accordion">
            	<li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "main") !== FALSE) echo 'active dcjq-parent';?>" href="../main/main.php"><span>Dashboard</span></a></li>
				<?php if($_SESSION["flag_type"]==1) {?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "orders") !== FALSE) echo 'active dcjq-parent';?>" href="../orders/"><span>orders</span></a></li>
                <li><a class="<?php if((strpos($_SERVER["REQUEST_URI"], "providers") !== FALSE))  echo 'active dcjq-parent';?>" href="../providers/"><span>Vendors Management</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "members") !== FALSE) echo 'active dcjq-parent';?>" href="../members/index.php"><span>Customers</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "country_city") !== FALSE) echo 'active dcjq-parent';?>" href="../country_city/"><span>Country / City</span></a></li>
                <li>
                    <a class="<?php if((strpos($_SERVER["REQUEST_URI"], "features/index.php?type=1") !== FALSE) ||
                                       (strpos($_SERVER["REQUEST_URI"], "features/index.php?type=2") !== FALSE) ||
                                       (strpos($_SERVER["REQUEST_URI"], "features/index.php?type=3") !== FALSE)
                                      ) echo 'active dcjq-parent';?>" 
                        href=""><span>features</span></a>
                    <ul class="sub">                                                                        
                        <li><a href="../features/index.php?type=1"><span>providers features</span></a></li>
                        <li><a href="../features/index.php?type=2"><span>cars features</span></a></li>
                        <li><a href="../features/index.php?type=3"><span>insurance features</span></a></li>                       
                    </ul>
                </li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "car_brands") !== FALSE) echo 'active dcjq-parent';?>" href="../car_brands/"><span>Cars brands</span></a></li>
                <li>
                    <a class="<?php if(strpos($_SERVER["REQUEST_URI"], "companies") !== FALSE) echo 'active dcjq-parent';?>" href=""><span>Companies</span></a>
                    <ul class="sub">                                                                        
                        <li><a href="../companies/index.php?type=1"><span>insurance companies</span></a></li>                                              
                    </ul>
                </li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "index.php?type=4") !== FALSE) echo 'active dcjq-parent';?>" href="../features/index.php?type=4"><span>colors</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "index.php?type=5") !== FALSE) echo 'active dcjq-parent';?>" href="../features/index.php?type=5"><span>rent periods</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "cars") !== FALSE) echo 'active dcjq-parent';?>" href="../cars/"><span>Cars</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "banners") !== FALSE) echo 'active dcjq-parent';?>" href="../banners/"><span>Banners</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "news") !== FALSE) echo 'active dcjq-parent';?>" href="../news/"><span>News</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "offers") !== FALSE) echo 'active dcjq-parent';?>" href="../offers/"><span>offers</span></a></li>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "reports") !== FALSE) echo 'active dcjq-parent';?>" href="../reports/"><span>Reports</span></a></li>                
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "users") !== FALSE) echo 'active dcjq-parent';?>" href="../users/"><span>Users</span></a></li>
            	<?php 
                    } else{
                        if($_SESSION['permission'] == 1){
                ?>
                <?php if(in_array('cars', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "cars") !== FALSE) echo 'active dcjq-parent';?>" href="../cars/"><span>cars</span></a></li>
                
                <?php }if(in_array('employees', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "employees") !== FALSE) echo 'active dcjq-parent';?>" href="../employees/"><span>employees</span></a></li>
                
                <?php }if(in_array('offers', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "offers") !== FALSE) echo 'active dcjq-parent';?>" href="../offers/"><span>offers</span></a></li>                        
                
                <?php }if(in_array('orders', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "orders") !== FALSE) echo 'active dcjq-parent';?>" href="../orders/"><span>orders</span></a></li>
                
                <?php }if(in_array('news', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "news") !== FALSE) echo 'active dcjq-parent';?>" href="../news/"><span>news</span></a></li>                    
                
                <?php }if(in_array('customers', $permission_view)){?>
                <li><a class="<?php if(strpos($_SERVER["REQUEST_URI"], "customers") !== FALSE) echo 'active dcjq-parent';?>" href="../cars/"><span>customers</span></a></li>
                <?php }?>
                <?php                
                        } // $_SESSION['permission'] == 1
                ?>        
						
                <?php }?>
            </ul>
        </div>
    </div>
</aside>