<?php
define('INCLUDE_CHECK',true);
require_once('../../function.php');
if(! isset($_SESSION['rent_id'])) header( 'Location: ../index.php' ) ; 

// GET PARENT ID
if(! isset($_SESSION['parent_id'])) $_SESSION['parent_id'] 	= 0;
$parent_id 	= $_SESSION['parent_id'];

// GET USER PERMISSION  
if(! isset($_SESSION['permission_view'])) 	$_SESSION['permission_view'] 	= '';
if(! isset($_SESSION['permission_add']))  	$_SESSION['permission_add'] 	= '';
if(! isset($_SESSION['permission_edit'])) 	$_SESSION['permission_edit'] 	= '';
if(! isset($_SESSION['permission_delete'])) $_SESSION['permission_delete'] 	= '';
$permission_view 	= explode(',', $_SESSION['permission_view']);
$permission_add		= explode(',', $_SESSION['permission_add']);
$permission_edit	= explode(',', $_SESSION['permission_edit']);
$permission_delete	= explode(',', $_SESSION['permission_delete']);



$a = new auth(); 
$login_id = $a->getLoggedUserID();

if($parent_id > 0){
	$login_provider 	= $parent_id;
}else{
	$login_provider 	= $login_id;
}

$classObj = new Query();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Rent Car</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mohammed AlDini - Software Engineer - mo.aldini@gmail.com">
    <link rel="shortcut icon" type="image/x-icon" href="../../logo.png">
    <link rel="icon" type="image/x-icon" href="../../logo.png"> 
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <link rel="apple-touch-icon" href="../../logo.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <meta content="yes" name="apple-mobile-web-app-capable" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">    
	<meta name="HandheldFriendly" content="true">
	<meta name="MobileOptimized" content="width">
    <link href="../bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="../css/bootstrap-reset.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../js/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="../css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="../js/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="../js/morris-chart/morris.css">

    <link rel="stylesheet" type="text/css" href="../js/bootstrap-datepicker/css/datepicker.css" /> 
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet"/> 
    <style type="text/css">
    	.error,.red{
    		color: red;
    	}
    	.blue{
    		color: blue;
    	}
    	.fa_{
    		font-size: 20px !important;
    	}
    	aside li a,
    	ul.breadcrumb li,
    	ul.breadcrumb li a{
    		text-transform: capitalize;
    	}
    	.tab-content{
    		padding: 20px;
    	}
    	div.view-img {
	    	width: 100px;
	    	height: 100px;
	    	overflow: hidden;
	    }
	    div.view-img img{
	    	width: 100%;
	    	height: 100%;
	    }
	    .form-horizontal .control-label, 
	    .form-horizontal .radio, 
	    .form-horizontal .checkbox,
	    .form-horizontal .radio-inline,
	    .form-horizontal .checkbox-inline{
	    	padding-top: 0;
	    }
    </style> 
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="../js/ckeditor/ckeditor.js" language="javascript"></script>
    <script language="javascript" type="text/javascript"> 
	function getXMLHTTP() {
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e) {		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		return xmlhttp;
	}
	function subDDL(divName,Para,pageName) {
		var req = getXMLHTTP();
		if (req) {
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					if (req.status == 200) {						
						document.getElementById(divName).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET",pageName+"?para="+Para, true);
			req.send(null);
		}
	}
	function confirmDel(links) 
	{
		var answer = confirm("Are you sure?")
		if (answer)
		{
			window.location = links;
		}
	}

	// CHANGE STATUS
	function changeStatus(link) {
	    var answer = confirm("Are you sure?")
	    if (answer)
	    {                
	        window.location='../controllers/changeStatus.php'+link
	    }
	}
	</script>