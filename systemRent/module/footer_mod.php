<script src="../js/jquery.js"></script>
<script src="../bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/jquery.validate.min.js"></script>
<?php /*?><script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/sparkline/jquery.sparkline.js"></script>
<script src="../js/flot-chart/jquery.flot.js"></script>
<script src="../js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../js/flot-chart/jquery.flot.resize.js"></script>
<script src="../js/flot-chart/jquery.flot.pie.resize.js"></script><?php */?>
<script src="../js/scripts.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // ASIDE HEIGHT
        $('#sidebar').height($(document).height());    

        // FEATURED
        $('body').on('click','#featured',function(){                
            var answer      = confirm('Are You Sure ?');
            
            if (answer) {
                var table       = $(this).attr('table');
                var table_id    = $(this).attr('table-id'); 
                var value_id    = $(this).attr('value-id');
                var value       = $(this).attr('value');
                $.ajax({
                    url:"../controllers/featured.php",
                    type:"post",
                    data:{"table":table,"table_id":table_id,"value_id":value_id,"value":value},
                    success:function(response){                            
                        location.reload();
                    }
                });
                return false;
                
            };
        });

        // APPROVED
        $('body').on('click','#approved',function(){                
            var answer      = confirm('Are You Sure ?');
            
            if (answer) {
                var table       = $(this).attr('table');
                var table_id    = $(this).attr('table-id'); 
                var value_id    = $(this).attr('value-id');
                var value       = $(this).attr('value');
                $.ajax({
                    url:"../controllers/approved.php",
                    type:"post",
                    data:{"table":table,"table_id":table_id,"value_id":value_id,"value":value},
                    success:function(response){                            
                        location.reload();
                    }
                });
                return false;
                
            };
        });

         // FEATURED
        $('body').on('click','.ch_status',function(){                
            var answer      = confirm('Are You Sure ?');
            
            if (answer) {
                var table       = $(this).attr('table');
                var table_id    = $(this).attr('table-id'); 
                var value_id    = $(this).attr('value-id');
                var value       = $(this).attr('value');
                $.ajax({
                    url:"../controllers/status.php",
                    type:"post",
                    data:{"table":table,"table_id":table_id,"value_id":value_id,"value":value},
                    success:function(response){                            
                        location.reload();
                    }
                });
                return false;
                
            };
        });
    });
</script>