<header class="header fixed-top clearfix">
<div class="brand">
    <a href="../main/main.php" class="logo"><img src="../../logo.png" /></a>
    <div class="sidebar-toggle-box"><div class="fa fa-bars"></div></div>
</div>
<div class="nav notify-row" id="top_menu">    
    <!--  notification start -->
    <ul class="nav top-menu">
    <?php 
        if($_SESSION['flag_type'] == 1){
            // CARS
            $pending_cars   = $dbh->query("SELECT count(*) FROM `cars`
                                            WHERE `approved`=0
                                            AND `status`    < 3
                                        ")->fetchColumn();

            // OFFERS
            $pending_offers  = $dbh->query("SELECT count(*) FROM `offers`
                                            WHERE `approved`=0
                                            AND `status`    < 3
                                        ")->fetchColumn();
    ?>  
    <!-- CARS  -->
    <li id="header_notification_bar" class="dropdown">
        <a class="dropdown-toggle" href="../cars/index.php?pendding" title="cars">
            <i class="fa fa-car" aria-hidden="true"></i>
            <span class="badge bg-warning"><?php if($pending_cars > 0)echo $pending_cars; else echo '';?></span>
        </a>
    </li>
    <!-- OFFERS  -->
    <li id="header_notification_bar" class="dropdown">
        <a class="dropdown-toggle" href="../offers/index.php?pendding" title="offers">
            <i class="fa fa-globe"></i>
            <span class="badge bg-warning"><?php if($pending_offers > 0)echo $pending_offers; else echo '';?></span>
        </a>
    </li>
    <?php } ?>
    </ul> 
    <!--  notification end -->
</div>
<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="../images/avatar.png">
                <span class="username"><?php echo $a->getLoggedUserName();?></span> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
				<?php if($_SESSION['flag_type'] == 2 && $_SESSION['parent_id'] == 0){ ?>
                    <li><a href="../providers/provider_form.php?id=<?php echo $login_id;?>"> Edit Profile</a></li>
                    <li><a href="../providers/change_password.php?id=<?php echo $login_id;?>"> Cahnge Password</a></li>
                <?php } ?> 
                <li><a href="../index.php?act=logout"> Log Out</a></li>
            </ul>
        </li>
        <?php /*?><li><div class="toggle-right-box"><div class="fa fa-bars"></div></div></li><?php */?>
    </ul>
</div>
</header>