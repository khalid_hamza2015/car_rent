<?php
define('INCLUDE_CHECK',true);
require_once('../function.php');
$errorMessage = "none";
$a = new auth();   
if(isset($_SESSION['rent_id'])) {
   header( 'Location: main/main.php' ) ; 
} elseif((isset($_REQUEST["username"])) and (isset($_REQUEST["password"]))) {     
    if($a->login($_REQUEST["username"],$_REQUEST["password"]) ) {
       header( 'Location: main/main.php' ) ; 
    } else { 
      $errorMessage = "block";
    }    
}
if((isset($_REQUEST["act"])) and ($_REQUEST["act"]=="logout"))
{       
    $a->logOut();
}?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.png">
    <link rel="icon" type="image/x-icon" href="../favicon.png"> 
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <link rel="apple-touch-icon" href="../favicon.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <title>Rent Car</title>
    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-body">
    <div class="container">
        <div class="alert alert-danger" style="display:<?php echo $errorMessage?>">
            <span class="alert-icon"><i class="fa fa-warning"></i></span>
            <div class="notification-info">
                <ul class="clearfix notification-meta">
                    <li class="pull-left notification-sender"><span><a href="#">Error</a></span></li>
                    <li class="pull-right notification-time"></li>
                </ul>
                <p>Wrong username or password.</p>
            </div>
        </div>        
	<?php if((isset($_REQUEST['error_forget'])) and ($_REQUEST['error_forget']==2)) {?>
    <div class="alert alert-success ">
        <span class="alert-icon"><i class="fa fa-check"></i></span>
        <div class="notification-info">
            <ul class="clearfix notification-meta">
                <li class="pull-left notification-sender"><span><a href="#">Successfully</a></span></li>
                <li class="pull-right notification-time"></li>
            </ul>
            <p>Your Password has been successfully sent in your email.</p>
        </div>
    </div>
    <?php }?>
    <?php if((isset($_REQUEST['error_forget'])) and ($_REQUEST['error_forget']==1)) {?>
    <div class="alert alert-danger ">
        <span class="alert-icon"><i class="fa fa-times"></i></span>
        <div class="notification-info">
            <ul class="clearfix notification-meta">
                <li class="pull-left notification-sender"><span><a href="#">Error</a></span></li>
                <li class="pull-right notification-time"></li>
            </ul>
            <p>The Email is incorrect.</p>
        </div>
    </div>
    <?php }?>
    <form class="form-signin" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
        <h2 class="form-signin-heading"><img src="../logo.png" /></h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" name="username" id="username" class="form-control" placeholder="User ID" autofocus>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right"><a data-toggle="modal" href="#myModal"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>            
        </div>
      </form>
    </div>
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
      <div class="modal-dialog">
        <form method="post" action="forget_code.php">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Forgot Password ?</h4>
              </div>
              <div class="modal-body">
                  <p>Enter your e-mail address below to send your password.</p>
                  <input type="email" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" required>

              </div>
              <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                  <button class="btn btn-success" type="submit">Submit</button>
              </div>
          </div>
        </form>
      </div>
  </div>
<script src="js/jquery.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
</body>
</html>