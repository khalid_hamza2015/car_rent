<?php 
	require_once("../module/include_mod.php");

	if(! in_array('cars', $permission_edit) && ! in_array('cars', $permission_add)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}

	// COUNTRY ID+
	$country_id 	= (isset($_SESSION['country']) ? $_SESSION['country'] : 1);


	if(! isset($_GET['car_id'])) {header('Location: index.php');}

	$car_id 	= $_GET['car_id'];

	// CAR
	$car_query = $dbh->query("SELECT * FROM `cars` WHERE `car_id`='{$car_id}' ");

	if ($car_query->rowCount()>0) {
			$car 	= $car_query->fetch();

			// GET INSURENCE FEATURES
			$features 		= array();
			$features_query = $dbh->query("SELECT * FROM `features` WHERE `feature_for`=3 AND `status`=1");
			if($features_query->rowCount() > 0){
				$features 	= $features_query->fetchall();
			}


			// GET INSURENCE COMPANIES
			$companies 		= array();
			$where 	= '';
			if($_SESSION['flag_type'] != 1){$where = "AND `country_id`	='{$country_id}' "; }
			$company_query 	= $dbh->query("SELECT * FROM `companies` 
											WHERE `status`		=1
											AND `company_type`	=1																				
											$where
										");			
			if($company_query->rowCount() > 0){
				$companies 	= $company_query->fetchall();				
			}


			// ADD OR UPDATE
			$ins_features		= array();
			$action 			= 'add';
			$insurance_query 	= $dbh->query("SELECT * FROM `car_insurance`
											WHERE `car_id`='{$car_id}' 											
										");
			if($insurance_query->rowCount()>0){				
				$action 		= 'update';
				$insurance 		= $insurance_query->fetch();
				$ins_features 	= explode(',', $insurance['insurance_features']);

				$company_id 	= $insurance['company_id'];
				$com_types_query 	= $dbh->query("SELECT * FROM `insurance_types` 
													WHERE `company_id`='{$company_id}'											
													AND `status`	=1
												");
				if($com_types_query->rowCount () > 0){
					$com_types 	= $com_types_query->fetchall();
				}
			}	
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	  
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">cars</a></li>		                    
		                	<li class="active">car insurace</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div> 

		                            	<?php if(isset($_SESSION['sql_status_msg'])){?>
		                            	<div class="alert alert-success">
			                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
			                                <div class="notification-info">
			                                    <ul class="clearfix notification-meta">
			                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
			                                    </ul><p><br></p>
			                                </div>
			                            </div>
			                            <?php 
			                                unset($_SESSION['sql_status_msg']);
			                                }
			                            ?>

			                            <form action="carController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
											
											<!-- COMPANY -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Company</label>
												<div class="col-md-4">
													<select name="company_id" class="form-control" required>
														<option value="">Please select comapny</option>
														<?php foreach ($companies as $rec):?>
														<option value="<?php echo $rec['company_id']?>" <?php if(@$insurance['company_id'] == $rec['company_id']) echo 'selected'; ?> > <?php echo $rec['company_name_en'];?></option>
														<?php endforeach;?>																
													</select>
												</div>
											</div>

											<!-- INSURENCE TYPE -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Insurance type</label>
												<div class="col-md-4">
													<select name="insurance_type" class="form-control" required>
														<option value="">Please select type</option>
														<?php 
															if(isset($com_types)):
																foreach($com_types as $rec):
														?>
														<option value="<?php echo $rec['type_id']?>" <?php if(@$insurance['insurance_type'] == $rec['type_id']) echo 'selected'; ?> > <?php echo $rec['type_name_en'];?></option>
														<?php			
																endforeach;
															endif;
														?>																										
													</select>
												</div>
											</div>

											<!-- DATE STRAT -->
											<div class="form-group">
				                            	<label class="col-lg-2 control-label">Insurance Start</label>
				                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date=""  class="col-md-4 input-append date dpYears">
				                                    <input type="text" name="insurance_start" readonly="" value="<?php echo @$insurance['insurance_strat_date'];?>" size="16" class="form-control">
				                                    <span class="input-group-btn add-on">
				                                        <button class="btn btn-primary" type="button" style="margin-left:-30px;height:34px"><i class="fa fa-calendar"></i></button>
				                                    </span>
				                                </div>
				                            </div>

				                            <!-- DATE END -->
											<div class="form-group">
				                            	<label class="col-lg-2 control-label">Insurance End</label>
				                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date=""  class="col-md-4 input-append date dpYears">
				                                    <input type="text" name="insurance_end" readonly="" value="<?php echo @$insurance['insurance_end_date'];?>" size="16" class="form-control">
				                                    <span class="input-group-btn add-on">
				                                        <button class="btn btn-primary" type="button" style="margin-left:-30px;height:34px"><i class="fa fa-calendar"></i></button>
				                                    </span>
				                                </div>
				                            </div>

											<!-- FEATURES -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Features</label>
												<div class="col-md-9">
													<?php foreach ($features as $feature):?>
													<label class="checkbox-inline">
														<input name="feature[]" type="checkbox" value="<?php echo $feature['feature_id']?>" <?php if(in_array($feature['feature_id'], $ins_features)) echo 'checked';?>>
														<?php echo $feature['feature_name_en'];?>
													</label>
													<?php endforeach; ?>													
												</div>
											</div>											

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="car_id" value="<?php echo $car_id; ?>">
			                                    <div class="col-lg-4">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="car_insurance" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>			                          
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php //include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<?php } ?>
<script src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../js/advanced-form.js"></script>    
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				username:"required",
				email:{required:true,email:true},
				password:"required",
				res_name:"required",
				res_mobile:"required",
				name_en:"required",
				name_ar:"required",				
				tele:{required:true,number:true}				
			}
		});

	    /***** GET COPMPANY TRYPES ******/
		$('body').on('change','select[name=company_id]',function(){
			var comapny = $(this).val();
			var action 	= $('form').attr('action');
			$('select[name=insurance_type]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_company_types':1,'comapny':comapny},
				success:function(response){
					$('select[name=insurance_type]').append(response);					
					//alert(response);
				}
			});
			return false;
		});

	});
</script>
</body>
</html>