<?php 
    require_once("../module/include_mod.php");
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active">cars</li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <?php if(in_array('cars', $permission_add)){?>
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="car_form.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                    <?php } ?>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <?php if(in_array('cars', $permission_view)){?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <?php if($_SESSION['flag_type'] == 1): ?>
                                        <th>Provider</th>
                                        <th>Approved</th>
                                        <?php endif ?>
                                        <th>Brand</th>
                                        <?php if($_SESSION['flag_type'] == 1): ?>
                                        <th>Instructions</th>
                                        <?php else: ?>
                                        <th>Type</th>
                                        <th>Category</th>
                                        <th>Admin Status</th>
                                        <?php endif ?>
                                        <th>Insurance </th> 
                                        <th>Rent Cost</th>                                                                                
                                        <th>Action</th>
                                        <?php if($_SESSION['flag_type'] == 1)echo '<th>Commission</th>';?>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $where  = " WHERE a.provider_id ='$login_provider}' AND a.status < 3";  
                                    if($_SESSION['flag_type'] == 1){
                                        $where  = " WHERE a.status < 3 ";
                                    }
                                    if(isset($_GET['pendding'])){
                                        $where .= " AND a.approved=0";
                                    }
        							$stmt = $dbh->query("SELECT *,a.status as car_status,b.cat_name_en as car_brand,
                                                            c.cat_name_en as car_type,e.cat_name_en as car_cat
                                                            FROM `cars` a
                                                            INNER JOIN `car_brands` b ON(b.cat_id=a.car_brand)
                                                            INNER JOIN `car_brands` c ON(c.cat_id=a.car_type)
                                                            INNER JOIN `car_brands` e ON(e.cat_id=a.car_cat)                                                            
                                                            $where                                                          
                                                        ");                                    
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['car_id'];

                                            if($rec['approved'] == 0){                                                
                                                $st_class   = 'btn-danger';
                                                $icon       = '<i class="fa fa-times-circle"> Pendding</i>';
                                            }else{                                                
                                                $st_class   = 'btn-success';
                                                $icon       = '<i class="fa fa-check-circle"> Approved</i>';
                                            }


                                            if($_SESSION['flag_type'] == 1){
                                                $owner_id   = $rec['car_owner'];
                                                
                                                if($owner_id == 2){
                                                    $provider   = '';
                                                    $provider_query = $dbh->query("SELECT * FROM `providers` 
                                                                                    WHERE `provider_id`='{$owner_id}'
                                                                                    LIMIT 1
                                                                                ");
                                                    if($provider_query->rowCount() > 0){
                                                        $provider_  = $provider_query->fetch();
                                                        $provider   = $provider_['provider_name_en'];
                                                    }
                                                }else{
                                                    $provider   = 'Admin';
                                                }
                                            }                                                                                                                        
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <?php if($_SESSION['flag_type'] == 1): ?>
                                    <td><?php echo $provider; ?></td>
                                    <td>
                                        <?php 
                                            if($rec['approved'] == 0){
                                                echo '<i class="fa fa-times-circle fa_ red" id="approved" value="1" value-id="'.$id.'" table="cars" table-id="car_id"></i>';
                                            }else{
                                                echo '<i class="fa fa-check-circle fa_ blue" id="approved" value="0" value-id="'.$id.'" table="cars" table-id="car_id"></i>';
                                            }
                                        ?>
                                    </td>
                                    <?php endif ?>
                                    <td><?php echo $rec['car_brand']?></td>
                                    <?php if($_SESSION['flag_type'] == 1): ?>
                                    <td><a href="instructions.php?car_id=<?php echo $id;?>" class="btn btn-info">Add / View</a></td>
                                    <?php else: ?>
                                    <td><?php echo $rec['car_type']?></td>
                                    <td><?php echo $rec['car_cat']?></td> 
                                    <td><?php echo '<span class="btn '.$st_class.'">'.$icon.'</span>'; ?></td>
                                    <?php endif ?>
                                    <td><a href="insurance.php?car_id=<?php echo $id;?>" class="btn btn-warning">Add / View</a></td>
                                    <td><a href="rent_cost.php?car_id=<?php echo $id;?>" class="btn btn-success">Add / View</a></td>
                                    <td> 
                                    <?php if(in_array('cars', $permission_edit)){?>                                       
                                        <a href="<?php echo 'car_form.php?id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['car_status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="cars" table-id="car_id" >Make stopped</a>
                                    <?php } elseif($rec['car_status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="cars" table-id="car_id" >Make active</a>
                                    <?php }}if(in_array('cars', $permission_delete)){?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="cars" table-id="car_id">Delete</a> 
                                    <?php } ?>
                                    </td>
                                    <?php if($_SESSION['flag_type'] == 1){ ?>
                                        <td style="width:25%">
                                            <form class="commission">
                                                <div class="col-md-4">
                                                    <input type='number' name="commission_value" class='form-control' value="<?php echo $rec['commission_value'] ?>" required>
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="commission_type" class="form-control">
                                                        <option value="1" <?php if(@$rec['commission_type'] == 1) echo 'selected'; ?> >KD</option>
                                                        <option value="2" <?php if(@$rec['commission_type'] == 2) echo 'selected'; ?> >Percent</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="hidden" value="<?php echo $id?>" name="car_id">
                                                    <input type="submit" class="btn btn-primary" name="car_commission" value="update">
                                                </div>
                                            </form>
                                        </td>
                                    <?php  } ?>                                      
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                            <?php } // permission_view ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.commission').submit(function(e){
            e.preventDefault; 
            var data    = $(this).serialize();                
            $.ajax({
                url:'carController.php',
                type:'post',
                data:data,
                success:function(response){
                    location.reload();
                }
            });
            return false
        });
    });
</script>
</body>
</html>