<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	$uploaddir  = '../../images/car_books/'; 
	$instdir	= '../../images/instructions/';  
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	// GET PARENT ID
	if(! isset($_SESSION['parent_id'])) $_SESSION['parent_id'] 	= 0;
	$parent_id 	= $_SESSION['parent_id'];

	if($parent_id > 0){
		$login_provider 	= $parent_id;
	}else{
		$login_provider 	= $login_id;
	}

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){

		/***** GET BRAND TYPES STRAT *****/
		if(isset($_POST['get_types'])):
			$brand_id	= ( isset($_POST['brand']) ? $_POST['brand'] : 0);
			$result		= "<option value=''>Please select type </option>";

			$types_query	= $dbh->query("SELECT * FROM `car_brands` 
											WHERE `parent_id`='{$brand_id}'
											AND `sub_id`	=0
											AND `status`	=1
										");
			if($types_query->rowCount() > 0){
				while ($rec = $types_query->fetch()) {
					$result .= "<option value='".$rec['cat_id']."'>".$rec['cat_name_en']."</option>";
				}
			}
			echo $result;
		/***** GET BRAND TYPES END *****/


		/***** GET TYPE CATEGORIES STRAT *****/
		elseif(isset($_POST['get_cats'])):
			$brand_id	= ( isset($_POST['brand']) ? $_POST['brand'] : 0);
			$type_id	= ( isset($_POST['type']) ? $_POST['type'] : 0);
			$result		= "<option value=''>Please select type </option>";

			$cats_query	= $dbh->query("SELECT * FROM `car_brands` 
											WHERE `parent_id`='{$brand_id}'
											AND `sub_id`	 ='{$type_id}'
											AND `status`	 =1
										");
			if($cats_query->rowCount() > 0){
				while ($rec = $cats_query->fetch()) {
					$result .= "<option value='".$rec['cat_id']."'>".$rec['cat_name_en']."</option>";
				}
			}
			echo $result;
		/***** GET TYPE CATEGORIES END *****/


		/***** ADD & UPDATE CAR START *****/
		elseif (isset($_POST['car_details'])):
			
			// CHECK USER TYPE
			if($_SESSION['flag_type'] == 1){
				$car_owner 		= 1; // ADMIN
				$provider_id 	= 0;
				$approved 		= 1;
			}else{
				$car_owner 		= 2; // PROVIDER
				$provider_id 	= $login_provider;
				$approved 		= 0;
			}

			// GET POST DATA
			$area_id 		= $_POST['area_id'];
			$brand_id 		= $_POST['brand_id'];
			$type_id 		= $_POST['type_id'];
			$cat_id 		= $_POST['cat_id'];
			$model 			= $_POST['model'];
			$engine 		= $_POST['engine'];
			$gear 			= $_POST['gear'];
			$color 			= $_POST['color'];
			$inner_color 	= $_POST['inner_color'];
			$car_plate 		= $_POST['car_plate'];
			$rule_number 	= $_POST['rule_number'];
			$id 			= $_POST['id'];			
			$feature 		= (isset($_POST['feature']) ? $_POST['feature'] : array() );
			$feature 		= implode(',', $feature);
			$delivery_service 		= (isset($_POST['delivery_service']) ? $_POST['delivery_service'] : 0 );
			$delivery_to_airport 	= (isset($_POST['delivery_to_airport']) ? $_POST['delivery_to_airport'] : 0 );
			$delivery_with_driver 	= (isset($_POST['delivery_with_driver']) ? $_POST['delivery_with_driver'] : 0 );
			

			/* CAR BOOK */			
			$Hfile 		='';
			if(isset($_REQUEST['Hfile']))
				$Hfile = $_REQUEST['Hfile'];
				
			if(isset($_REQUEST['chkdel'])) {
				unlink($uploaddir.$Hfile);	
				$Hfile='';
			}

			$image_name   = $Hfile;
			$tempimage    = $_FILES['file_upload']['name'];
			$image        = $_FILES["file_upload"]["name"];
			$uploadedfile = $_FILES['file_upload']['tmp_name'];

			if($image)  {
			  	$filename = stripslashes($image);
			  	$extension = end(explode('.', $image));
			  	$extension = strtolower($extension);
			  	if($extension == "") {
					echo ' Unknown Image extension ';
			  	} else {
					$image_name = rand(0000,9999).$image;
					copy($uploadedfile,$uploaddir.$image_name);
			  	}
			} 							
			/* CAR BOOK */

			// ADD 
			if($_POST['car_details'] == 'add'){
				$add 	= $dbh->query("INSERT INTO `cars` SET 
										`car_owner`				='{$car_owner}',
										`provider_id`			='{$provider_id}',
										`car_brand`				='{$brand_id}',
										`car_type`				='{$type_id}',
										`car_cat`				='{$cat_id}',
										`car_model`				='{$model}',
										`car_color`				='{$color}',
										`car_inner_color`		='{$inner_color}',
										`car_engine`			='{$engine}',
										`area_id`				='{$area_id}',
										`car_gear`				='{$gear}',
										`car_features`			='{$feature}',
										`delivery_service`		='{$delivery_service}',
										`delivery_to_airport`	='{$delivery_to_airport}',
										`delivery_with_driver` 	='{$delivery_with_driver}',
										`approved`				='{$approved}',
										`status`				=1,
										`created_by`			='{$login_id}',
										`created_at`			='{$now}'
									");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Car added Successfuly';
					$car_id 	= $dbh->lastInsertId();

					$add_tarfic = $dbh->query("INSERT INTO `car_traffic` SET 
												`car_id`		='{$car_id}',
												`car_plate`		='{$car_plate}',
												`rule_number`	='{$rule_number}',
												`car_book`		='{$image_name}',
												`created_by`	='{$login_id}',
												`created_at`	='{$now}'	
											");					
				}					 
			}
			// UPDATE
			else{				
				$upadte = $dbh->query("UPDATE `cars` SET 										
										`car_brand`				='{$brand_id}',
										`car_type`				='{$type_id}',
										`car_cat`				='{$cat_id}',
										`car_model`				='{$model}',
										`car_color`				='{$color}',
										`car_inner_color`		='{$inner_color}',
										`car_engine`			='{$engine}',
										`area_id`				='{$area_id}',
										`car_gear`				='{$gear}',
										`car_features`			='{$feature}',
										`delivery_service`		='{$delivery_service}',
										`delivery_to_airport`	='{$delivery_to_airport}',
										`delivery_with_driver` 	='{$delivery_with_driver}',
										`approved`				='{$approved}',										
										`updated_by`			='{$login_id}',
										`updated_at`			='{$now}'
										WHERE 
										`car_id`				='{$id}'
									");
				$add_tarfic = $dbh->query("UPDATE `car_traffic` SET 												
												`car_plate`		='{$car_plate}',
												`rule_number`	='{$rule_number}',
												`car_book`		='{$image_name}',
												`updated_by`	='{$login_id}',
												`updated_at`	='{$now}'
												WHERE 
												`car_id`		='{$id}'
											");

				if($upadte){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Car upadted Successfuly';
				}
			}
			// REDIRECT
			header('Location:index.php');
		
		/***** ADD & UPDATE CAR END *****/	

		

		/***** ADD & UPDATE CAR RENT COST START  *****/	
		elseif (isset($_POST['car_rent_cost'])):
			// GET POST DATA
			$rent_id 	= $_POST['rent_id'];
			$type_id 	= $_POST['type_id'];
			$car_id 	= $_POST['car_id'];
			$cost 		= $_POST['cost'];
			
			// GET MAX INDEX
			$max_index 	= max(array_keys($rent_id));
			//print_r($_POST);die();

			// ADD OR UPDATE

			for ($i=0; $i <= $max_index ; $i++) { 
				
				if(isset($rent_id[$i])){

					$rent_id_txt 	= $rent_id[$i];
					
					// UPDATE  & DELETE
					if($rent_id_txt > 0){
						
						// UPDATE
						if (isset($cost[$i])) {
							$cost_txt 		= $cost[$i];

							$update 	= $dbh->query("UPDATE `car_rent_cost` SET 												
													`rent_type`		='{$i}',
													`rent_cost`		='{$cost_txt}',
													`updated_by`	='{$login_id}',
													`updated_at`	='{$now}'
													WHERE
													`rent_cost_id`	='{$rent_id_txt}'		
												");
							if($update){
								$_SESSION['sql_status_icon'] ='fa-check';
								$_SESSION['sql_status_msg']  ='cost updated Successfuly';
							}
						} // update

						// DELETE
						else{
							$delete 	= $dbh->query("DELETE  FROM `car_rent_cost` 
															WHERE `rent_cost_id`='{$rent_id_txt}'
													");
							if($delete){
								$_SESSION['sql_status_icon'] ='fa-check';
								$_SESSION['sql_status_msg']  ='cost deleted Successfuly';
							}
						} // delete

					} // update & delete


					// ADD
					else{
						if (isset($cost[$i])) {
							$cost_txt 		= $cost[$i];
							$add 	= $dbh->query("INSERT INTO `car_rent_cost` SET 
													`car_id`		='{$car_id}',
													`rent_type`		='{$i}',
													`rent_cost`		='{$cost_txt}',
													`created_by`	='{$login_id}',
													`created_at`	='{$now}'		
												");
							if($add){
								$_SESSION['sql_status_icon'] ='fa-check';
								$_SESSION['sql_status_msg']  ='cost added Successfuly';
							}
						} // if isset cost[i]	

					} // add
				} // if isset rent_id[i]	
					
			} // for

			// REDIRECT
			header('Location:rent_cost.php?car_id='.$car_id);
		
		/***** ADD & UPDATE CAR RENT COST  END *****/


		/***** GET INSURANCE TYPES STRAT *****/
		elseif(isset($_POST['get_company_types'])):			
			$comapny_id	= ( isset($_POST['comapny']) ? $_POST['comapny'] : 0);
			$result		= "<option value=''>Please select comapny </option>";

			$types_query	= $dbh->query("SELECT * FROM `insurance_types` 
											WHERE `company_id`='{$comapny_id}'											
											AND `status`	=1
										");
			if($types_query->rowCount() > 0){
				while ($rec = $types_query->fetch()) {
					$result .= "<option value='".$rec['type_id']."'>".$rec['type_name_en']."</option>";
				}
			}
			echo $result;
		/***** GET INSURANCE TYPES END *****/	


		/***** ADD & UPDATE CAR INSURANCE START *****/
		elseif(isset($_POST['car_insurance'])):
			// GET POST DATA			
			$company_id 		= $_POST['company_id'];
			$insurance_type 	= $_POST['insurance_type'];
			$insurance_start 	= $_POST['insurance_start'];
			$insurance_end 		= $_POST['insurance_end'];
			$car_id 			= $_POST['car_id'];
			$feature 			= (isset($_POST['feature']) ? $_POST['feature'] : array() );
			$feature 			= implode(',', $feature);

			// CHECK IF THE HAS HAS INSURANCE
			$check 	= $dbh->query("SELECT * FROM `car_insurance` WHERE `car_id`='{$car_id}'");
			
			// UPDATE
			if($check->rowCount() > 0){
				$insurance 		= $check->fetch();
				$insurance_id 	= $insurance['insurance_id'];
				$update 	= $dbh->query("UPDATE `car_insurance` SET 
												`company_id`			='{$company_id}',
												`insurance_type`		='{$insurance_type}',
												`insurance_features`	='{$feature}',
												`insurance_strat_date`	='{$insurance_start}',
												`insurance_end_date`	='{$insurance_end}',
												`updated_by`			='{$login_id}',
												`updated_at`			='{$now}'
												WHERE
												`insurance_id`			='{$insurance_id}'	
										");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='insurance updated Successfuly';
				}
			}

			// ADD
			else{
				//die('ADD');
				$add 	= $dbh->query("INSERT INTO  `car_insurance` SET
												`car_id`				='{$car_id}', 
												`company_id`			='{$company_id}',
												`insurance_type`		='{$insurance_type}',
												`insurance_features`	='{$feature}',
												`insurance_strat_date`	='{$insurance_start}',
												`insurance_end_date`	='{$insurance_end}',
												`created_by`			='{$login_id}',
												`created_at`			='{$now}'												
										");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='insurance added Successfuly';
				}	
			}

			// REDIRECT
			header('Location:insurance.php?car_id='.$car_id);
		
		/***** ADD & UPDATE CAR INSURANCE END *****/	


		/***** ADD & UPDATE CAR INSTRUCTIONS START *****/	
		elseif(isset($_POST['car_instructions'])):
			//print_r($_POST);
			// GET POST DATA
			$car_id 	= $_POST['car_id'];
			$img_id 	= $_POST['img_id'];

			/* IMAGE */			
			$Hfile 		='';
			if(isset($_REQUEST['Hfile']))
				$Hfile = $_REQUEST['Hfile'];
				
			if(isset($_REQUEST['chkdel'])) {
				unlink($instdir.$Hfile);	
				$Hfile='';
			}

			$image_name   = $Hfile;			
			$image        = $_FILES["file_upload"]["name"];
			$uploadedfile = $_FILES['file_upload']['tmp_name'];
			$error 		  = $_FILES['file_upload']['error'];				

			if($error > 0 && $Hfile == ''){
				$_SESSION['sql_status_msg'] = ' Please Select Image ';
				header('Location:instructions.php?car_id='.$car_id);	
				die();		
			}		

			if($image)  {
			  	$filename 	= stripslashes($image);
			  	$extension 	= end(explode('.', $image));
			  	$extension 	= strtolower($extension);
			  	if($extension == "") {
					$_SESSION['sql_status_msg']= ' Unknown Image extension ';
			  	} else {
					$image_name = rand(0000,9999).$image;
					copy($uploadedfile,$instdir.$image_name);
			  	}
			}
			/* IMAGE */
			
			// ADD
			if($_POST['car_instructions'] == 'add'){
									
				$add  	= $dbh->query("INSERT INTO `car_instructions` SET
											`car_id`			='{$car_id}',
											`instruction_image`	='{$image_name}',
											`status`			=1,
											`created_by`		='{$login_id}',	
											`created_at`		='{$now}'	
										");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Image added Successfuly';
				}						 
			}

			// UPDATE
			else{
				$update  	= $dbh->query("UPDATE `car_instructions` SET											
											`instruction_image`	='{$image_name}',											
											`updated_by`		='{$login_id}',	
											`updated_at`		='{$now}'
											WHERE 
											`instruction_id`	='{$img_id}'	
										");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Image updated Successfuly';
				}
			}
			
			// REDIRECT
			header('Location:instructions.php?car_id='.$car_id);		

		/***** ADD & UPDATE CAR INSTRUCTIONS END *****/	



		/***** UPDATE CAR COMMISSION START *****/	
		elseif (isset($_POST['commission_value'])):
			// GET POST DATA
			$commission_value	= $_POST['commission_value'];
			$commission_type	= $_POST['commission_type'];
			$car_id				= $_POST['car_id'];

			$update 	= $dbh->query("UPDATE `cars` SET 
										`commission_value`		='{$commission_value}',
										`commission_type`		='{$commission_type}'
										WHERE
										`car_id`				='{$car_id}'
									");
			unset($_SESSION['sql_status_msg']);
		/***** UPDATE CAR COMMISSION END *****/					
		endif;
	} // IF ISSET POST
?>
