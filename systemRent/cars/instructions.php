<?php 
	require_once("../module/include_mod.php");
	$path 	= "../../images/instructions/";

	if(! in_array('cars', $permission_edit) && ! in_array('cars', $permission_add)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}	

	// COUNTRY ID
	$country_id 	= (isset($_SESSION['country']) ? $_SESSION['country'] : 1);


	if(! isset($_GET['car_id'])) {header('Location: index.php');}

	$car_id 	= $_GET['car_id'];

	// CAR
	$car_query = $dbh->query("SELECT * FROM `cars` WHERE `car_id`='{$car_id}' ");

	if ($car_query->rowCount()>0) {
		$car 	= $car_query->fetch();

		// ADD OR UPDATE
		$img_id = 0 ;if(isset($_GET['img_id'])) $img_id = $_GET['img_id'];		
		if($img_id > 0){
			$action 	= 'update';
			$img_query 	= $dbh->query("SELECT * FROM `car_instructions`
											WHERE `instruction_id`='{$img_id}' 											
										");
			if($img_query->rowCount()>0){
				$img 	= $img_query->fetch();
			}
		}else{
			$action 	= 'add';
		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	  
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">cars</a></li>		                    
		                	<li class="active">car instructions</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div> 

		                            	<?php if(isset($_SESSION['sql_status_msg'])){?>
		                            	<div class="alert alert-success">
			                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
			                                <div class="notification-info">
			                                    <ul class="clearfix notification-meta">
			                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
			                                    </ul><p><br></p>
			                                </div>
			                            </div>
			                            <?php 
			                                unset($_SESSION['sql_status_msg']);
			                                }
			                            ?>

			                            <form action="carController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
											
											<!-- INSTRUCTION -->
		                                    <div class="form-group">
		                                      	<label class="col-lg-2 control-label">Image:</label>
		                                      	<div class="col-lg-6" style="margin-top: 12px;">
			                                        <input type="hidden" name="Hfile" value="<?php echo @$img['instruction_image'];?>">
			                                        <?php if((trim(@$img['instruction_image'])) and (file_exists($path.@$img['instruction_image']))) {?>
			                                            <a href="<?php echo $path.@$img['instruction_image'];?>" target="_new" style="text-decoration:underline">Show Image</a>
			                                            <div class="clearfix"></div>			                                            
			                                        <?php }?>
			                                        <input type="file" name="file_upload">
		                                      	</div>
		                                    </div>

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="car_id" value="<?php echo $car_id; ?>">
			                                	<input type="hidden" name="img_id" value="<?php echo $img_id; ?>">
			                                    <div class="col-lg-2">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="car_instructions" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>
			                            </form>	

			                             <!-- images -->
			                            <?php if($img_id == 0){?>
			                            <table class="table table-striped">
			                                <thead>
			                                    <tr>
			                                        <th>#</th>
			                                        <th>Image</th>	
			                                        <!-- <th>Order</th>			                                         -->
			                                        <th>Action</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                <?php
			        							$stmt = $dbh->query("SELECT * FROM `car_instructions` 
			        													WHERE status < 3 
			        													AND `car_id`='{$car_id}'
		        													");
			        							$stmt->execute();
			        							if($stmt->rowCount() > 0) {
			                                        $ceq = 0;
			        							    while($rec = $stmt->fetch()) {
			                                            $img_id    = $rec['instruction_id'];
			                                ?>
			                                <tr>
			                                    <td><?php echo ++$ceq; ?></td>
			                                    <td>
			                                    	<div class="view-img"><img src="<?php echo $path.@$rec['instruction_image'];?>"></div>			                                    	
			                                    </td>
			                                    <!-- <td></td>			                                     -->
			                                    <td> 		                                        
			                                        <a href="instructions.php?car_id=<?php echo $car_id.'&img_id='.$img_id;?>" class="btn btn-warning">Edit</a>
			                                    <?php if($rec['status']==1) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $img_id ;?>" table="car_instructions" table-id="instruction_id" >Enable </a>
			                                    <?php } elseif($rec['status']==2) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $img_id ;?>" table="car_instructions" table-id="instruction_id" >Disable </a>
			                                    <?php }?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $img_id ;?>" table="car_instructions" table-id="instruction_id">Delete</a> 
			                                    </td>                                        
			                                </tr>
			                                <?php 
			                                        } // while
			    							    } // if
			                                ?>
			                                </tbody>
			                            </table>
			                            <?php } // if img_id == 0?>

                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php //include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<?php } ?>
<script src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!--<script src="../js/advanced-form.js"></script>    -->
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				username:"required",
				email:{required:true,email:true},
				password:"required",
				res_name:"required",
				res_mobile:"required",
				name_en:"required",
				name_ar:"required",				
				tele:{required:true,number:true}				
			}
		});

	    /***** GET COPMPANY TRYPES ******/
		$('body').on('change','select[name=company_id]',function(){
			var comapny = $(this).val();
			var action 	= $('form').attr('action');
			$('select[name=insurance_type]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_company_types':1,'comapny':comapny},
				success:function(response){
					$('select[name=insurance_type]').append(response);					
					//alert(response);
				}
			});
			return false;
		});

	});
</script>
</body>
</html>