<?php 
	ob_start();
	require_once("../module/include_mod.php");
	$path   ='../../images/car_books/'; 
	
	$id 			= 0; if(isset($_GET['id'])) $id = $_GET['id'];

	if($id == 0 && ! in_array('cars', $permission_add)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}
	elseif($id > 0 && ! in_array('cars', $permission_edit)) {
		header('Location:../main/main.php');
		$_SESSION['permission_msg'] 	= "You don't have permission to access this page";
	}

	// COUNTRY ID
	$country_id 	= (isset($_SESSION['country']) ? $_SESSION['country'] : 1);

	// GET COUNTRIES
	$areas 		= array();
	if($_SESSION['flag_type'] == 1){
		$where 	= "WHERE `parent_id` > 0";
	}else{
		$where 	= "WHERE `parent_id`='{$country_id}'";
	}
	$area_query = $dbh->query("SELECT * FROM `country_city` 
									$where
									AND `status_flag`=1
								");
	if($area_query->rowCount()> 0){
		$areas 	= $area_query->fetchall();
	}

	// GET BRANDS
	$brands 	= array();
	$brand_query 	= $dbh->query("SELECT * FROM `car_brands` 
									WHERE `parent_id`	=0 
									AND `status`		=1
								");
	if($brand_query->rowCount()> 0){
		$brands 	= $brand_query->fetchall();
	}


	// GET CAR FEATURES
	$features_query 	= $dbh->query("SELECT * FROM `features` WHERE `status`=1 AND `feature_for`=2");
	if($features_query->rowCount()>0){
		$features 		= $features_query->fetchall();		
	}


	// GET COLORS
	$colors_query 	= $dbh->query("SELECT * FROM `features` WHERE `status`=1 AND `feature_for`=4");
	if($colors_query->rowCount()>0){
		$colors 		= $colors_query->fetchall();		
	}


	$car_features	= array();
	if($id > 0){
		$action = 'update';			
		$bc 	= 'updade car';
		
		// CAR  
		$car_query 	= $dbh->query("SELECT * FROM `cars` a
									INNER JOIN `car_traffic` b ON(b.car_id=a.car_id)									
									WHERE a.car_id='{$id}'
								");
		if ($car_query->rowCount()>0) {
			$car 			= $car_query->fetch();
			$car_features   = explode(',', $car['car_features']);

			// GET BRAND TYPES
			$car_brand 		= $car['car_brand'];
			$brand_types 	= array();
			$types_query	= $dbh->query("SELECT * FROM `car_brands` 
											WHERE `parent_id`='{$car_brand}'
											AND `sub_id`	=0
											AND `status`	=1
										");
			if($types_query->rowCount() > 0){
				$brand_types 	= $types_query->fetchall();
			}

			// GET TYPE CATS
			$car_type 		= $car['car_type'];
			$type_cats 		= array();
			$cats_query		= $dbh->query("SELECT * FROM `car_brands` 
											WHERE `parent_id`='{$car_brand}'
											AND `sub_id`	='{$car_type}'
											AND `status`	=1
										");
			if($cats_query->rowCount() > 0){
				$type_cats 	= $cats_query->fetchall();
			}
		}

	}else{
		$action = 'add';
		$bc 	= 'add car';


	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
	<style type="text/css">
		ul.nav-tabs{
			background: #ccc;
		}
		ul.nav-tabs li{
			width: 50%;
		}
		ul.nav-tabs li a{
			font-weight: bold;
		}
	</style>
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">cars</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <div class="form-tab">
			                            	<form action="carController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">											
										  		<!-- Nav tabs -->
										  		<ul class="nav nav-tabs" role="tablist">
										    		<li role="presentation" class="x_c active car_details"><a href="#car_details" aria-controls="car_details" role="tab" data-toggle="tab">Car details</a></li>
										    		<li role="presentation" class="x_c traffic_details"><a href="#traffic_details" aria-controls="traffic_details" role="tab" data-toggle="tab">Traffic details</a></li>
										    		<?php /*
										    		<li role="presentation" class="x_c insurance_details"><a href="#insurance_details" aria-controls="insurance_details" role="tab" data-toggle="tab">Insurance details</a></li>
										    		<li role="presentation" class="x_c rent_details"><a href="#rent_details" aria-controls="rent_details" role="tab" data-toggle="tab">Rent details</a></li>
										    		<li role="presentation" class="x_c rent_cost_details"><a href="#rent_cost_details" aria-controls="rent_cost_details" role="tab" data-toggle="tab">Rent Cost details</a></li>
										    		*/ ?>
										  		</ul>
												<!-- Tab panes -->
												<div class="tab-content">
													<!-- Car details start  -->
												    <div role="tabpanel" class="tab-pane active" id="car_details">
												    	<!-- AREA -->
												    	<div class="form-group">
															<label class="col-lg-2 control-label">Area</label>
															<div class="col-md-4">
																<select name="area_id" class="form-control">
																	<option value="">Please select area</option>
																	<?php foreach ($areas as $area):?>
																	<option value="<?php echo $area['id_country'] ?>" <?php if(@$car['area_id'] == $area['id_country'] ) echo 'selected'; ?> ><?php echo $area['name_en']; ?></option>
																	<?php endforeach; ?>
																</select>
															</div>
														</div>

														<!-- BRAND -->
												    	<div class="form-group">
															<label class="col-lg-2 control-label">Brand</label>
															<div class="col-md-4">
																<select name="brand_id" class="form-control">
																	<option value="">Please select brand</option>
																	<?php foreach ($brands as $brand):?>
																	<option value="<?php echo $brand['cat_id'] ?>" <?php if(@$car['car_brand'] == $brand['cat_id'] ) echo 'selected'; ?> ><?php echo $brand['cat_name_en']; ?></option>
																	<?php endforeach; ?>
																</select>
															</div>
														</div>

														<!-- BRAND -->
												    	<div class="form-group">
															<label class="col-lg-2 control-label">Type</label>
															<div class="col-md-4">
																<select name="type_id" class="form-control" required>
																	<option value="">Please select type</option>
																	<?php 
																		if($id > 0):
																			foreach ($brand_types as $rec):
																	?>
																	<option value="<?php echo $rec['cat_id']?>" <?php if($car['car_type'] == $rec['cat_id']) echo 'selected'; ?> > <?php echo $rec['cat_name_en'];?></option>
																	<?php			
																			endforeach;
																		endif;
																	?>																
																</select>
															</div>
														</div>

														<!-- BRAND -->
												    	<div class="form-group">
															<label class="col-lg-2 control-label">Category</label>
															<div class="col-md-4">
																<select name="cat_id" class="form-control" required>
																	<option value="">Please select category</option>
																	<?php 
																		if($id > 0):
																			foreach ($type_cats as $rec):
																	?>
																	<option value="<?php echo $rec['cat_id']?>" <?php if($car['car_cat'] == $rec['cat_id']) echo 'selected'; ?> > <?php echo $rec['cat_name_en'];?></option>
																	<?php			
																			endforeach;
																		endif;
																	?>																
																</select>
															</div>
														</div>

														<!-- MODEL -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Model</label>
															<div class="col-md-4">
																<select name="model" class="form-control">
																	<option value="">Please select model</option>
																	<?php for($i= 1970; $i  <= date('Y'); $i++):?>
																	<option value="<?php echo $i; ?>" <?php if($i ==  @$car['car_model'] ) echo 'selected'; ?> ><?php echo $i; ?></option>
																	<?php endfor; ?>
																</select>															
															</div>
														</div>

														<!-- ENGINE -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Engine</label>
															<div class="col-md-4">
																<input type="text" name="engine" class="form-control required" value="<?php echo @$car['car_engine'];?>" required>
															</div>
														</div>
														
														<!-- GEAR -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Gear</label>
															<div class="col-md-4">
																<input type="text" name="gear" class="form-control required" value="<?php echo @$car['car_gear'];?>" required>
															</div>
														</div>

														<!-- COLOR -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Color</label>
															<div class="col-md-4">																
																<select name="color" class="form-control" required>
																	<option value="">Please select color</option>
																	<?php foreach ($colors as $rec): ?>
																	<option value="<?php echo $rec['feature_id']?>" <?php if(@$car['car_color'] == $rec['feature_id']) echo 'selected'; ?> > <?php echo $rec['feature_name_en'];?></option>
																	<?php endforeach; ?>																
																</select>
															</div>
														</div>

														<!-- INNER COLOR -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Inner Color</label>
															<div class="col-md-4">																
																<select name="inner_color" class="form-control" required>
																	<option value="">Please select inner color</option>
																	<?php foreach ($colors as $rec): ?>
																	<option value="<?php echo $rec['feature_id']?>" <?php if(@$car['car_inner_color'] == $rec['feature_id']) echo 'selected'; ?> > <?php echo $rec['feature_name_en'];?></option>
																	<?php endforeach; ?>																
																</select>
															</div>
														</div>

														<!-- FEATURES -->
					                                    <div class="form-group">
															<label class="col-lg-2 control-label">Features</label>
															<div class="col-md-9">
																<?php foreach ($features as $feature):?>
																<label class="checkbox-inline">
																	<input name="feature[]" type="checkbox" value="<?php echo $feature['feature_id']?>" <?php if(in_array($feature['feature_id'], $car_features)) echo 'checked';?>>
																	<?php echo $feature['feature_name_en'];?>
																</label>
																<?php endforeach; ?>													
															</div>
														</div>

														<!-- DELIVERY SERVICE -->
					                                    <div class="form-group">
															<label class="col-lg-2 control-label">Delivery Service</label>
															<div class="col-md-9">																
																<label class="checkbox-inline">
																	<input name="delivery_service" type="checkbox" value="1" <?php if(@$car['delivery_service'] == 1) echo 'checked';?>>																	
																</label>																											
															</div>
														</div>
														
														<!-- DELIVERY TO AIR PORT -->
					                                    <div class="form-group">
															<label class="col-lg-2 control-label">Delivery To airport </label>
															<div class="col-md-9">																
																<label class="checkbox-inline">
																	<input name="delivery_to_airport" type="checkbox" value="1" <?php if(@$car['delivery_to_airport'] == 1) echo 'checked';?>>																	
																</label>																											
															</div>
														</div>

														<!-- DELIVERY WITH DRIVER -->
					                                    <div class="form-group">
															<label class="col-lg-2 control-label">Delivery With Driver</label>
															<div class="col-md-9">																
																<label class="checkbox-inline">
																	<input name="delivery_with_driver" type="checkbox" value="1" <?php if(@$car['delivery_with_driver'] == 1) echo 'checked';?>>																	
																</label>																											
															</div>
														</div>

													

														<!-- NEXT -->
														<a href="#traffic_details" aria-controls="traffic_details" class="next btn btn-info pull-right" role="tab" data-toggle="tab">Next</a>
												    </div>
												    <!-- Car details end   -->
												   
												    <!-- Traffic details start  -->
												    <div role="tabpanel" class="tab-pane" id="traffic_details">
												    	<!-- CAR PLAE -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Car Plate</label>
															<div class="col-md-4">
																<input type="text" name="car_plate" class="form-control required" value="<?php echo @$car['car_plate'];?>" required>
															</div>
														</div>

														<!-- RULE NUMBER -->
														<div class="form-group">
															<label class="col-lg-2 control-label">Rule Number</label>
															<div class="col-md-4">
																<input type="text" name="rule_number" class="form-control required" value="<?php echo @$car['rule_number'];?>" required>
															</div>
														</div>

														<!-- CAR BOOK -->	
														<div class="form-group">
					                                      	<label class="col-lg-2 control-label">Car Book</label>
					                                      	<div class="col-lg-6">
						                                        <input type="hidden" name="Hfile" value="<?php echo @$car['car_book'];?>">
						                                        <?php if((trim(@$car['car_book'])) and (file_exists($path.@$car['car_book']))) {?>
						                                            <a href="<?php echo $path.@$car['car_book'];?>" target="_new" style="text-decoration:underline">Show Image</a>
						                                            <div class="clearfix"></div>
						                                            <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel"></div>
						                                            <div style="width:90% !important;float:left" align="left">delete file.</div>
						                                            <d="clearfix"><iv class/div>
						                                        <?php }?>
						                                        <input type="file" name="file_upload">
					                                      	</div>
					                                    </div>

					                                    <!-- NEXT -->
														<!-- <a href="#insurance_details" aria-controls="insurance_details" class="next btn btn-info pull-right" role="tab" data-toggle="tab">Next</a> -->
			                                    		<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                    		<button class="btn btn-primary pull-right" type="submit" value="<?php echo $action; ?>" name="car_details">Submit</button>
			                                    		<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>

												    </div>
												    <!-- Traffic details end  -->
												    <?php /*
												    <!-- Insurance details start  -->
												    <div role="tabpanel" class="tab-pane" id="insurance_details">
												    	
												    	<!-- INSURANCE -->
					                                    <div class="form-group">
															<label class="col-lg-2 control-label">Is insurance </label>
															<div class="col-md-9">																
																<label class="checkbox-inline">
																	<input name="insurance" type="checkbox" value="1">
																</label>																												
															</div>
														</div>

												    </div>
												    <!-- Insurance details end  -->
												   
												    <!-- Rent details start  -->
												    <div role="tabpanel" class="tab-pane" id="rent_details">settings</div>
												    <!-- Rent details end  -->
												    <!-- Rent Cost details start  -->
												    <div role="tabpanel" class="tab-pane" id="rent_cost_details">settings</div>
												    <!-- Rent Cost details end  -->
												    */ ?>
												</div><!-- Tab panes end -->
			                            	</form>
										</div>	
										<?php /* ?>										
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                    <div class="col-lg-11">
			                                    </div>
			                                </div>
			                            <?php */ ?>    
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				area_id:"required",
				brand_id:"required",
				type_id:"required",
				model:"required",
				engine:"required",
				gear:"required",
				color:"required",
				inner_color:"required",
				car_plate:"required",
				rule_number:"required",
			}
		});

		/*****  NEXT *****/
		$('body').on('click','.next',function(){
			var class_	= $(this).attr('aria-controls'); 			
			$('.x_c').removeClass('active');
			$('.'+class_).addClass('active');
			
		});

		/***** GET BRAND TRYPES ******/
		$('body').on('change','select[name=brand_id]',function(){
			var brand 	= $(this).val();
			var action 	= $('form').attr('action');
			$('select[name=type_id]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_types':1,'brand':brand},
				success:function(response){
					$('select[name=type_id]').append(response);					
				}
			});
			return false;
		});

		/***** GET TRYPE CATS ******/
		$('body').on('change','select[name=type_id]',function(){
			var type 	= $(this).val();
			var brand 	= $('select[name=brand_id]').val();
			var action 	= $('form').attr('action');
			$('select[name=cat_id]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_cats':1,'brand':brand,'type':type},
				success:function(response){
					$('select[name=cat_id]').append(response);					
				}
			});
			return false;
		});
	});
</script>
</body>
</html>