<?php 
	require_once("../module/include_mod.php");


	if(! isset($_GET['car_id'])) {header('Location: index.php');}

	$car_id 	= $_GET['car_id'];

	// CAR
	$car_query = $dbh->query("SELECT * FROM `cars` WHERE `car_id`='{$car_id}' ");

	if ($car_query->rowCount()>0) {
			$car 	= $car_query->fetch();

			// GET RENT TYPES
			$types 		= array();
			$type_query = $dbh->query("SELECT * FROM `features` WHERE `feature_for`=5 AND `status`=1");
			if($type_query->rowCount() > 0){
				$types 	= $type_query->fetchall();
			}


			// ADD OR UPDATE		
			$action 	= 'add';
			$time_query = $dbh->query("SELECT * FROM `car_rent_cost`
											WHERE `car_id`='{$car_id}' 											
										");
			if($time_query->rowCount()>0){				
				$action 	= 'update';
			}	
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	
	<style type="text/css">
		label{
			text-align: left !important;
			text-transform: capitalize;
		}
		.control-group {
			padding: 5px 0;
		}
		input[type=checkbox]{
			height: 25px;
		}
	</style>   
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">cars</a></li>		                    
		                	<li class="active">rent cost</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div> 

		                            	<?php if(isset($_SESSION['sql_status_msg'])){?>
		                            	<div class="alert alert-success">
			                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
			                                <div class="notification-info">
			                                    <ul class="clearfix notification-meta">
			                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
			                                    </ul><p><br></p>
			                                </div>
			                            </div>
			                            <?php 
			                                unset($_SESSION['sql_status_msg']);
			                                }
			                            ?>

			                            <form action="carController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
											
											<?php 											
												foreach ($types as $type):													
													$type_id 	= $type['feature_id'];
													$rent_id 	= 0;
													$rent_cost 	= '';
													$cost_query = $dbh->query("SELECT * FROM `car_rent_cost` 
																					WHERE `car_id`	='{$car_id}'
																					AND `rent_type`='{$type_id}'
																				");
													if($cost_query->rowCount()>0){
														$cost 		= $cost_query->fetch();
														$rent_id 	= $cost['rent_cost_id'];
														$rent_cost 	= $cost['rent_cost'];
													}
											?>
												<div class="control-group row">
		                                            <label class="control-label col-md-2"><?php echo $type['feature_name_en'].' :';?></label>
		                                            <div class="col-md-1">
		                                                <input type="hidden"  name="rent_id[<?php echo $type_id?>]" value="<?php echo $rent_id?>" >
		                                                <input type="checkbox"  name="type_id[<?php echo $type_id?>]" id="<?php echo $type_id?>" class="type" <?php if($rent_id > 0) echo 'checked';?>>
		                                            </div>
		                                            <div class="controls col-md-6">	
		                                            	<div class="col-md-6">
		                                            		<input  type='text' name="cost[<?php echo $type_id?>]" class="form-control <?php echo $type_id?>" value="<?php echo $rent_cost?>" required >                                               
		                                            	</div> 		                                            	
		                                            </div>
		                                        </div>
											<?php 													
												endforeach;
											?>

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="car_id" value="<?php echo $car_id; ?>">
			                                    <div class="col-lg-4">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="car_rent_cost" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>			                          
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php //include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				username:"required",
				email:{required:true,email:true},
				password:"required",
				res_name:"required",
				res_mobile:"required",
				name_en:"required",
				name_ar:"required",				
				tele:{required:true,number:true}				
			}
		});

		$(".type").each(function(){ 
            var class_name = $(this).attr('id');                      
            if ($(this).is(':checked')) {                                               
                $("."+class_name).attr('disabled',false);
            }else{
                $("."+class_name).attr('disabled',true);
            }
        });
        // ON CHANGE
        $('body').on('change','input[name^=type_id]',function(){
            var class_name = $(this).attr('id'); 
            $("."+class_name).val('');
            if ($(this).is(':checked')) {                                               
                $("."+class_name).attr('disabled',false);
            }else{
                $("."+class_name).attr('disabled',true);
            }
        });

	});
</script>
</body>
</html>