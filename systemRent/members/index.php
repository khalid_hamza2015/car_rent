<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                <li class="active">Members</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                        <div class="alert alert-success">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul><p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
                        <div class="alert alert-danger">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul><p><br></p>
                            </div>
                        </div>
                        <?php }?>
						<form method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">you can search by mobile number or name or email:</label>
                                <div class="col-lg-5"><input type="text" name="search_v" class="form-control" value="<?=(! empty($_POST['search_v'])?$_POST['search_v']:"")?>"></div>
                                <div class="col-lg-3"><button class="btn btn-primary" type="submit" name="submit">Filter</button></div>
                            </div>
                        </form>                        
                        <div class="space15"></div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Weight</th>
                                    <th>Height</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$i=0;
							$where = (! empty($_POST['search_v'])?" AND (txt_name LIKE '%{$_POST['search_v']}%' or txt_phone LIKE '%{$_POST['search_v']}%' or txt_email LIKE '%{$_POST['search_v']}%') ":"");
							$stmt = $dbh->prepare("SELECT * FROM `members` WHERE status_flag < 3 {$where};");
							$stmt->execute();
							if($stmt->rowCount() > 0) {
							  while($rec = $stmt->fetch()) {?>
                            <tr style=" <?php if($rec['status_flag']==2) echo 'background-color:red !important;color:#FFF !important'; else echo 'background-color:#FFF !important;color:#000 !important';?>">
                                <td><?php echo ++$i;?></td>
                                <td><?php echo $rec['txt_name']?></td>
                                <td><?php echo ($rec['gender']==1?"male":"female")?></td>
                                <td><?php echo $rec['txt_phone'];?></td>
                                <td><?php echo $rec['txt_email'];?></td>
                                <td><?php echo $rec['weight'];?></td>
                                <td><?php echo $rec['height'];?></td>
                                <td>
                                <?php if($rec['status_flag']==1) {?>
                                <a href="javascript: return void(0)" class="btn btn-danger btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=2&table=members')">Make Disable</a>
                                <?php } elseif($rec['status_flag']==2) {?>
                                <a href="javascript: return void(0)" class="btn btn-success btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=1&table=members')">Make Enable</a>
                                <?php }?></td>
                            </tr>
                            <?php }
							}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</html>