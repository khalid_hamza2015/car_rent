<?php require_once("../module/include_mod.php");?>
    <link rel="stylesheet" type="text/css" href="../js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="../js/bootstrap-timepicker/css/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="../js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="../js/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="../js/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="../js/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.css" />    
</head>
<body>
<section id="container">
<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                	<li class="active">Users</li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <?php 
							$id = 0;
							if(isset($_GET['id']))
								$id = $_GET['id'];
								
							$name = '';
							$uname = '';
							$password = '';
							$flag_type = 0;
							$department_id = 0;
							$dataset = $classObj->getTableDataByID("users",$id);
							if($dataset) {
								$rec = mysql_fetch_array($dataset);
								$name = $rec['name'];
								$uname = $rec['uname'];
								$password = $rec['password'];
								$flag_type = $rec['flag_type'];
								//$department_id = $rec['department_id'];
							}?>
                            <form action="code_contract.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Name:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="name" maxlength="255" value="<?php echo $name?>" required /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Username:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="uname" maxlength="255" value="<?php echo $uname?>" required /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Password:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="password" maxlength="255" value="<?php echo $password?>" required /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Type:</label>
                                    <div class="col-lg-6">
                                        <select name="flag_type" class="form-control">
                                        	<option value="1" <?php if($flag_type==1) echo 'selected';?>>Management</option>
                                            <option value="2" <?php if($flag_type==2) echo 'selected';?>>Staff</option>
                                        </select> 
                                    </div>
                                </div>
                                <?php /*?><div class="form-group">
                                    <label class="col-lg-3 control-label">القسم</label>
                                    <div class="col-lg-6">
                                    <select name="department_id" class="form-control" required>
                                        <option value="" selected>الرجاء الإختيار</option><?php 
                                        $datasetCategories = $classObj->checkByTableNameWithCondition("estimations"," `status_flag` = 1 AND `cat_id` = 0 AND `type_flag` = 5 ");
                                        if($datasetCategories) {
                                          while($rec_cat = mysql_fetch_array($datasetCategories)) {?>
                                            <option value="<?php echo $rec_cat['id']?>" <?php if($rec_cat['id']==$department_id) echo 'selected';?>><?php echo $rec_cat['txt_name']?></option>
                                        <?php }
                                        }?>
									</select>
                                    </div>
                                </div><?php */?>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6"><button class="btn btn-primary" type="submit">Submit</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
<!--right sidebar end-->
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="../js/jquery.js"></script>
<script src="../js/jquery-1.8.3.min.js"></script>
<script src="../bs3/js/bootstrap.min.js"></script>
<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/bootstrap-switch.js"></script>
<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="../js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="../js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="../js/select2/select2.js"></script>
<script src="../js/select-init.js"></script>
<script src="../js/scripts.js"></script>
<script src="../js/toggle-init.js"></script>
<script src="../js/advanced-form.js"></script>
<script src="../js/easypiechart/jquery.easypiechart.js"></script>
<script src="../js/sparkline/jquery.sparkline.js"></script>
<script src="../js/flot-chart/jquery.flot.js"></script>
<script src="../js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../js/flot-chart/jquery.flot.resize.js"></script>
<script src="../js/flot-chart/jquery.flot.pie.resize.js"></script>
</body>
</html>