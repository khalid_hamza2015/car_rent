<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <!-- page start-->
    <div class="row">
        <div class="col-md-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                <li class="active">Users</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <div class="clearfix" style="padding-bottom:10px">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                            <div class="input-group m-bot15" style="float:left"></div>
                            <div class="btn-group pull-right"><a class="btn btn-primary" href="form_contract.php?id=0">Add New <i class="fa fa-plus"></i></a></div>
                         </form>
                        </div>
                        <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="space15"></div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Type</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
							$dataset = $classObj->checkByTableNameWithCondition("users"," `flag_status` = 1 ");
							if($dataset) {
							  while($rec = mysql_fetch_array($dataset)) {?>
                            <tr>
                                <td><?php echo $rec['name'];?></td>
                                <td><?php echo $rec['uname'];?></td>
                                <td><?php 
								switch($rec['flag_type']) {
									case 1:
										echo "Management";
										break;
									case 2:
										echo "Staff";
										break;
								}?></td>
                                <td><a class="btn btn-warning" href="form_contract.php?id=<?php echo $rec['id']?>">Edit</a></td>
                                <td><a href="javascript: return void(0)" class="btn btn-danger btn-sm" onClick="javascript: confirmDel('change_status.php?id=<?php echo $rec['id']?>&status_flag=2&table=users')">Delete</a></td>
                            </tr>
                            <?php }
							}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start--><div class="right-sidebar"><?php include("../module/right_mod.php");?></div><!--right sidebar end-->
</section>
<?php include("../module/footer_mod.php");?>
</body>
</html>