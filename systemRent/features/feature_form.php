<?php 
	require_once("../module/include_mod.php");

	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

		$type   = (isset($_GET['type']) ? $_GET['type'] : 0);

		switch ($type) {
			case 1:
				$bc_type = 'provider feature';
				break;
			case 2:
				$bc_type = 'car feature';
				break;
			case 3:
				$bc_type = 'insurance feature';
				break;
			case 4:
				$bc_type = 'colors';
				break;
			case 5:
				$bc_type = 'rent periods';
				break;			
			default:
				$bc_type = ' ';
				break;
		}	

		$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

		if($id > 0){
			$action = 'update';			
			$bc 	= 'updade '.$bc_type;
			
			// FEATURE 
			$feature_query 	= $dbh->query("SELECT * FROM `features` WHERE `feature_id`='{$id}'");
			if ($feature_query->rowCount()>0) {
				$feature 	= $feature_query->fetch();		
			}

		}else{
			$action = 'add';
			$bc 	= 'add '.$bc_type;


		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                     <li><a href="index.php?type=<?php echo $type;?>">Features</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="featureController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">											

			                            	<!-- NAME EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Name</label>
												<div class="col-md-4">
													<input type="text" name="name_en" class="form-control required" value="<?php echo @$feature['feature_name_en'];?>" required>
												</div>
											</div>

											<!-- NAME AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Name</label>
												<div class="col-md-4">
													<input type="text" name="name_ar" class="form-control required" value="<?php echo @$feature['feature_name_ar'];?>" required>
												</div>
											</div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                	<input type="hidden" name="type" value="<?php echo $type; ?>">
			                                    <div class="col-lg-4">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php?type=<?php echo $type?>" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				name_en:"required",
				name_ar:"required",				
			}
		});
	});
</script>
</body>
</html>