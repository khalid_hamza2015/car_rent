<?php 
    require_once("../module/include_mod.php");
    if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

    $type   = (isset($_GET['type']) ? $_GET['type'] : 0);

    switch ($type) {
        case 1:
            $bc_type = 'providers fearures';
            break;
        case 2:
            $bc_type = 'cars fearures';
            break;
        case 3:
            $bc_type = 'insurance fearures';
            break;
        case 4:
            $bc_type = 'colors';
            break;
        case 5:
            $bc_type = 'rent periods';
            break;          
        default:
            $bc_type = 'fearures';
            break;
    }
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active"><?php echo $bc_type;?></li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="feature_form.php?type=<?php echo $type;?>" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name En</th>
                                        <th>Name Ar</th>                                                                                                                
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
        							$stmt = $dbh->query("SELECT * FROM `features` 
                                                            WHERE status < 3 
                                                            AND `feature_for`='{$type}'
                                                        ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['feature_id'];                                                                                        
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <td><?php echo $rec['feature_name_en']?></td>
                                    <td><?php echo $rec['feature_name_ar']?></td>                                    
                                    <td>                                        
                                        <a href="<?php echo 'feature_form.php?type='.$type.'&id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="features" table-id="feature_id" >Disable</a>
                                    <?php } elseif($rec['status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="features" table-id="feature_id" >Enable</a>
                                    <?php }?>  
                                    <?php if($type != 5): ?>                                      
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="features" table-id="feature_id">Delete</a> 
                                    <?php endif;?>
                                    </td>                                        
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>

</html>