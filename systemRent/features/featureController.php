<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){				
		// GET POST DATA			
		$name_en   		= $_POST['name_en'];
		$name_ar   		= $_POST['name_ar'];
		$type 			= $_POST['type'];
			
		/***** ADD *****/
		if(isset($_POST['add'])){
			$add 	= $dbh->query("INSERT INTO `features` SET
										`feature_for`		='{$type}',
										`feature_name_en` 	='{$name_en}',
										`feature_name_ar` 	='{$name_ar}',										
										`status` 			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");			
			if($add){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Feature added successfuly';
			}

		} // add

		/***** UPDATE *****/
		elseif ($_POST['update']) {
			$id 	= $_POST['id'];			
			$update = $dbh->query("UPDATE `features` SET										
										`feature_name_en` 	='{$name_en}',
										`feature_name_ar` 	='{$name_ar}',										
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`feature_id`		='{$id}'
									");
			if($update){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Feature updated successfuly';
			}

		}//update		

		header('Location:index.php?type='.$type);

	} // IF ISSET POST
?>
