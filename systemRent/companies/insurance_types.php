<?php 
	require_once("../module/include_mod.php");


	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');
	
	if(! isset($_GET['company_id'])) {header('Location: index.php');}

	$company_id 	= $_GET['company_id'];	

	// COMPANY
	$comapny_query = $dbh->query("SELECT * FROM `companies` WHERE `company_id`='{$company_id}' ");

	if ($comapny_query->rowCount()>0) {
			$comapny 	= $comapny_query->fetch();


	$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];
	if($id > 0){
		$action = 'update';
		$bc 	= 'update type';
		$back 	= 'insurance_types.php?company_id='.$company_id;
		
		// TYPE
		$type_query 	= $dbh->query("SELECT * FROM `insurance_types` WHERE `type_id`='{$id}'");
		if ($type_query->rowCount()>0) {
			$type 		= $type_query->fetch();			
		}
	}else{
		$action = 'add';
		$bc 	= 'add type';
		$back 	= 'index.php?type=1';


	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	   
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                     <li><a href="index.php?type=1">companies</a></li>
		                     <li><a href=""><?php echo $comapny['company_name_en'];?></a></li>
		                	<li class="active">types</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="companyController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
											
											<!-- NAME EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Name</label>
												<div class="col-md-4">
													<input type="text" name="name_en" class="form-control required" value="<?php echo @$type['type_name_en'];?>" required>
												</div>
											</div>

											<!-- NAME AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Name</label>
												<div class="col-md-4">
													<input type="text" name="name_ar" class="form-control " value="<?php echo @$type['type_name_ar'];?>" >
												</div>
											</div>

											<!-- DESCRIPTION EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_en" class="form-control" rows="4" required><?php echo @$type['type_desc_en'];?></textarea></div>
			                                </div>

			                                <!-- DESCRIPTION AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_ar" class="form-control" rows="4" ><?php echo @$type['type_desc_ar'];;?></textarea></div>
			                                </div>

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                	<input type="hidden" name="company_id" value="<?php echo $company_id; ?>">
			                                    <div class="col-lg-11">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="insurance_type" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="<?php echo $back; ?>" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>

			                             <!-- OFFERS -->
			                            <?php if($id == 0){?>
			                            <table class="table table-striped">
			                                <thead>
			                                    <tr>
			                                        <th>#</th>
			                                        <th>Name EN</th>
			                                        <th>Name AR</th>			                                        
			                                        <th>Action</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                <?php
			        							$stmt = $dbh->query("SELECT * FROM `insurance_types` 
			        													WHERE status 	< 3 			        													
			        													AND `company_id`	='{$company_id}'			        													
		        													");
			        							$stmt->execute();
			        							if($stmt->rowCount() > 0) {
			                                        $ceq = 0;
			        							    while($rec = $stmt->fetch()) {
			                                            $type_id    = $rec['type_id'];
			                                ?>
			                                <tr>
			                                    <td><?php echo ++$ceq; ?></td>
			                                    <td><?php echo $rec['type_name_en'];?></td>
			                                    <td><?php echo $rec['type_name_ar'];?></td>			                                    		                                   
			                                    <td> 		                                        
			                                        <a href="insurance_types.php?company_id=<?php echo $company_id.'&id='.$type_id;?>" class="btn btn-warning">Edit</a>
			                                    <?php if($rec['status']==1) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $type_id ;?>" table="insurance_types" table-id="type_id" >Disable</a>
			                                    <?php } elseif($rec['status']==2) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $type_id ;?>" table="insurance_types" table-id="type_id" >Enable</a>
			                                    <?php }?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $type_id ;?>" table="insurance_types" table-id="type_id">Delete</a> 
			                                    </td>                                        
			                                </tr>
			                                <?php 
			                                        } // while
			    							    } // if
			                                ?>
			                                </tbody>
			                            </table>
			                            <?php } // if id == 0?>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{				
				name_en:"required",
				desc_en:"required",				
								
			}
		});

	});
</script>
<?php } ?>
</body>
</html>