<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){

		/***** ADD & UPDATE INSURANCE TYPES STRAT  *****/
		if(isset($_POST['insurance_type'])):			
			// GET POST DATA
			$company_id   	= $_POST['company_id'];
			$name_en   		= $_POST['name_en'];
			$name_ar   		= $_POST['name_ar'];
			$desc_en   		= $_POST['desc_en'];
			$desc_ar   		= $_POST['desc_ar'];			
			$id 			= $_POST['id'];	
			// ADD
			if($_POST['insurance_type'] == 'add'){
				$add 	= $dbh->query("INSERT INTO `insurance_types` SET 
										`company_id`		='{$company_id}',
										`type_name_en`		='{$name_en}',
										`type_name_ar`		='{$name_ar}',
										`type_desc_en`		='{$desc_en}',
										`type_desc_ar`		='{$desc_ar}',
										`status` 			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Insurance type added successfuly';
				}
			}

			// UPDATE
			else{
				$update = $dbh->query("UPDATE `insurance_types` SET 										
										`type_name_en`		='{$name_en}',
										`type_name_ar`		='{$name_ar}',
										`type_desc_en`		='{$desc_en}',
										`type_desc_ar`		='{$desc_ar}',										
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`type_id`			='{$id}'
									");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Insurance type updated successfuly';
				}
			}

			// REDIRECT
			header('Location:insurance_types.php?company_id='.$company_id);
		/***** ADD & UPDATE INSURANCE TYPES END  *****/




		/***** ADD & UPDATE COMPANY STRAT  *****/
		else:
			// GET POST DATA			
			$country_id   	= $_POST['country_id'];
			$name_en   		= $_POST['name_en'];
			$name_ar   		= $_POST['name_ar'];
			$desc_en   		= $_POST['desc_en'];
			$desc_ar   		= $_POST['desc_ar'];
			$type 			= $_POST['type'];
			$id 			= $_POST['id'];	
			//print_r($_POST);die();		
				
			/***** ADD *****/
			if(isset($_POST['add'])){			
				$add 	= $dbh->query("INSERT INTO `companies` SET
											`country_id`		='{$country_id}',
											`company_type`		='{$type}',
											`company_name_en` 	='{$name_en}',
											`company_name_ar` 	='{$name_ar}',
											`company_desc_en`	='{$desc_en}',
											`company_desc_ar`	='{$desc_ar}',
											`status` 			=1,
											`created_by`		='{$login_id}',
											`created_at`		='{$now}'
										");			
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Company added successfuly';
				}

			} // add

			/***** UPDATE *****/
			elseif ($_POST['update']) {		
				$update = $dbh->query("UPDATE `companies` SET										
											`country_id`		='{$country_id}',										
											`company_name_en` 	='{$name_en}',
											`company_name_ar` 	='{$name_ar}',
											`company_desc_en`	='{$desc_en}',
											`company_desc_ar`	='{$desc_ar}',
											`updated_by`		='{$login_id}',
											`updated_at`		='{$now}'
											WHERE 
											`company_id`		='{$id}'
										");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Company updated successfuly';
				}

			}//update		

			header('Location:index.php?type='.$type);
		/***** ADD & UPDATE COMPANY END  *****/
		endif;
	} // IF ISSET POST
?>
