<?php 
	require_once("../module/include_mod.php");

	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

		// GET COUNTRIES
		$countries 		= array();
		$country_query 	= $dbh->query("SELECT * FROM `country_city` WHERE `parent_id`=0 AND `status_flag`=1");
		if($country_query->rowCount()> 0){
			$countries 	= $country_query->fetchall();
		}

		$type   = (isset($_GET['type']) ? $_GET['type'] : 0);

		switch ($type) {
        case 1:
            $bc_type = 'insurance';
            break;          
        default:
            $bc_type = '';
            break;
    	}	

		$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

		if($id > 0){
			$action = 'update';			
			$bc 	= 'updade '.$bc_type.' company';
			
			// COMAPANY 
			$company_query 	= $dbh->query("SELECT * FROM `companies` WHERE `company_id`='{$id}'");
			if ($company_query->rowCount()>0) {
				$company 	= $company_query->fetch();		
			}

		}else{
			$action = 'add';
			$bc 	= 'add '.$bc_type.' company';


		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php?type=<?php echo $type;?>">companies</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="companyController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">											

			                            	<!-- COUNTRY -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Country</label>
												<div class="col-md-4">
													<select name="country_id" class="form-control">
														<option value="">Please select country</option>
														<?php foreach ($countries as $country):?>
														<option value="<?php echo $country['id_country'] ?>" <?php if(@$company['country_id'] == $country['id_country'] ) echo 'selected'; ?> ><?php echo $country['name_en']; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>

			                            	<!-- NAME EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Name</label>
												<div class="col-md-4">
													<input type="text" name="name_en" class="form-control required" value="<?php echo @$company['company_name_en'];?>" required>
												</div>
											</div>

											<!-- NAME AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Name</label>
												<div class="col-md-4">
													<input type="text" name="name_ar" class="form-control " value="<?php echo @$company['company_name_ar'];?>" >
												</div>
											</div>

											<!-- DESCRIPTION EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_en" class="form-control" rows="4" required><?php echo @$company['company_desc_en'];?></textarea></div>
			                                </div>

			                                <!-- DESCRIPTION AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_ar" class="form-control" rows="4" ><?php echo @$company['company_desc_ar'];;?></textarea></div>
			                                </div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                	<input type="hidden" name="type" value="<?php echo $type; ?>">
			                                    <div class="col-lg-11">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php?type=<?php echo $type?>" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				name_en:"required",
				country_id:"required",
				desc_en:"required",
			}
		});
	});
</script>
</body>
</html>