<?php 
    require_once("../module/include_mod.php");
    if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

    $type   = (isset($_GET['type']) ? $_GET['type'] : 0);

    switch ($type) {
        case 1:
            $bc_type = 'insurance companies';
            break;          
        default:
            $bc_type = 'companies';
            break;
    }
?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active"><?php echo $bc_type;?></li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="company_form.php?type=<?php echo $type;?>" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Country</th>
                                        <th>Name En</th>
                                        <th>Name Ar</th> 
                                        <?php if($type == 1):?>
                                        <th>Insurance types</th> 
                                        <?php endif;?>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
        							$stmt = $dbh->query("SELECT * FROM `companies` a
                                                            INNER JOIN `country_city` b ON(b.id_country=a.country_id) 
                                                            WHERE a.status      < 3 
                                                            AND a.company_type  ='{$type}'
                                                        ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['company_id'];                                                                                        
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <td><?php echo $rec['name_en']?></td>
                                    <td><?php echo $rec['company_name_en']?></td>
                                    <td><?php echo $rec['company_name_ar']?></td>
                                    <?php if($type == 1):?>
                                    <td><a href="insurance_types.php?company_id=<?php echo $id; ?>" class="btn btn-success"> Add/ View</a></td> 
                                    <?php endif;?>                                  
                                    <td>                                        
                                        <a href="<?php echo 'company_form.php?type='.$type.'&id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="companies" table-id="company_id" >Disable</a>
                                    <?php } elseif($rec['status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="companies" table-id="company_id" >Enable</a>
                                    <?php }?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="companies" table-id="company_id">Delete</a> 
                                    </td>                                        
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>

</html>