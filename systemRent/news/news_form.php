<?php 
	require_once("../module/include_mod.php");
	$path 	= "../../images/news/";

	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

		$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

		if($id > 0){
			$action = 'update';			
			$bc 	= 'updade news';
			
			// NEWS 
			$news_query 	= $dbh->query("SELECT * FROM `news` WHERE `news_id`='{$id}'");
			if ($news_query->rowCount()>0) {
				$news 	= $news_query->fetch();		
			}

		}else{
			$action = 'add';
			$bc 	= 'add news';


		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                    <li><a href="index.php">news</a></li>		                  
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="newsController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">														                            

			                            	<!-- TITLE EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Title</label>
												<div class="col-md-4">
													<input type="text" name="title_en" class="form-control required" value="<?php echo @$news['news_title_en'];?>" required>
												</div>
											</div>

											<!-- TITLE AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Title</label>
												<div class="col-md-4">
													<input type="text" name="title_ar" class="form-control " value="<?php echo @$news['news_title_ar'];?>" >
												</div>
											</div>

											<!-- DATE STRAT -->
											<div class="form-group">
				                            	<label class="col-lg-2 control-label">Date</label>
				                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date=""  class="col-md-4 input-append date dpYears">
				                                    <input type="text" name="date" readonly="" value="<?php echo @$news['news_date'];?>" size="16" class="form-control">
				                                    <span class="input-group-btn add-on">
				                                        <button class="btn btn-primary" type="button" style="margin-left:-30px;height:34px"><i class="fa fa-calendar"></i></button>
				                                    </span>
				                                </div>
				                            </div>											

											 <!-- IMAGE -->
		                                    <div class="form-group">
		                                      	<label class="col-lg-2 control-label">Image</label>
		                                      	<div class="col-lg-6">
			                                        <input type="hidden" name="Hfile" value="<?php echo @$news['news_image'];?>">
			                                        <?php if((trim(@$news['news_image'])) and (file_exists($path.@$news['news_image']))) {?>
			                                            <a href="<?php echo $path.@$news['news_image'];?>" target="_new" style="text-decoration:underline">Show Image</a>
			                                            <div class="clearfix"></div>
			                                            <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel"></div>
			                                            <div style="width:90% !important;float:left" align="left">delete file.</div>
			                                            <d="clearfix"><iv class/div>
			                                        <?php }?>
			                                        <input type="file" name="file_upload">
		                                      	</div>
		                                    </div>

		                                    <!-- BRIEF EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Brief</label>
			                                    <div class="col-md-9"><textarea name="brief_en" class="form-control" rows="4" required><?php echo @$news['news_desc_en'];?></textarea></div>
			                                </div>

			                                <!-- BRIEF AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Brief</label>
			                                    <div class="col-md-9"><textarea name="brief_ar" class="form-control" rows="4" ><?php echo @$news['news_desc_ar'];;?></textarea></div>
			                                </div>

											<!-- DESCRIPTION EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_en" class="form-control" rows="4" required><?php echo @$news['news_desc_en'];?></textarea></div>
			                                </div>

			                                <!-- DESCRIPTION AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Desc</label>
			                                    <div class="col-md-9"><textarea name="desc_ar" class="form-control" rows="4" ><?php echo @$news['news_desc_ar'];;?></textarea></div>
			                                </div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">			                                	
			                                    <div class="col-lg-11">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script src="../js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../js/advanced-form.js"></script>    
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				title_en:"required",
				brief_en:"required"
				desc_en:"required"
			}
		});
	});
</script>
</body>
</html>