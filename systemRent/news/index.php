<?php 
    ob_start();
    require_once("../module/include_mod.php");
    if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <li class="active">news</li>              
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="news_form.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title EN</th>
                                        <th>Title AR</th>
                                        <th>Date</th>                                         
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
        							$stmt = $dbh->query("SELECT * FROM `news`                                                             
                                                            WHERE status < 3 
                                                            
                                                        ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['news_id'];                                                                                        
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <td><?php echo $rec['news_title_en']?></td>
                                    <td><?php echo $rec['news_title_ar']?></td>
                                    <td><?php echo $rec['news_date']?></td> 
                                    <td>                                                                       
                                        <a href="<?php echo 'news_form.php?id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="news" table-id="news_id" >Disable</a>
                                    <?php } elseif($rec['status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="news" table-id="news_id" >Enable</a>
                                    <?php }?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="news" table-id="news_id">Delete</a> 
                                    </td>                                        
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>

</html>