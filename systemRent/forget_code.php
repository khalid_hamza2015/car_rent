<?php
define('INCLUDE_CHECK',true);
require_once('../function.php');
$error = true;
$email = (! empty($_POST['email'])?$_POST['email']:NULL);
if(! is_null($email)) {
	$stmtShop = $dbh->prepare("SELECT * FROM `shop` WHERE `email` = :uname AND `status_flag`= 1;");
	$stmtShop->bindParam(':uname',$email, PDO::PARAM_STR);
	$stmtShop->execute();
	if($stmtShop->rowCount() > 0) {
		$error = false;
		$row = $stmtShop->fetch();
		$from = "Dayout <info@dayoutkw.com>";
		$sub  = "Dayout | Password Recovery";
		$msg  = "Dear ".$row['name_en']." ,<br />Your password is ".$row['password'];
		$headers = "content-type: text/html"."\r\n"."from: $from";
		mail($email, $sub, $msg, $headers);	
		header('Location: index.php?error_forget=2');
	}
} 
if($error) {
	header('Location: index.php?error_forget=1');
}?>
