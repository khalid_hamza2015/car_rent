<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<?php
$parent_id=0;
if(isset($_REQUEST['parent_id']))
	$parent_id = $_REQUEST['parent_id'];

$dept_name='';
$stmtParent = $dbh->prepare("SELECT * FROM `country_city` WHERE `id_country` = {$parent_id};");
$stmtParent->execute();
if($stmtParent->rowCount() > 0) {
	$recPar    = $stmtParent->fetch();	
	$dept_name = $recPar['name_en'];
}?>
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                <li><a href="index.php"><?php echo $dept_name?></a></li>                
                <li class="active">City</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body">
                    <div class="adv-table editable-table ">
					   <div class="clearfix" style="padding-bottom:10px"><a class="btn btn-primary pull-right" href="form_city.php?<?php echo "parent_id={$parent_id}";?>">Add New <i class="fa fa-plus"></i></a></div>
                        <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="space15"></div><?php
						$stmt = $dbh->prepare("SELECT * FROM `country_city` WHERE `parent_id` = {$parent_id} AND `status_flag` < 3 ORDER BY `sort_id`;");
						$stmt->execute();
						if($stmt->rowCount() > 0) {?>
                        <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>English Name</th>
                                <th>Arabic Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php while($rec = $stmt->fetch()) {?>
                        <tr>
                            <td><?php echo $rec['name_en'];?></td>
                            <td><?php echo $rec['name_ar'];?></td>
                            <td>
                            <a class="btn btn-warning" href="form_city.php?id=<?php echo $rec['id_country']."&parent_id={$parent_id}";?>">Edit</a>
                            <?php if($rec['status_flag']==0) {?>
                                <a href="status.php?id=<?php echo $rec['id_country']. "&table=areas&parent_id={$parent_id}"?>&status_flag=1" class="btn btn-danger">Make Enable</a>
                            <?php } elseif($rec['status_flag']==1) { ?>
                                <a href="status.php?id=<?php echo $rec['id_country']."&table=areas&parent_id={$parent_id}"?>&status_flag=0" class="btn btn-primary">Make Disable</a>
                            <?php }?> 
                            </td>
                        </tr>
                        <?php }?>
                        </tbody>
                        </table>
                        <?php }?>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start--><div class="right-sidebar"><?php include("../module/right_mod.php");?></div><!--right sidebar end-->
</section>
<?php include("../module/footer_mod.php");?>
</body>
</html>