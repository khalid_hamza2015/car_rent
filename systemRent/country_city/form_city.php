<?php require_once("../module/include_mod.php");?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container"><?php 
include("../module/header_mod.php"); include("../module/left_menu_mod.php");
$parent_id=0;
if(isset($_REQUEST['parent_id']))
	$parent_id = $_REQUEST['parent_id'];

$dept_name='';
$stmtParent = $dbh->prepare("SELECT * FROM `country_city` WHERE `id_country` = {$parent_id};");
$stmtParent->execute();
if($stmtParent->rowCount() > 0) {
	$recPar    = $stmtParent->fetch();	
	$dept_name = $recPar['name_en'];
}?>
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="index.php">Country / City</a></li>                
                    <li><a href="index.php"><?php echo $dept_name?></a></li>                
                    <li class="active">City</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <div class="clearfix" style="padding-bottom:10px">
                                <div class="btn-group"><a class="btn btn-primary" href="city.php?parent_id=<?php echo $parent_id?>">Back <i class="fa fa-backward"></i></a></div>
                            </div>
                            <div class="space15"></div><?php 
							$id=0; if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
							$name_en = "";
							$name_ar = "";
							$stmt = $dbh->prepare("SELECT * FROM `country_city` WHERE `id_country` = {$id};");
							$stmt->execute();
							if($stmt->rowCount() > 0) {
								$rec     = $stmt->fetch();
								$name_en = $rec['name_en'];
								$name_ar = $rec['name_ar'];
							}?>
                            <form action="code_city.php?<?php echo "id={$id}&parent_id={$parent_id}"?>" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">English Name</label>
                                    <div class="col-lg-6">
                                          <input type="text" name="name_en" class="form-control" value="<?php echo $name_en?>" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Arabic Name</label>
                                    <div class="col-lg-6">
                                          <input type="text" name="name_ar" class="form-control" value="<?php echo $name_ar?>" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>
</html>