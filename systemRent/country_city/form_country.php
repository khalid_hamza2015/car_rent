<?php require_once("../module/include_mod.php");?>
</head>
<body>
<section id="container">
<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="index.php">Country / City</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body"><?php 
					$id=0; if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
					$name_en = "";
					$name_ar = "";
					$stmt = $dbh->prepare("SELECT * FROM `country_city` WHERE `id_country` = {$id};");
					$stmt->execute();
					if($stmt->rowCount() > 0) {
						$rec     = $stmt->fetch();
						$name_en = $rec['name_en'];
						$name_ar = $rec['name_ar'];
					}?>
                        <form action="code_country.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <input type="hidden" name="id_country" value="<?php echo $id;?>">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">English Name</label>
                                <div class="col-lg-6">
                                      <input type="text" id="txt_EngName" name="txt_EngName" class="form-control" value="<?php echo $name_en?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Arabic Name</label>
                                <div class="col-lg-6">
                                      <input type="text" id="txt_arabName" name="txt_arabName" class="form-control" value="<?php echo $name_ar?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9"><button class="btn btn-primary" type="submit">Submit</button></div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>
</html>