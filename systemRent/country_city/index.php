<?php require_once("../module/include_mod.php");?>
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?>
<?php include("../module/left_menu_mod.php");?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active">Country / City</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="form_country.php" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                        <?php if((isset($_REQUEST['done'])) and ($_REQUEST['done']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully saved.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <?php if((isset($_REQUEST['del'])) and ($_REQUEST['del']==1)) {?>
                        <div class="alert alert-success ">
                            <span class="alert-icon"><i class="fa fa-check"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Your record has been successfully deleted.</li>
                                    <li class="pull-right notification-time"></li>
                                </ul>
                                <p><br></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="space15"></div><?php
						$stmt = $dbh->prepare("SELECT * FROM `country_city` WHERE `parent_id` = 0 AND `status_flag` < 3 ORDER BY `sort_id`;");
						$stmt->execute();
						if($stmt->rowCount() > 0) {?>
                        <table class="table table-bordered table-advance">
                            <thead>
                                <tr>
                                    <th>English Name</th>
                                    <th>Arabic Name</th>
                                    <th>City</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php while($rec = $stmt->fetch()) { $id_country = $rec['id_country'];?>
                                <tr>
                                    <td><?php echo $rec['name_en'];?></td>
                                    <td><?php echo $rec['name_ar'];?></td>
                                    <td><a href="city.php?parent_id=<?php echo $id_country?>">Manage City</a></td>
                                    <td>
                                        <a href="form_country.php?id=<?php echo $id_country?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                        <?php if($rec['status_flag']==2) {?>
                                        <a href="status.php?id=<?php echo $id_country?>&status_flag=1" class="btn btn-danger">Make Enable</a>
                                        <?php } elseif($rec['status_flag']==1) { ?>
                                        <a href="status.php?id=<?php echo $id_country?>&status_flag=2" class="btn btn-primary">Make Disable</a>
                                        <?php }?> 
                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                        <?php }?>
                    </div>
                </section>
            </div>
        </div>
    </section>
 </section>
 <div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>                  
<?php include("../module/footer_mod.php");?>
</body>
</html>