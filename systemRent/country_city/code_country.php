<?php require_once("../module/include_mod.php");?>
</head>
<body><?php
$id =0;
if(isset($_REQUEST['id']))
	 $id = $_REQUEST['id'];

$txt_EngName = "";
if(isset($_REQUEST['txt_EngName']))
	$txt_EngName = $_REQUEST['txt_EngName'];

$txt_arabName = "";
if(isset($_REQUEST['txt_arabName']))
	$txt_arabName = $_REQUEST['txt_arabName'];

$txt_order = 0;
if(isset($_REQUEST['txt_order']))
	$txt_order = $_REQUEST['txt_order'];

if($id==0) {
	$stmt = $dbh->prepare(" INSERT INTO `country_city` SET 
											`name_en`     = :name_en , 
											`name_ar`     = :name_ar , 
											`parent_id`   = 0 , 
											`sort_id`     = :sort_id , 
											`status_flag` = 1;");
	$stmt->bindParam(':name_en', $txt_EngName);
	$stmt->bindParam(':name_ar', $txt_arabName);
	$stmt->bindParam(':sort_id', $txt_order);
	$stmt->execute();
} elseif($id>0) {
	$stmt = $dbh->prepare(" UPDATE `country_city` SET
										`name_en` = :name_en , 
										`name_ar` = :name_ar
							WHERE `id_country` = :id ");
	$stmt->bindParam(':name_en', $txt_EngName);
	$stmt->bindParam(':name_ar', $txt_arabName);
	$stmt->bindParam(':id',$id);
	$stmt->execute();
}?>
</body>
</html>
<script language="javascript">document.location='index.php?done=1';</script>