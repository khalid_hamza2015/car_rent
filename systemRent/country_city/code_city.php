<?php require_once("../module/include_mod.php");?>
</head>
<body><?php 
$txt_EngName = "";
if(isset($_POST['name_en']))
	$txt_EngName = $_POST['name_en'];

$txt_arabName = "";
if(isset($_POST['name_ar']))
	$txt_arabName = $_POST['name_ar'];

$parent_id =0;
if(isset($_REQUEST['parent_id']))
   $parent_id = $_REQUEST['parent_id'];

$id=0; if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
//------------------------------------------------------------------------------------
if(trim($txt_EngName)<>'') {
	if($id==0) {
		$stmt = $dbh->prepare(" INSERT INTO `country_city` SET 
												`name_en`     = :name_en , 
												`name_ar`     = :name_ar , 
												`parent_id`   = :parent_id , 
												`sort_id`     = :sort_id , 
												`status_flag` = 1;");
		$stmt->bindParam(':name_en', $txt_EngName);
		$stmt->bindParam(':name_ar', $txt_arabName);
		$stmt->bindParam(':parent_id', $parent_id);
		$stmt->bindParam(':sort_id', $txt_order);
		$stmt->execute();
	} elseif($id>0) {
		$stmt = $dbh->prepare(" UPDATE `country_city` SET
											`name_en` = :name_en , 
											`name_ar` = :name_ar
								WHERE `id_country` = :id ");
		$stmt->bindParam(':name_en', $txt_EngName);
		$stmt->bindParam(':name_ar', $txt_arabName);
		$stmt->bindParam(':id',$id);
		$stmt->execute();
	}
}?>
<script language="javascript" type="text/javascript">document.location='city.php?done=1&parent_id=<?php echo $parent_id?>'</script>
</body>
</html>