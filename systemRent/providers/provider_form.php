<?php 
	require_once("../module/include_mod.php");
	$path 	= "../../images/providers/";
	$lat 	= '29.311660';
	$long 	= '47.481766';

	$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

	if(($_SESSION['flag_type'] == 2 && $id != $login_id) || $_SESSION['flag_type'] == 3 ){
		header('Location:../main/main.php');
	}
	$provider_features = array();
	
	// COUNTRIES
	$country_query 	= $dbh->query("SELECT * FROM `country_city` WHERE `status_flag`=1 AND `parent_id`=0");
	if($country_query->rowCount()>0){
		$countries 	= $country_query->fetchall();		
	}	
	
	// FEATURES
	$features_query 	= $dbh->query("SELECT * FROM `features` WHERE `status`=1 AND `feature_for`=1");
	if($features_query->rowCount()>0){
		$features 		= $features_query->fetchall();		
	}	


	if($id > 0){
		$action = 'update';
		$bc 	= 'Update Provider';
		
		// PROVIDER 
		$provider_query = $dbh->query("SELECT * FROM `providers` 
											WHERE `provider_id`='{$id}' 
											AND `status`< 3
										");
		if ($provider_query->rowCount()>0) {
			$provider 	= $provider_query->fetch();
			$lat 		= $provider['provider_lat'];
			$long 		= $provider['provider_long'];
			$provider_c = $provider['country_id'];
			$provider_features 	= explode(',', $provider['provider_features']);

			// AREEAS
			$area_query 	= $dbh->query("SELECT * FROM `country_city` WHERE `status_flag`=1 AND `parent_id`='{$provider_c }'");
			if($area_query->rowCount()>0){
				$areas 		= $area_query->fetchall();		
			}
		}
	}else{
		$action = 'add';
		$bc 	= 'Add Provider';


	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	   
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                     <li><a href="../providers/index.php">Vendor Management</a></li>
		                	<li class="active"><?php echo $bc;?></li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="providerController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
												
											<!-- EMIAL -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Email</label>
												<div class="col-md-4">
													<input type="email" name="email" class="form-control" value="<?php echo @$provider['provider_email'];?>" required>
												</div>
											</div>   

											<!-- USERNAME  -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Username</label>
												<div class="col-md-4">
													<input type="text" name="username" class="form-control" value="<?php echo @$provider['provider_username'];?>" required>
												</div>
											</div>

											<!-- PASSWORD  -->
											<?php if($id == 0){?>
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Password</label>
												<div class="col-md-4">
													<input type="password" name="password" class="form-control" value="" required>
												</div>
											</div>
											<?php } ?>
											<hr>
											
											<!-- RESPONIABLE NAME -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Responsible Name</label>
												<div class="col-md-4">
													<input type="text" name="res_name" class="form-control" value="<?php echo @$provider['provider_responsible_name'];?>" />
												</div>
											</div>

											<!-- RESPOSIBLE MOILE -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Responsible Mobile</label>
												<div class="col-md-4">
													<input type="text" name="res_mobile" class="form-control" value="<?php echo @$provider['provider_responsible_mobile'];?>" />
												</div>
											</div>
											<hr>

											<!-- TYPE -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Type</label>
												<div class="col-md-4">
													<select name="type" class="form-control" required>
														<option value="">Please Select Type</option>
														<option value="1" <?php if(@$provider['provider_type'] == 1) echo 'selected'; ?> > Car Rent</option>
														<option value="2" <?php if(@$provider['provider_type'] == 2) echo 'selected'; ?> > Traveling Office</option>
														<option value="3" <?php if(@$provider['provider_type'] == 3) echo 'selected'; ?> > Sales Representative</option>												
													</select>													
												</div>
											</div>
											
			                            	
			                            	<!-- NAME EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Name</label>
												<div class="col-md-4">
													<input type="text" name="name_en" class="form-control required" value="<?php echo @$provider['provider_name_en'];?>" required>
												</div>
											</div>

											<!-- NAME AR -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Arabic Name</label>
												<div class="col-md-4">
													<input type="text" name="name_ar" class="form-control" value="<?php echo @$provider['provider_name_ar'];?>" required>
												</div>
											</div>										

											<!-- ADDRESS & MAP -->
											<div class="form-group">																																											
												<div class="col-md-6">
													<!-- COUNTRY -->
													<div class="form-group">
														<label class="col-lg-4 control-label">Country</label>
														<div class="col-md-8">
															<select name="country" class="form-control" required>
																<option value="">Please Select Country</option>
																<?php 
																	if(isset($countries)){																
																		foreach ($countries as $country) {
																?>
																<option value="<?php echo $country['id_country'];?>" <?php if(@$provider['country_id'] == $country['id_country']) echo 'selected'; ?> > <?php echo $country['name_en']?></option>
																<?php					
																		} // foreach countries
																	} // if isset countries
																?>
															</select>													
														</div>
													</div>
													<!-- AREA -->
													<div class="form-group">
														<label class="col-lg-4 control-label">Area</label>
														<div class="col-md-8">
															<select name="area" class="form-control" required>
																<option value="">Please Select Area</option>												
																<?php 
																	if($id >0 && isset($areas)){																
																		foreach ($areas as $area) {
																?>
																<option value="<?php echo $area['id_country'];?>" <?php if(@$provider['area_id'] == $area['id_country']) echo 'selected'; ?> > <?php echo $area['name_en']?></option>
																<?php					
																		} // foreach countries
																	} // if isset countries
																?>
															</select>													
														</div>
													</div>	
													<!-- ADDRESS -->
													<div class="form-group">
														<label class="col-lg-4 control-label">Address</label>
														<div class="col-md-8">
															<input type="text" name="address" class="form-control" value="<?php echo @$provider['provider_address'];?>" />
														</div>
													</div>
				                                    <!-- LATITIDE -->
				                                    <div class="form-group">
			                                            <label class="col-lg-4 control-label">Latitude</label>
			                                            <div class="col-lg-8">                                                
			                                                <input type="text" name="lat" class="form-control" id="latitude" value="<?php echo @$lat;?>" >
			                                            </div>
			                                        </div>
			                                        <!-- LONGITUDE -->
			                                        <div class="form-group">
			                                            <label class="col-lg-4 control-label">Longitude</label>
			                                            <div class="col-lg-8">
			                                                <input type="text" name="long" class="form-control" id="longitude"  value="<?php echo $long;?>" />
			                                            </div>
			                                        </div>											
												</div>
												<!-- MAP -->
												<div class="col-md-5">
													<div id="somecomponent" style="height:230px"></div> 
												</div>
											</div>

											<!-- IMPORTANT -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Important Level</label>
												<div class="col-md-4">
													<select name="imporatnt" class="form-control" required>
														<option value="">Please Select important level</option>
														<option value="1" <?php if(@$provider['provider_important'] == 1) echo 'selected'; ?> > High</option>
														<option value="2" <?php if(@$provider['provider_important'] == 2) echo 'selected'; ?> > Average</option>
														<option value="3" <?php if(@$provider['provider_important'] == 3) echo 'selected'; ?> > Low</option>												
													</select>													
												</div>
											</div>	

											<!-- CONTRACT NUMBER -->
	                                        <div class="form-group">
	                                            <label class="col-lg-2 control-label">Contract Number</label>
	                                            <div class="col-lg-4">
	                                                <input type="text" name="contract_number" class="form-control" id="longitude"  value="<?php echo @$provider['provider_contract_number'];?>" />
	                                            </div>
	                                        </div>

	                                        <!-- FEATURES -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Features</label>
												<div class="col-md-9">
													<?php foreach ($features as $feature):?>
													<label class="checkbox-inline">
														<input name="feature[]" type="checkbox" value="<?php echo $feature['feature_id']?>" <?php if(in_array($feature['feature_id'], $provider_features)) echo 'checked';?>>
														<?php echo $feature['feature_name_en'];?>
													</label>
													<?php endforeach; ?>													
												</div>
											</div>

											<!-- MCAN RESERVE -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Can reserve from others</label>
												<div class="col-md-9">													
													<label class="checkbox-inline">
														<input name="can_reserve" type="checkbox" value="1" <?php if(@$provider['can_reserve'] == 1) echo 'checked';?>>														
													</label>																									
												</div>
											</div>								
											
		                                    <!-- LOGO -->
		                                    <div class="form-group">
		                                      	<label class="col-lg-2 control-label">Logo</label>
		                                      	<div class="col-lg-6">
			                                        <input type="hidden" name="Hfile" value="<?php echo @$provider['provider_logo'];?>">
			                                        <?php if((trim(@$provider['provider_logo'])) and (file_exists("../../images/providers/".@$provider['provider_logo']))) {?>
			                                            <a href="<?php echo $path.@$provider['provider_logo'];?>" target="_new" style="text-decoration:underline">Show Image</a>
			                                            <div class="clearfix"></div>
			                                            <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel"></div>
			                                            <div style="width:90% !important;float:left" align="left">delete file.</div>
			                                            <d="clearfix"><iv class/div>
			                                        <?php }?>
			                                        <input type="file" name="file_upload">
		                                      	</div>
		                                    </div>

		                                    <!-- BANNER -->
		                                    <div class="form-group">
		                                      	<label class="col-lg-2 control-label">Banner</label>
		                                      	<div class="col-lg-6">
			                                        <input type="hidden" name="Bfile" value="<?php echo @$provider['provider_banner'];?>">
			                                        <?php if((trim(@$provider['provider_banner'])) and (file_exists($path.@$provider['provider_banner']))) {?>
			                                            <a href="<?php echo $path.@$provider['provider_banner'];?>" target="_new" style="text-decoration:underline">Show Image</a>
			                                            <div class="clearfix"></div>
			                                            <div style="width:10% !important;float:left" align="left"><input type="checkbox" name="chkdel_b"></div>
			                                            <div style="width:90% !important;float:left" align="left">delete file.</div>
			                                            <div class="clearfix"></div>
			                                        <?php }?>
			                                        <input type="file" name="banner">
		                                      	</div>
		                                    </div>
		                                    
		                                    <!-- Notes EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Notes</label>
			                                    <div class="col-md-9"><textarea name="breif_en" class="form-control" rows="4" required><?php echo @$provider['provider_brief_en'];?></textarea></div>
			                                </div>

			                                <!-- Notes AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Notes</label>
			                                    <div class="col-md-9"><textarea name="breif_ar" class="form-control" rows="4" required><?php echo @$provider['provider_brief_ar'];?></textarea></div>
			                                </div>

		                                    <!-- DESCRIPTION EN  -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">English Desc</label>
			                                    <div class="col-md-9"><textarea name="description_en" class="form-control" rows="4" required><?php echo @$provider['provider_description_en'];?></textarea></div>
			                                </div>

			                                <!-- DESCRIPTION AR -->
			                                <div class="form-group">
			                                    <label class="col-lg-2 control-label">Arabic Desc</label>
			                                    <div class="col-md-9"><textarea name="description_ar" class="form-control" rows="4" required><?php echo @$provider['provider_description_ar'];;?></textarea></div>
			                                </div>

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                    <div class="col-lg-9">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="provider" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?libraries=places'></script>
<script src="../js/locationpicker.jquery.js"></script>
<script>
    $('#somecomponent').locationpicker({
        location: {
            latitude: $('#latitude').val(),
            longitude: $('#longitude').val()
        },            
        radius: 0,
        zoom: 8,
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            $('#latitude').val(currentLocation.latitude);
            $('#longitude').val(currentLocation.longitude);            
        }
    });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				username:"required",
				email:{required:true,email:true},
				password:"required",
				res_name:"required",
				res_mobile:"required",
				name_en:"required",
				name_ar:"required",				
				tele:{required:true,number:true}				
			}
		});

		// GET AREAS
		$('body').on('change','select[name=country]',function(){
			var country 	= $(this).val();
			var action 		= $('form').attr('action');
			$('select[name=area]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_areas':1,'country':country},
				success:function(response){
					$('select[name=area]').append(response);
				}
			});
			return false;
		});
	});
</script>
</body>
</html>