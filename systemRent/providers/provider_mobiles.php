<?php 
	require_once("../module/include_mod.php");

	$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

	if(! isset($_GET['provider_id'])) {header('Location: index.php');}

	$provider_id 	= $_GET['provider_id'];

	if(($_SESSION['flag_type'] == 2 && $id != $login_id) || $_SESSION['flag_type'] == 3 ){
		header('Location:../main/main.php');
	}

	// PROVIDER
	$provider_query = $dbh->query("SELECT * FROM `providers` WHERE `provider_id`='{$provider_id}' ");

	if ($provider_query->rowCount()>0) {
			$provider 	= $provider_query->fetch();

	if($id > 0){
		$action = 'update';
		$bc 	= 'Update Mobile';
		$back 	= 'provider_mobiles.php?provider_id='.$provider_id;
		
		// MOBILE 
		$mobile_query = $dbh->query("SELECT * FROM `provider_mobiles` WHERE `mobile_id`='{$id}'");
		if ($mobile_query->rowCount()>0) {
			$mobile 	= $mobile_query->fetch();			
		}
	}else{
		$action = 'add';
		$bc 	= 'Add Mobile';
		$back 	= 'index.php';


	}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />	   
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>
		                     <li><a href="index.php">Vendor Management</a></li>
		                     <li><a href=""><?php echo $provider['provider_name_en'];?></a></li>
		                	<li class="active">Mobiles</li>
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="providerController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
											
											<!-- MOBILE -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Type</label>
												<div class="col-md-4">
													<select name="type" class="form-control">
														<option value="">Please select type</option>		
														<option value="1" <?php  if(@$mobile['mobile_type'] == 1) echo 'selected'; ?> > Mobile</option>		
														<option value="2" <?php  if(@$mobile['mobile_type'] == 2) echo 'selected'; ?> >Telephone</option>
													</select>													
												</div>
											</div>

											<!-- TELEPHONE -->
		                                    <div class="form-group">
												<label class="col-lg-2 control-label">Number</label>
												<div class="col-md-4">
													<input type="text" name="number" class="form-control" value="<?php echo @$mobile['mobile_number'];?>" />
												</div>
											</div>

			                                <div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                	<input type="hidden" name="provider_id" value="<?php echo $provider_id; ?>">
			                                    <div class="col-lg-4">
			                                    	<button class="btn btn-primary pull-right" type="submit" name="provider_mobile" value="<?php echo $action; ?>">Submit</button>
			                                    	<a href="<?php echo $back; ?>" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>

			                             <!-- OFFERS -->
			                            <?php if($id == 0){?>
			                            <table class="table table-striped">
			                                <thead>
			                                    <tr>
			                                        <th>#</th>
			                                        <th>Mobile</th>
			                                        <th>Telephone</th>			                                        
			                                        <th>Action</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                <?php
			        							$stmt = $dbh->query("SELECT * FROM `provider_mobiles` 
			        													WHERE status 	< 3 			        													
			        													AND `provider_id`	='{$provider_id}'			        													
		        													");
			        							$stmt->execute();
			        							if($stmt->rowCount() > 0) {
			                                        $ceq = 0;
			        							    while($rec = $stmt->fetch()) {
			                                            $mobile_id    = $rec['mobile_id'];
			                                ?>
			                                <tr>
			                                    <td><?php echo ++$ceq; ?></td>
			                                    <td><?php echo ($rec['mobile_type'] == 1 ? 'Mobile' : 'Telephone');?></td>
			                                    <td><?php echo $rec['mobile_number'];?></td>			                                    		                                   
			                                    <td> 		                                        
			                                        <a href="provider_mobiles.php?provider_id=<?php echo $provider_id.'&id='.$mobile_id;?>" class="btn btn-warning">Edit</a>
			                                    <?php if($rec['status']==1) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $mobile_id ;?>" table="provider_mobiles" table-id="mobile_id" >Disable</a>
			                                    <?php } elseif($rec['status']==2) {?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $mobile_id ;?>" table="provider_mobiles" table-id="mobile_id" >Enable</a>
			                                    <?php }?>                                        
			                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $mobile_id ;?>" table="provider_mobiles" table-id="mobile_id">Delete</a> 
			                                    </td>                                        
			                                </tr>
			                                <?php 
			                                        } // while
			    							    } // if
			                                ?>
			                                </tbody>
			                            </table>
			                            <?php } // if id == 0?>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				username:"required",
				email:{required:true,email:true},
				password:"required",
				res_name:"required",
				res_mobile:"required",
				name_en:"required",
				name_ar:"required",				
				tele:{required:true,number:true}				
			}
		});

		// GET AREAS
		$('body').on('change','select[name=country]',function(){
			var country 	= $(this).val();
			var action 		= $('form').attr('action');
			$('select[name=area]').empty();
			$.ajax({
				url:action,
				type:'post',
				data:{'get_areas':1,'country':country},
				success:function(response){
					$('select[name=area]').append(response);
				}
			});
			return false;
		});
	});
</script>
<?php } ?>
</body>
</html>