<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	require_once('../car_rent_permisssion.php');
	$permissions= implode(',', $car_rent_permisssion) ;
	$now 		= date('Y-m-d h:i:s');
	$uploaddir  = '../../images/providers/'; 
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();



	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';

	if(isset($_POST)){
		
		/***** GET COUNTRY AREAS  STRAT *****/
		if(isset($_POST['get_areas'])):
			$country_id 	= (isset($_POST['country']) ? $_POST['country'] : 0);
			$result 		= "<option value=''>Please select area</option>";
			$area_query 	= $dbh->query("SELECT * FROM `country_city` WHERE `parent_id`='{$country_id}'");

			if($area_query->rowCount() > 0){
				while ($rec = $area_query->fetch()) {
					$result .= "<option value='".$rec['id_country']."'>".$rec['name_en']."</option>";
				}
			}
			echo $result;

		/***** GET COUNTRY AREAS END *****



		/***** CHANGE PASSWORD  STRAT *****/
		elseif (isset($_POST['change_password'])):
			$id  		= $_POST['id'];
			$password 	= base64_encode($_POST['password']);
			$change 	= $dbh->query(" UPDATE `providers` 
											SET 
												`provider_password`='{$password}' 
											WHERE 
												`provider_id`='{$id}'
										");
			if($change){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='Password Changed Successfuly';
			}															
			header("Location:index.php");		
		/***** CHANGE PASSWORD  STRAT *****/


		/***** ADD & UPDATE PROVIDER  STRAT *****/
		elseif(isset($_POST['provider'])):			
			// GET POST DATA					
			$email 			= $_POST['email'];
			$username 		= $_POST['username'];
			$password 		= base64_encode($_POST['password']);
			$res_name 		= $_POST['res_name'];
			$res_mobile 	= $_POST['res_mobile'];
			$type 			= $_POST['type'];
			$name_en   		= $_POST['name_en'];
			$name_ar   		= $_POST['name_ar'];			
			$country 		= $_POST['country'];
			$area 			= $_POST['area'];
			$address 		= $_POST['address'];
			$lat   	   	 	= $_POST['lat'];
			$long   	   	= $_POST['long'];
			$contract_number= $_POST['contract_number'];
			$imporatnt		= $_POST['imporatnt'];
			$breif_en   	= $_POST['breif_en'];
			$breif_ar   	= $_POST['breif_ar'];
			$description_en = $_POST['description_en'];		
			$description_ar = $_POST['description_ar'];
			$id 			= $_POST['id'];	

			$feature 		= (isset($_POST['feature']) ? $_POST['feature'] : array() );
			$can_reserve 	= (isset($_POST['can_reserve']) ? $_POST['can_reserve'] : 0 );
			$feature 		= implode(',', $feature);		

			/* LOGO */			
			$Hfile 		='';
			if(isset($_REQUEST['Hfile']))
				$Hfile = $_REQUEST['Hfile'];
				
			if(isset($_REQUEST['chkdel'])) {
				unlink($uploaddir.$Hfile);	
				$Hfile='';
			}

			$image_name   = $Hfile;
			$tempimage    = $_FILES['file_upload']['name'];
			$image        = $_FILES["file_upload"]["name"];
			$uploadedfile = $_FILES['file_upload']['tmp_name'];

			if($image)  {
			  	$filename = stripslashes($image);
			  	$extension = end(explode('.', $image));
			  	$extension = strtolower($extension);
			  	if($extension == "") {
					echo ' Unknown Image extension ';
			  	} else {
					$image_name = rand(0000,9999).$image;
					copy($uploadedfile,$uploaddir.$image_name);
			  	}
			} 							
			/* LOGO */

			/* BANNER */			
			$Bfile 		='';
			if(isset($_REQUEST['Bfile']))
				$Hfile = $_REQUEST['Bfile'];
				
			if(isset($_REQUEST['chkdel_b'])) {
				unlink($uploaddir.$Bfile);	
				$Bfile='';
			}

			$image_name_b = $Bfile;
			$tempimage    = $_FILES['banner']['name'];
			$image        = $_FILES["banner"]["name"];
			$uploadedfile = $_FILES['banner']['tmp_name'];

			if($image_b)  {
			  	$filename = stripslashes($image_b);
			  	$extension = end(explode('.', $image_b));
			  	$extension = strtolower($extension);
			  	if($extension == "") {
					echo ' Unknown Image extension ';
			  	} else {
					$image_name_b = rand(0000,9999).$image_b;
					copy($uploadedfile,$uploaddir.$image_name_b);
			  	}
			} 							
			/* BANNER */
			
			// UNIQUE USERNAME
			$uniqu 	= $dbh->query("SELECT * FROM `providers` 
										WHERE `provider_username` 	='{$username}'
										AND `status`<3
										AND `provider_id`!='{$id}'
									");
			if($uniqu->rowCount()>0){
				$_SESSION['sql_status_icon'] ='fa-exclamation';
				$_SESSION['sql_status_msg']  ='ERROR : Username already taken';										
				header('Location:index.php');
				break;
			}			
			
			/*** ADD STRAT ***/
			if($_POST['provider'] == 'add'){


				$add 	= $dbh->query("INSERT INTO `providers` SET
											`provider_type`					='{$type}',		
											`provider_email` 				='{$email}',
											`provider_username` 			='{$username}',
											`provider_password` 			='{$password}',
											`provider_responsible_name`		='{$res_name}',
											`provider_responsible_mobile`	='{$res_mobile}',
											`provider_name_en` 				='{$name_en}',
											`provider_name_ar` 				='{$name_ar}',
											`provider_description_en` 		='{$description_en}',
											`provider_description_ar` 		='{$description_ar}',
											`provider_brief_en` 			='{$breif_en}',
											`provider_brief_ar` 			='{$breif_ar}',
											`country_id`         			='{$country}',
											`area_id`						='{$area}',
											`provider_address` 		    	='{$address}',
											`provider_lat`					='{$lat}',
											`provider_long`					='{$long}',										
											`provider_logo` 				='{$image_name}',
											`provider_banner`				='{$image_name_b}',
											`provider_contract_number`		='{$contract_number}',
											`provider_features`				='{$feature}',
											`can_reserve`					='{$can_reserve}',
											`provider_important`			='{$imporatnt}',
											`permission_view`				='{$permissions}',
											`permission_add`				='{$permissions}',
											`permission_edit`				='{$permissions}',
											`permission_delete`				='{$permissions}',
											`status` 						=1,
											`created_by`					='{$login_id}',
											`created_at`					='{$now}'
										");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Provider Added Successfuly';
				}						 				
			}
			/*** ADD END ***/

			/*** UPDATE START ***/
			else{				
				$update = $dbh->query("UPDATE `providers` SET 
											`provider_type`					='{$type}',		
											`provider_email` 				='{$email}',
											`provider_username` 			='{$username}',											
											`provider_responsible_name`		='{$res_name}',
											`provider_responsible_mobile`	='{$res_mobile}',
											`provider_name_en` 				='{$name_en}',
											`provider_name_ar` 				='{$name_ar}',
											`provider_description_en` 		='{$description_en}',
											`provider_description_ar` 		='{$description_ar}',
											`provider_brief_en` 			='{$breif_en}',
											`provider_brief_ar` 			='{$breif_ar}',
											`country_id`         			='{$country}',
											`area_id`						='{$area}',
											`provider_address` 		    	='{$address}',
											`provider_lat`					='{$lat}',
											`provider_long`					='{$long}',										
											`provider_logo` 				='{$image_name}',
											`provider_banner`				='{$image_name_b}',
											`provider_contract_number`		='{$contract_number}',
											`provider_features`				='{$feature}',
											`provider_important`			='{$imporatnt}',
											`can_reserve`					='{$can_reserve}',											
											`updated_by`					='{$login_id}',
											`updated_at`					='{$now}'
											WHERE 
											`provider_id`				='{$id}'
										");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Provider Updated Successfuly';
				}
			}
			/*** UPDATE END ***/		

			header('Location:index.php');

		/***** ADD & UPDATE PROVIDER  END *****/


		/***** ADD & UPDATE PROVIDER MONILE STRAT *****/
		elseif(isset($_POST['provider_mobile'])):
			// GET POST DATA
			$number 		= $_POST['number'];
			$type 			= $_POST['type'];
			$provider_id 	= $_POST['provider_id'];
			$id 			= $_POST['id'];			
			// ADD
			if($_POST['provider_mobile'] == 'add'){
				$add 	= $dbh->query("INSERT INTO `provider_mobiles` SET 
										`provider_id`		='{$provider_id}',
										`mobile_number`		='{$number}',
										`mobile_type`		='{$type}',
										`status`			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Mobile added Successfuly';
				}
			}
			// UPDATE
			else{				
				$update = $dbh->query("UPDATE `provider_mobiles` SET 										
										`mobile_number`		='{$number}',
										`mobile_type`		='{$type}',										
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE
										`mobile_id`			='{$id}'
									");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Mobile updated Successfuly';
				}			
			}
			// REDIRECT
			header('Location:provider_mobiles.php?provider_id='.$provider_id);
		/***** ADD & UPDATE PROVIDER MONILE END *****/	

		/***** ADD & UPDATE PROVIDER MONILE STRAT *****/
		elseif(isset($_POST['provider_social'])):			
			// GET POST DATA
			$socail 		= $_POST['socail'];
			$link 			= $_POST['link'];
			$provider_id 	= $_POST['provider_id'];
			$id 			= $_POST['id'];			
			// ADD
			if($_POST['provider_social'] == 'add'){
				$add 	= $dbh->query("INSERT INTO `provider_social` SET 
										`provider_id`		='{$provider_id}',
										`social_name`		='{$socail}',
										`social_link`		='{$link}',
										`status`			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");
				if($add){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Social media added Successfuly';
				}
			}
			// UPDATE
			else{				
				$update = $dbh->query("UPDATE `provider_social` SET 										
										`social_name`		='{$socail}',
										`social_link`		='{$link}',										
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`social_id`			='{$id}'
									");
				if($update){
					$_SESSION['sql_status_icon'] ='fa-check';
					$_SESSION['sql_status_msg']  ='Social media updated Successfuly';
				}			
			}
			// REDIRECT
			header('Location:provider_socials.php?provider_id='.$provider_id);
		/***** ADD & UPDATE PROVIDER MONILE END *****/		
		endif;
	} // IF ISSET POST
?>
