<?php 
	define('INCLUDE_CHECK',true);
	require_once('../../function.php');
	$now 		= date('Y-m-d h:i:s');
	$a 			= new auth();  
	$login_id 	= $a->getLoggedUserID();

	$_SESSION['sql_status_icon'] ='fa-exclamation';
	$_SESSION['sql_status_msg']  ='Error';	
	
	if(isset($_POST)){				
		// GET POST DATA			
		$name_en   		= $_POST['name_en'];
		$name_ar   		= $_POST['name_ar'];
		$parent_id   	= $_POST['parent_id'];
		$sub_id   		= $_POST['sub_id'];	
		$id 			= $_POST['id'];			
			
		/***** ADD *****/
		if(isset($_POST['add'])){
			$add 	= $dbh->query("INSERT INTO `car_brands` SET 
										`parent_id`			='{$parent_id}',
										`sub_id`			='{$sub_id}',
										`cat_name_en` 		='{$name_en}',
										`cat_name_ar` 		='{$name_ar}',										
										`status` 			=1,
										`created_by`		='{$login_id}',
										`created_at`		='{$now}'
									");			
			if($add){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='record added successfuly';
			}

		} // add

		/***** UPDATE *****/
		elseif ($_POST['update']) {
			$update = $dbh->query("UPDATE `car_brands` SET 
										`parent_id`			='{$parent_id}',
										`sub_id`			='{$sub_id}',
										`cat_name_en` 		='{$name_en}',
										`cat_name_ar` 		='{$name_ar}',										
										`updated_by`		='{$login_id}',
										`updated_at`		='{$now}'
										WHERE 
										`cat_id`			='{$id}'
									");
			if($update){
				$_SESSION['sql_status_icon'] ='fa-check';
				$_SESSION['sql_status_msg']  ='record updated successfuly';
			}

		}//update		

		header('Location:index.php?parent_id='.$parent_id.'&sub_id='.$sub_id);

	} // IF ISSET POST
?>
