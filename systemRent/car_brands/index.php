<?php 
    require_once("../module/include_mod.php");
    if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');

    $parent_id  = (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0);
    $sub_id     = (isset($_GET['sub_id']) ? $_GET['sub_id'] : 0);

    // // PARENT CATEGOREY
    // $cat_query  = $dbh->query("SELECT * FROM `car_brands` WHERE `cat_id`='{$parent_id}' AND `status`=1 ");
    // if($cat_query->rowCount() > 0){
    //     $cat        = $cat_query->fetch();      
    // }

    // // SUB CATEGORY
    // $sub_query  = $dbh->query("SELECT * FROM `car_brands` WHERE `cat_id`='{$sub_id}' AND `status`=1 ");
    // if($sub_query->rowCount() > 0){
    //     $sub    = $sub_query->fetch();
    // }

    $bc     = 'car brands';
    if($parent_id  > 0){
        $bc     = 'car types';
    }
    
    if($sub_id > 0){
        $bc     = 'car categories';
    }

?>
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
<section id="container">
<?php include("../module/header_mod.php");?><!--header end-->
<?php include("../module/left_menu_mod.php");?><!--sidebar end-->
<section id="main-content">
    <section class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
                <?php if($parent_id > 0):?>
                <li><a href="index.php">cars brands</a></li> 
                <?php if($parent_id > 0):?>
                <li><a href="index.php?parent_id=<?php echo $parent_id;?>">car types</a></li> 
                <?php endif; ?>
                <li class="active"><?php echo $bc;?></li> 
                <?php else:?> 
                <li class="active"><?php echo $bc;?></li>   
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
				<div class="panel-body">
                    <div class="clearfix" style="padding-bottom:10px"><div class="btn-group pull-right"><a class="btn btn-primary" href="<?php echo 'cat_form.php?parent_id='.$parent_id.'&sub_id='.$sub_id?>" style="margin-right:10px">Add New <i class="fa fa-plus"></i></a></div></div>
                        <div class="adv-table editable-table ">
                            
                            <?php if(isset($_SESSION['sql_status_msg'])){?>
                            <div class="alert alert-success">
                                <span class="alert-icon"><i class="fa <?php echo $_SESSION['sql_status_icon']?>"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender"><?php echo $_SESSION['sql_status_msg']?></li>
                                    </ul><p><br></p>
                                </div>
                            </div>
                            <?php 
                                unset($_SESSION['sql_status_msg']);
                                }
                            ?>

                            <div class="space15"></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name EN</th>
                                        <th>Name AR</th>
                                        <?php if($parent_id == 0):?>
                                        <th>Types</th>
                                        <?php elseif($sub_id == 0): ?>
                                        <th>Category</th>
                                        <?php endif; ?>                                                                                                            
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
        							$stmt = $dbh->query("SELECT * FROM `car_brands` 
                                                            WHERE `parent_id`   ='{$parent_id}'
                                                            AND `sub_id`        ='{$sub_id}'
                                                            AND status < 3 
                                                        ");
        							$stmt->execute();
        							if($stmt->rowCount() > 0) {
                                        $ceq = 0;
        							    while($rec = $stmt->fetch()) {
                                            $id    = $rec['cat_id'];                                                                                        
                                ?>
                                <tr>
                                    <td><?php echo ++$ceq; ?></td>
                                    <td><?php echo $rec['cat_name_en']?></td>
                                    <td><?php echo $rec['cat_name_ar']?></td>
                                    <?php if($parent_id == 0):?>
                                    <td><a href="index.php?parent_id=<?php echo $id;?>" class="btn btn-info">Add/ View</a></td>                                    
                                    <?php elseif($sub_id== 0): ?>
                                    <td><a href="<?php echo 'index.php?parent_id='.$parent_id.'&sub_id='.$id;?>" class="btn btn-info">Add/ View</a></td>                                    
                                    <?php endif; ?>                                   
                                    <td>                                        
                                        <a href="<?php echo 'cat_form.php?parent_id='.$parent_id.'&sub_id='.$sub_id.'&id='.$id;?>" class="btn btn-warning">Edit</a>
                                    <?php if($rec['status']==1) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-default ch_status" value="2" value-id="<?php echo $id ;?>" table="car_brands" table-id="cat_id" >Disable</a>
                                    <?php } elseif($rec['status']==2) {?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-primary ch_status" value="1" value-id="<?php echo $id ;?>" table="car_brands" table-id="cat_id" >Enable</a>
                                    <?php }?>                                        
                                        <a href="javascript: return void(0);" class="btn btn-danger ch_status"  value="3" value-id="<?php echo $id ;?>" table="car_brands" table-id="cat_id">Delete</a> 
                                    </td>                                        
                                </tr>
                                <?php 
                                        } // while
    							    } // if
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
</section>
<?php include("../module/footer_mod.php");?>
</body>

</html>