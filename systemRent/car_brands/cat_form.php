<?php 
	require_once("../module/include_mod.php");

	if($_SESSION['flag_type'] != 1)header('Location:../main/main.php');
		
		$parent_id  = (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0);
	    $sub_id     = (isset($_GET['sub_id']) ? $_GET['sub_id'] : 0);

	    // // PARENT CATEGOREY
	    // $cat_query  = $dbh->query("SELECT * FROM `car_brands` WHERE `cat_id`='{$parent_id}' AND `status`=1 ");
	    // if($cat_query->rowCount() > 0){
	    //     $cat        = $cat_query->fetch();
	    //     $sub_id     = $cat['sub_id'];

	    //     // SUB CATEGORY
	    //     $sub_query  = $dbh->query("SELECT * FROM `car_brands` WHERE `cat_id`='{$sub_id}' AND `status`=1 ");
	    //     if($sub_query->rowCount() > 0){
	    //         $sub    = $sub_query->fetch();
	    //     }
	    // }

		$id 	= 0; if(isset($_GET['id'])) $id = $_GET['id'];

		if($id > 0){
			$action = 'update';
			$bc 	= 'updade brand';			
			if($parent_id  > 0){
				$bc 	= 'updade type';
			}
			if($sub_id > 0){
				$bc 	= 'updade category';
			}
			
			//  
			$update_query 	= $dbh->query("SELECT * FROM `car_brands` WHERE `cat_id`='{$id}'");
			if ($update_query->rowCount()>0) {
				$update 	= $update_query->fetch();		
			}

		}else{
			$action = 'add';
			$bc 	= 'add brand';
			if($parent_id  > 0){
				$bc 	= 'add type';
			}
			if($sub_id > 0){
				$bc 	= 'add category';
			}



		}
?>
	<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container">
		<!--header start--><?php include("../module/header_mod.php");?><!--header end-->
		<!--sidebar start--><?php include("../module/left_menu_mod.php");?><!--sidebar end-->
		<section id="main-content">
        	<section class="wrapper">
		        <div class="row">
		            <div class="col-md-12">
		                <ul class="breadcrumb">
		                    <li><a href="../main/main.php"><i class="fa fa-home"></i> Dashboard</a></li>                
			                <?php if($parent_id > 0):?>
			                <li><a href="index.php">cars brands</a></li> 
			                <?php if($sub_id > 0):?>
			                <li><a href="index.php?parent_id=<?php echo $parent_id;?>">car types</a></li> 
			                <?php endif; ?>
			                <li class="active"><?php echo $bc;?></li> 
			                <?php else:?> 
			                <li class="active"><?php echo $bc;?></li>   
			                <?php endif; ?>		                	
		                </ul>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-sm-12">
		                <section class="panel">
		                    <div class="panel-body">
		                        <div class="adv-table editable-table ">                            
		                            <div class="space15"></div>                    
			                            <form action="catController.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">											

			                            	<!-- NAME EN -->
											<div class="form-group">
												<label class="col-lg-2 control-label">English Name</label>
												<div class="col-md-4">
													<input type="text" name="name_en" class="form-control required" value="<?php echo @$update['cat_name_en'];?>" required>
												</div>
											</div>

											<!-- NAME AR -->
											<div class="form-group">
												<label class="col-lg-2 control-label">Arabic Name</label>
												<div class="col-md-4">
													<input type="text" name="name_ar" class="form-control required" value="<?php echo @$update['cat_name_ar'];?>" required>
												</div>
											</div>
											
			                               	<div class="form-group">
			                                	<label class="col-lg-2 control-label"></label>			                                	
			                                	<input type="hidden" name="id" value="<?php echo $id; ?>">
			                                	<input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>">
			                                	<input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
			                                    <div class="col-lg-4">
			                                    	<input class="btn btn-primary pull-right" type="submit" name="<?php echo $action; ?>" value="Submit">
			                                    	<a href="index.php" class="btn btn-danger pull-right" style="margin-right:5px">Back</a>
			                                    </div>
			                                </div>

			                            </form>
                        			</div>
                    		</div>
                		</section>
            		</div>
        		</div>
        	<!-- page end-->
        	</section>
    	</section>
    	<!--main content end-->
		<!--main content end-->
		<!--right sidebar start-->
		<div class="right-sidebar"><?php include("../module/right_mod.php");?></div>
	<!--right sidebar end-->
	</section>
<!-- Placed js at the end of the document so the pages load faster -->
<?php include("../module/footer_mod.php");?>
<script type="text/javascript">
	$(document).ready(function(){
		$('form').validate({
			rules:{
				name_en:"required",
				name_ar:"required",				
			}
		});
	});
</script>
</body>
</html>