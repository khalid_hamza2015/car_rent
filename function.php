<?php
if(!defined('INCLUDE_CHECK')) die('You are not allowed to execute this file directly');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"'); 
session_start();
require_once('DBF.php');
require_once('Auth.php');
ini_set('memory_limit', '-1');
ini_set('max_input_time', 10000000);  
ini_set('max_execution_time', 10000000);
set_time_limit(3600);
ini_set("post_max_size","128M");
ini_set("upload_max_filesize","128M");
date_default_timezone_set("Etc/GMT-3");

function full_url() {
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
	$string_link = $_SERVER['REQUEST_URI'];
	if((trim($string_link)=="/") or (trim($string_link)=="/index.php") or (trim($string_link)=="")) $string_link="/index.php";
	$parts = Explode('/', $string_link);
	return $parts[count($parts) - 1];
}

class Query  {
	////////////////////////////////General////////////////////////////////////
	function getByTableName($tableName)
	{
		$query = mysql_query(" SELECT * 
							   FROM   `".mysql_real_escape_string($tableName)."` ");
							   
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function getByTableNameWithOrder($tableName,$order)
	{
		$query = mysql_query(" SELECT * 
							   FROM   `".mysql_real_escape_string($tableName)."`
							   ORDER BY  `".mysql_real_escape_string($order)."` ");
							   
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function getByTableNameWithParent($tableName,$parent)
	{
		$query = mysql_query(" SELECT * 
							   FROM     `".mysql_real_escape_string($tableName)."`
							   WHERE    
							   	`parent_id` = '".mysql_real_escape_string($parent)."'  AND 
								`status_flag` = 1 
							   ORDER BY `name` ");
							   
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function getTableDataByID($table,$id)
	{
		$query = mysql_query(" SELECT * 
							   FROM   `".mysql_real_escape_string($table)."`  
							   where  `id` = ".mysql_real_escape_string($id)." ");
							   
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function getTableDataByType($table,$type)
	{
		$query = mysql_query(" SELECT * 
							   FROM   `".mysql_real_escape_string($table)."`  
							   WHERE   
							   	`flag_type`   = '".mysql_real_escape_string($type)."' AND 
								`status_flag` = 1
							   ORDER BY  `name_en`	 ");
							   
		if(mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function getTableDataByFlag($table,$flag)
	{
		$query = mysql_query(" SELECT  * 
							   FROM    `".mysql_real_escape_string($table)."`  
							   WHERE   `flag_status` = '".mysql_real_escape_string($flag)."' 
							   ORDER BY  `id`");
							   
		if(mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}

	function delRecordTableDataByID($table,$id) {
		$delQuery = @ mysql_query("DELETE  FROM  `".mysql_real_escape_string($table)."`  WHERE `id` = '".mysql_real_escape_string($id)."' ;");
		
		if ($delQuery)
			return 1;
		else
			return 0;
	}

	function changeStatusByTableNameAndID($tableName,$id,$status_flag) {
		$InsertQuery = @ mysql_query("UPDATE `{$tableName}` SET	 `status_flag` = {$status_flag} WHERE  `id` = {$id}  ");
		if($InsertQuery)
			return 1;
		else
			return 0;
	}

	function checkByTableNameAndFieldvalue($tableName,$txt_FieldName,$txt_FieldValue) {
		$sql = " SELECT * 
			     FROM   `".mysql_real_escape_string($tableName)."`
				 WHERE  `".mysql_real_escape_string($txt_FieldName)."` = '".mysql_real_escape_string($txt_FieldValue)."' 
				 ORDER BY `id` ";
		$query = mysql_query($sql);
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}
	function checkByTableNameWithCondition($tableName,$condition)
	{
		$query = mysql_query(" SELECT * FROM `{$tableName}` WHERE {$condition};");
		if(@ mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}
	function verifyCodeByTableNameAndFieldvalue($tableName,$username,$code)
	{
		$InsertQuery = @ mysql_query("UPDATE `".mysql_real_escape_string($tableName)."` SET  `active_flag` = '1'
									  WHERE  `email` = '".mysql_real_escape_string($username)."' AND `verfiy_code` = '".mysql_real_escape_string($code)."' ;");
		
		if ($InsertQuery)
			return 1;
		else
			return 0;
	}
	function login($tableName,$user,$pass)
	{
		$sql = " SELECT * 
			     FROM   `".mysql_real_escape_string($tableName)."`
				 WHERE  `email` = '".mysql_real_escape_string($user)."'  And `password` = '".mysql_real_escape_string($pass)."' ";
		
		$query = mysql_query($sql);
							   
		if(mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}
	function getCountryByChar($char) {
		$sql = " SELECT `short_name` 
		         FROM   `country` 
				 WHERE  `short_name` LIKE '".mysql_real_escape_string($char)."%' 
			     ORDER BY `short_name`";

		$query = mysql_query($sql);									   
		if(mysql_num_rows($query) > 0)
			return $query;
		else
			return 0;
	}
}
function resize_image($file, $w, $h, $crop=false) {
    list($width, $height) = getimagesize($file);
    $r = 1;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    
    //Get file extension
    $exploding = explode(".",$file);
    $ext = end($exploding);
    
    switch($ext){
        case "png":
            $src = imagecreatefrompng($file);
        break;
        case "jpeg":
        case "jpg":
            $src = imagecreatefromjpeg($file);
        break;
        case "gif":
            $src = imagecreatefromgif($file);
        break;
        default:
            $src = imagecreatefromjpeg($file);
        break;
    }
    
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    return $dst;
}?>